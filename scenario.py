import subprocess
import requests

print("!!!WELCOME TO THE WINE MANAGERY SYSTEM SCENARIO!!!" + "\n")
# this is something that simply had to be added to make the scenario work on first time run
subprocess.run(["bash", "./database-management/seed-all.sh"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

print("Lets start with clear database")
print("Cleaning the database..." + "\n")
subprocess.run(["bash", "./database-management/clean-all.sh"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

headers = {
    'Authorization': 'Bearer eyJraWQiOiJyc2ExIiwidHlwIjoiYXQrand0IiwiYWxnIjoiUlMyNTYifQ.eyJhdWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJzdWIiOiI1MTQxNDhAbXVuaS5jeiIsImFjciI6Imh0dHBzOi8vcmVmZWRzLm9yZy9wcm9maWxlL21mYSIsInNjb3BlIjoidGVzdF80IHRlc3RfMyB0ZXN0XzIgdGVzdF8xIHRlc3RfcmVhZCBvcGVuaWQgcHJvZmlsZSBlZHVwZXJzb25fc2NvcGVkX2FmZmlsaWF0aW9uIHRlc3Rfd3JpdGUgZW1haWwgdGVzdF81IiwiYXV0aF90aW1lIjoxNzE1NzA4MDk3LCJpc3MiOiJodHRwczovL29pZGMubXVuaS5jei9vaWRjLyIsImV4cCI6MTcxNjM5MzczOCwiaWF0IjoxNzE2MzkwMTM4LCJjbGllbnRfaWQiOiI3ZTAyYTBhOS00NDZhLTQxMmQtYWQyYi05MGFkZDQ3YjBmZGQiLCJqdGkiOiI2YjhkYzA1OC0zMzMwLTQwNGQtYjk2Mi1jZTQxMDFmZmM1M2UifQ.XEKCDplat0dPS-GO3tNZHql-b4KzC0FvK3QUDCSs3LCdCj1hLYac6ABHqQsFX4URCQRzH0PnDkFhnNjLrTQ6qdo8MhX0Wpt_8ByZBK0Z1X9vuDGuLdKeiu6H7bdhDJ9Ydc0uHDHAuF-sG5MZeTKefkJ_gLHbK2IdTjFSCfBSwFqXO9v8IgrC3R5rUVrugK4L66JL-NaZIsqaiOCSmY7jlGd5qokmDBiSgiYj3K1OcLlmBc8banAo59__N3cngy_k38IcK30fMhBCmtWd3jAg2bho4ssHXG_HZR2HtcNk06kDVvXMCwHnkOCXbaN_DQDfh17El39psxLRlQwcnrFBNQ'
}

print("As we can see, new grape has grown at our vineyard!")
print("[HARVEST MANAGER] Creating grape...")
create_wine = requests.post(
    'http://localhost:8081/grape',
    json={'code': 123, 'name': 'GreatPinkGrape', 'color': 6,
          'description': 'This is best pink grape'},
    headers=headers
)
print(create_wine.text + "\n")

print("Lets now harvest 200 kg of that grape!")
print("[HARVEST MANAGER + INVENTORY MANAGER] Creating harvest and stocking it...")
create_harvest = requests.post(
    'http://localhost:8081/harvest',
    json={
        "date": "2021-01-01",
        "quality": 1,
        "amountInKg": 200.0,
        "grapeType": {
            "code": 123,
            "name": "GreatPinkGrape",
            "color": 6,
            "description": "This is best pink grape"
        }
    },
    headers=headers
)
print(create_harvest.text + "\n")

print("Lets now put some more data into the system!")
print("Seeding the database..." + "\n")
res = subprocess.run(["bash", "./database-management/seed-all.sh"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

print("For new wine creation we need to know recipe a.k.a production process. Which production processes are present in the system?")
print("[WINE PRODUCTION] Production processes available:")
available_processes = requests.get("http://localhost:8083/productionProcess", headers=headers)
for item in available_processes.json():
    print(item)
print("\n")

print("However, for some of them we might not have enough ingredients or grapes in the inventory. Lets check which are possible to use:")
print("[WINE PRODUCTION + INVENTORY MANAGER] Production processes possible: ")
possible_processes = requests.get("http://localhost:8083/productionProcess/possible", headers=headers)
for item in possible_processes.json():
    print(item)
print("\n")

print("Demonstration of why some production processes are not possible:")
print("[INVENTORY MANAGER] Ingredients in stock:")
ingredient_list = requests.get('http://localhost:8082/ingredient/list', headers=headers)
for item in ingredient_list.json():
    print(item)
print("\n")

print("Grapes in stock:")
grape_list = requests.get('http://localhost:8082/grape/list', headers=headers)
for item in grape_list.json():
    print(item)
print("\n")

print("Lets produce wine using possible production process!")
print("[WINE PRODUCTION] Producing wine...")
produce = requests.post('http://localhost:8083/productionProcess/produce/652c1ca4-af70-4439-9e98-e9cfeb54eaec', headers=headers)
print(produce.text + "\n")


print("Lets check the inventory now!")
print("[INVENTORY MANAGER] Grapes in stock:")
grape_list = requests.get('http://localhost:8082/grape/list', headers=headers)
for item in grape_list.json():
    print(item)
print("\n")

print("[INVENTORY MANAGER] Ingredients in stock:")
ingredient_list = requests.get('http://localhost:8082/ingredient/list', headers=headers)
for item in ingredient_list.json():
    print(item)
print("\n")

print("[INVENTORY MANAGER] Wine in stock:")
product_list = requests.get('http://localhost:8082/wine/list', headers=headers)
for item in product_list.json():
    print(item)

print("Buying wine...")
buy_wine = requests.post(
    'http://localhost:8084/purchase/single',
    json={'customerId': 11, 'purchasedAt': '2021-01-01T17:52:07.994459792Z', 'wineBottleExtId': '111',
          'quantity': 1, 'price': 10},
    headers=headers
)
print("[CUSTOMER SERVICE] wine purchased:")
print(buy_wine.text + "\n")

print("Lets create invoice for the purchase!")
print("[CUSTOMER SERVICE] Creating invoice...")
create_invoice = requests.post(
    'http://localhost:8084/invoice?customerId=11',
    json={
        "price": 10.00,
        "description": "Product1 invoice",
        "issuedDate": "2022-04-01T00:00:00Z",
        "dueDate": "2022-04-30T00:00:00Z",
        "email": "showcase@email.com",
        "phone": "123456789",
        "zipCode": "12345",
        "address": "Showcase Street 1",
        "city": "Showcase",
        "items": [
            {
                "name": "Product1",
                "quantity": 1,
                "price": 10.00,
                "wineExtId": 111
            }
        ]
    },
    headers=headers
)

print("[CUSTOMER SERVICE] Invoice created:")
print(create_invoice.text + "\n")

print("Lets leave feedback for the wine!")
print("[CUSTOMER SERVICE] Leaving feedback...")
leave_feedback = requests.post(
    'http://localhost:8084/feedback/11',
    json={
        "rating": 4.7,
        "feedbackText": "The wine has a delicate taste with lingers of hazelnut and macadamia nut."
                        " I advise to try it yourself.",
        "wineBottleExtId": 111
    },
    headers=headers
)
print(leave_feedback.text + "\n")

print("Now let's see all the feedbacks our customer posted!")
print("[CUSTOMER SERVICE] Feedbacks for our customer:")
feedbacks = requests.get('http://localhost:8084/feedback/customer?customerId=11', headers=headers)
print(feedbacks.text + "\n")
