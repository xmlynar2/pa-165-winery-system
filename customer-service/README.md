# Customer service

The Customer service provides functionality to view all the wines the system offers.
The system provides functionality to buy a wine from the collection.
It also allows to give feedback on the particular wine bottle.

<hr />

## Prerequisites
1. Postgres needs to be installed on machine
2. Either run locally or start database with docker-compose.yml with docker in the parent directory

## How to run
1. `mvn clean install`
2. `mvn spring-boot:run`
