package cz.muni.fi.pa165.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;
import cz.muni.fi.pa165.repository.CustomerRepository;
import cz.muni.fi.pa165.repository.FeedbackRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
public class FeedbackControllerTestIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    void initData() {
        CustomerDAO customer = new CustomerDAO();
        customer = customerRepository.save(customer);

        FeedbackDAO feedback = new FeedbackDAO();
        feedback.setCustomer(customer);
        feedback.setWineExtId(10L);
        feedback.setRating(5);
        feedback.setFeedbackText("feedbackText");
        feedbackRepository.save(feedback);
    }

    @AfterEach
    void clearData() {
        customerRepository.deleteAll();
    }

    @Test
    void getFeedbackById_existingFeedback_returnsFeedback() throws Exception {
        String responseJson = mockMvc.perform(get("/feedback/1"))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        FeedbackDTO response = objectMapper.readValue(responseJson, FeedbackDTO.class);

        // Assert
        assertThat(response.getWineBottleExtId()).isEqualTo(10L);
        assertThat(response.getRating()).isEqualTo(5);
        assertThat(response.getFeedbackText()).isEqualTo("feedbackText");
    }

    @Test
    void getFeedbackById_nonExistingFeedback_returnsNotFound() throws Exception {
        mockMvc.perform(get("/feedback/100"))
                .andExpect(status().isNotFound());
    }
}
