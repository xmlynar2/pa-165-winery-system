package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.PurchaseFacade;
import cz.muni.fi.pa165.model.dto.PurchaseDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class PurchaseControllerTest {

    @Mock
    private PurchaseFacade purchaseFacade;

    @InjectMocks
    private PurchaseController purchaseController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void purchaseWineBottleReturnsPurchasedBottle() {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        when(purchaseFacade.purchaseWineBottle(purchaseDTO)).thenReturn(purchaseDTO);

        PurchaseDTO result = purchaseController.purchaseWineBottle(purchaseDTO);

        assertEquals(purchaseDTO, result);
    }

    @Test
    void purchaseMultipleWineBottlesReturnsPurchasedBottles() {
        List<PurchaseDTO> purchaseDTOList = new ArrayList<>();
        purchaseDTOList.add(new PurchaseDTO());
        when(purchaseFacade.purchaseWineBottles(purchaseDTOList)).thenReturn(purchaseDTOList);

        List<PurchaseDTO> result = purchaseController.purchaseMultipleWineBottles(purchaseDTOList);

        assertEquals(purchaseDTOList, result);
    }
}

