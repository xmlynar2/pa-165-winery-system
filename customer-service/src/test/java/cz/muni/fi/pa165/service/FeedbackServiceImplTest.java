package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;
import cz.muni.fi.pa165.repository.FeedbackRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Patrik Palovcik
 */
@ExtendWith(MockitoExtension.class)
public class FeedbackServiceImplTest {

    @Mock
    private FeedbackRepository feedbackRepository;

    @InjectMocks
    private FeedbackServiceImpl feedbackService;

    @Test
    void findById_existingFeedback_returnsFeedback() {
        // Arrange
        Long id = 123456L;
        FeedbackDAO expectedFeedback = createSampleFeedbackDAO();
        when(feedbackRepository.findById(id)).thenReturn(Optional.of(expectedFeedback));

        // Act
        FeedbackDAO foundFeedback = feedbackService.findById(id);

        // Assert
        assertEquals(expectedFeedback, foundFeedback);
        verify(feedbackRepository, times(1)).findById(id);
    }

    @Test
    void findById_nonExistingFeedback_throwsResourceNotFoundException() {
        // Given
        Long id = 123456L;
        when(feedbackRepository.findById(id)).thenReturn(Optional.empty());

        // When & Then
        assertThrows(ResourceNotFoundException.class, () -> feedbackService.findById(id));
    }

    @Test
    void findByCustomerId_existingCustomer_returnsFeedbackList() {
        // Arrange
        Long customerId = 1L;
        List<FeedbackDAO> expectedFeedbackList = Arrays.asList(createSampleFeedbackDAO());
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        when(feedbackRepository.findAllByCustomerId(customerId, pageable)).thenReturn(new PageImpl<>(expectedFeedbackList));

        // Act
        List<FeedbackDAO> foundFeedbackList = feedbackService.findByCustomerId(customerId, 0, 10);

        // Assert
        assertEquals(expectedFeedbackList, foundFeedbackList);
        verify(feedbackRepository, times(1)).findAllByCustomerId(customerId, pageable);
    }

    @Test
    void findByCustomerId_nonExistingCustomer_returnsEmptyList() {
        // Given
        Long customerId = 1L;
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        when(feedbackRepository.findAllByCustomerId(customerId, pageable)).thenReturn(new PageImpl<>(List.of()));

        // When
        List<FeedbackDAO> foundFeedbackList = feedbackService.findByCustomerId(customerId, 0, 10);

        // Then
        assertTrue(foundFeedbackList.isEmpty());
    }

    @Test
    void findByWineBottleId_existingWineBottle_returnsFeedbackList() {
        // Arrange
        Long wineBottleId = 1L;
        List<FeedbackDAO> expectedFeedbackList = Arrays.asList(createSampleFeedbackDAO());
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        when(feedbackRepository.findAllByWineExtId(wineBottleId, pageable)).thenReturn(new PageImpl<>(expectedFeedbackList));

        // Act
        List<FeedbackDAO> foundFeedbackList = feedbackService.findByWineBottleId(wineBottleId, 0, 10);

        // Assert
        assertEquals(expectedFeedbackList, foundFeedbackList);
        verify(feedbackRepository, times(1)).findAllByWineExtId(wineBottleId, pageable);
    }

    @Test
    void findByWineBottleId_nonExistingWineBottle_returnsEmptyList() {
        // Given
        Long wineBottleId = 1L;
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        when(feedbackRepository.findAllByWineExtId(wineBottleId, pageable)).thenReturn(new PageImpl<>(List.of()));

        // When
        List<FeedbackDAO> foundFeedbackList = feedbackService.findByWineBottleId(wineBottleId, 0, 10);

        // Then
        assertTrue(foundFeedbackList.isEmpty());
    }

    @Test
    void createFeedback_validFeedbackDTOAndCustomer_savesSuccessfully() {
        // Arrange
        FeedbackDTO feedbackDTO = createSampleFeedbackDTO();
        CustomerDAO customerDAO = createSampleCustomerDAO();
        FeedbackDAO expectedFeedback = createSampleFeedbackDAO();
        when(feedbackRepository.save(Mockito.any(FeedbackDAO.class))).thenReturn(expectedFeedback);

        // Act
        feedbackService.createFeedback(feedbackDTO, customerDAO);

        // Assert
        verify(feedbackRepository, times(1)).save(Mockito.any(FeedbackDAO.class));
    }

    @Test
    void updateFeedbackById_nonExistingFeedback_throwsResourceNotFoundException() {
        // Given
        FeedbackDTO feedbackDTO = createSampleFeedbackDTO();
        feedbackDTO.setId(1L);
        when(feedbackRepository.findById(feedbackDTO.getId())).thenReturn(Optional.empty());

        // When & Then
        assertThrows(ResourceNotFoundException.class, () -> feedbackService.updateFeedbackById(feedbackDTO));
    }

    // Helper methods to create sample instances
    private FeedbackDAO createSampleFeedbackDAO() {
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        feedbackDAO.setId(1L);
        return feedbackDAO;
    }

    private FeedbackDTO createSampleFeedbackDTO() {
        FeedbackDTO feedbackDTO = new FeedbackDTO();
        feedbackDTO.setRating(5);
        return feedbackDTO;
    }

    private CustomerDAO createSampleCustomerDAO() {
        CustomerDAO customerDAO = new CustomerDAO();
        customerDAO.setId(1L);
        return customerDAO;
    }
}
