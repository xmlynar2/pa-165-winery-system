package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

//@DataJpaTest
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class FeedbackRepositoryTest {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private FeedbackRepository feedbackRepository;

    private Long customerId = 1L;

    @Test
    void findAllByCustomerId_existingCustomer_returnsFeedbacks() {
        List<FeedbackDAO> feedbacks = feedbackRepository.findAllByCustomerId(customerId, Pageable.ofSize(10).withPage(0)).getContent();

        assertFalse(feedbacks.isEmpty());
        assertThat(feedbacks.size()).isEqualTo(1);
        FeedbackDAO feedback = feedbacks.getFirst();
        assertThat(feedback.getCustomer().getId()).isEqualTo(customerId);
        assertThat(feedback.getWineExtId()).isEqualTo(10L);
        assertThat(feedback.getRating()).isEqualTo(5);
        assertThat(feedback.getFeedbackText()).isEqualTo("feedbackText");
    }

    @BeforeEach
    void initData() {
        CustomerDAO customer = new CustomerDAO();
        customer.setAddress("address");
        customer.setEmail("email");
        customer.setFirstName("firstName");
        customer.setLastName("lastName");

        FeedbackDAO feedback = new FeedbackDAO();
        feedback.setCustomer(customer);
        feedback.setWineExtId(10L);
        feedback.setRating(5);
        feedback.setFeedbackText("feedbackText");

        customer.setFeedbacks(List.of(feedback));
        entityManager.persist(customer);
        customerId = customer.getId();

//        customerId = entityManager.persistAndGetId(customer, Long.class);
    }

    @Test
    void findAllByCustomerId_nonExistingCustomer_returnsEmptyList() {
        List<FeedbackDAO> feedbacks = feedbackRepository.findAllByCustomerId(3L, Pageable.ofSize(10).withPage(0)).getContent();

        assertThat(feedbacks).isEmpty();
    }

    @Test
    void findAllByWineExtId_existingWine_returnsFeedbacks() {
        List<FeedbackDAO> feedbacks = feedbackRepository.findAllByWineExtId(10L, Pageable.ofSize(10).withPage(0)).getContent();

        assertFalse(feedbacks.isEmpty());
        assertThat(feedbacks.size()).isEqualTo(1);
        FeedbackDAO feedback = feedbacks.getFirst();
        assertThat(feedback.getCustomer().getId()).isEqualTo(customerId);
        assertThat(feedback.getWineExtId()).isEqualTo(10L);
        assertThat(feedback.getRating()).isEqualTo(5);
        assertThat(feedback.getFeedbackText()).isEqualTo("feedbackText");
    }

    @Test
    void findAllByWineExtId_nonExistingWine_returnsEmptyList() {
        List<FeedbackDAO> feedbacks = feedbackRepository.findAllByWineExtId(3L, Pageable.ofSize(10).withPage(0)).getContent();

        assertThat(feedbacks).isEmpty();
    }
}
