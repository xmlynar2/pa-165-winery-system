package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;
import cz.muni.fi.pa165.service.CustomerService;
import cz.muni.fi.pa165.service.FeedbackService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FeedbackFacadeImplTest {

    @Mock
    private FeedbackService feedbackService;

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private FeedbackFacadeImpl feedbackFacade;

    @Test
    void findById_existingId_callsServiceMethod() {
        // Arrange
        Long id = 1L;
        FeedbackDAO feedbackDAO = createSampleFeedbackDAO();
        FeedbackDTO expectedFeedbackDTO = createSampleFeedbackDTO();
        when(feedbackService.findById(id)).thenReturn(feedbackDAO);

        // Act
        FeedbackDTO result = feedbackFacade.findById(id);

        // Assert
        assertEquals(expectedFeedbackDTO.getId(), result.getId());
        assertEquals(expectedFeedbackDTO.getRating(), result.getRating());
        assertEquals(expectedFeedbackDTO.getFeedbackText(), result.getFeedbackText());
        assertEquals(expectedFeedbackDTO.getWineBottleExtId(), result.getWineBottleExtId());

        // Assert for the time field with tolerance
        assertTrue(Math.abs(expectedFeedbackDTO.getCreatedAt().getEpochSecond() - result.getCreatedAt().getEpochSecond()) < 1); // Adjust the tolerance as needed

        verify(feedbackService, times(1)).findById(id);
    }

    @Test
    void findByCustomerId_existingCustomerId_callsServiceMethod() {
        // Arrange
        Long customerId = 1L;
        List<FeedbackDAO> feedbackDAOList = List.of(createSampleFeedbackDAO());
        List<FeedbackDTO> expectedFeedbackDTOList = List.of(createSampleFeedbackDTO());
        when(feedbackService.findByCustomerId(customerId, 0, 10)).thenReturn(feedbackDAOList);

        // Act
        List<FeedbackDTO> result = feedbackFacade.findByCustomerId(customerId, 0, 10);

        // Assert
        assertEquals(expectedFeedbackDTOList.size(), result.size());
        for (int i = 0; i < expectedFeedbackDTOList.size(); i++) {
            FeedbackDTO expected = expectedFeedbackDTOList.get(i);
            FeedbackDTO actual = result.get(i);
            assertEquals(expected.getId(), actual.getId());
            assertEquals(expected.getRating(), actual.getRating());
            assertEquals(expected.getFeedbackText(), actual.getFeedbackText());
            assertEquals(expected.getWineBottleExtId(), actual.getWineBottleExtId());

            // Assert for the time field with tolerance
            assertTrue(Math.abs(expected.getCreatedAt().getEpochSecond() - actual.getCreatedAt().getEpochSecond()) < 1); // Adjust the tolerance as needed
        }

        verify(feedbackService, times(1)).findByCustomerId(customerId, 0, 10);
    }

    @Test
    void findByWineId_existingWineId_callsServiceMethod() {
        // Arrange
        Long wineId = 1L;
        List<FeedbackDAO> feedbackDAOList = List.of(createSampleFeedbackDAO());
        List<FeedbackDTO> expectedFeedbackDTOList = List.of(createSampleFeedbackDTO());
        when(feedbackService.findByWineBottleId(wineId, 0, 10)).thenReturn(feedbackDAOList);

        // Act
        List<FeedbackDTO> result = feedbackFacade.findByWineId(wineId, 0, 10);

        // Assert
        assertEquals(expectedFeedbackDTOList.size(), result.size());
        for (int i = 0; i < expectedFeedbackDTOList.size(); i++) {
            FeedbackDTO expected = expectedFeedbackDTOList.get(i);
            FeedbackDTO actual = result.get(i);
            assertEquals(expected.getId(), actual.getId());
            assertEquals(expected.getRating(), actual.getRating());
            assertEquals(expected.getFeedbackText(), actual.getFeedbackText());
            assertEquals(expected.getWineBottleExtId(), actual.getWineBottleExtId());

            // Assert for the time field with tolerance
            assertTrue(expected.getCreatedAt().isBefore(actual.getCreatedAt().plusMillis(1000)));
            assertTrue(expected.getCreatedAt().isAfter(actual.getCreatedAt().minusMillis(1000)));
        }

        verify(feedbackService, times(1)).findByWineBottleId(wineId, 0, 10);
    }

    @Test
    void createFeedback_validFeedback_callsServiceMethods() {
        // Arrange
        FeedbackDTO feedbackDTO = createSampleFeedbackDTO();
        Long customerId = 1L;
        CustomerDAO customerDAO = createSampleCustomerDAO();
        when(customerService.findCustomerById(customerId)).thenReturn(customerDAO);

        // Act
        feedbackFacade.createFeedback(feedbackDTO, customerId);

        // Assert
        verify(customerService, times(1)).findCustomerById(customerId);
        verify(feedbackService, times(1)).createFeedback(feedbackDTO, customerDAO);
    }

    @Test
    void updateFeedbackReturnsUpdatedFeedback() {
        FeedbackDTO feedbackDTO = createSampleFeedbackDTO();
        FeedbackDAO feedbackDAO = createSampleFeedbackDAO();

        when(feedbackService.updateFeedbackById(feedbackDTO)).thenReturn(feedbackDAO);

        FeedbackDTO updatedFeedback = feedbackFacade.updateFeedback(feedbackDTO);

        verify(feedbackService, times(1)).updateFeedbackById(feedbackDTO);
        assertEquals(feedbackDTO.getId(), updatedFeedback.getId());
        assertEquals(feedbackDTO.getRating(), updatedFeedback.getRating());
        assertEquals(feedbackDTO.getFeedbackText(), updatedFeedback.getFeedbackText());
        assertEquals(feedbackDTO.getWineBottleExtId(), updatedFeedback.getWineBottleExtId());
    }

    @Test
    void updateFeedbackWithNonExistingIdThrowsNotFound() {
        FeedbackDTO feedbackDTO = createSampleFeedbackDTO();

        when(feedbackService.updateFeedbackById(feedbackDTO)).thenThrow(new ResourceNotFoundException("Feedback not found"));

        assertThrows(ResourceNotFoundException.class, () ->
            feedbackFacade.updateFeedback(feedbackDTO)
        );
    }

    // Helper methods to create sample instances
    private FeedbackDTO createSampleFeedbackDTO() {
        return new FeedbackDTO(
                1L,
                5.0,
                Instant.now(),
                "Sample feedback text",
                1L
        );
    }

    private FeedbackDAO createSampleFeedbackDAO() {
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        feedbackDAO.setId(1L);
        feedbackDAO.setRating(5.0);
        feedbackDAO.setCreatedAt(Instant.now());
        feedbackDAO.setFeedbackText("Sample feedback text");
        feedbackDAO.setWineExtId(1L);
        return feedbackDAO;
    }

    private CustomerDAO createSampleCustomerDAO() {
        return new CustomerDAO();
    }
}
