package cz.muni.fi.pa165.controller;


import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.InvoiceFacade;
import cz.muni.fi.pa165.model.dto.InvoiceDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class InvoiceControllerTest {

    @Mock
    private InvoiceFacade invoiceFacade;

    @InjectMocks
    private InvoiceController invoiceController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createInvoiceReturnsInvoice() {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        Long customerId = 1L;

        when(invoiceFacade.createInvoice(invoiceDTO, customerId)).thenReturn(invoiceDTO);

        InvoiceDTO actualInvoice = invoiceController.createInvoice(invoiceDTO, customerId);

        verify(invoiceFacade, times(1)).createInvoice(invoiceDTO, customerId);
        assertEquals(invoiceDTO, actualInvoice);
    }

    @Test
    void createInvoiceWithExistingInvoiceThrowsConflict() {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        Long customerId = 1L;

        doThrow(new DataIntegrityViolationException("Invoice already exists"))
                .when(invoiceFacade).createInvoice(invoiceDTO, customerId);

        assertThrows(DataIntegrityViolationException.class, () ->
            invoiceController.createInvoice(invoiceDTO, customerId)
        );
    }

    @Test
    void getInvoiceByIdReturnsInvoice() {
        String invoiceId = "1";
        InvoiceDTO expectedInvoice = new InvoiceDTO();

        when(invoiceFacade.getInvoiceById(invoiceId)).thenReturn(expectedInvoice);

        InvoiceDTO actualInvoice = invoiceController.getInvoiceById(invoiceId);

        verify(invoiceFacade, times(1)).getInvoiceById(invoiceId);
        assertEquals(expectedInvoice, actualInvoice);
    }

    @Test
    void getInvoiceByIdWithNonExistingIdThrowsNotFound() {
        String invoiceId = "1";

        when(invoiceFacade.getInvoiceById(invoiceId)).thenThrow(new ResourceNotFoundException("Invoice not found"));

        assertThrows(ResourceNotFoundException.class, () ->
            invoiceController.getInvoiceById(invoiceId)
        );
    }

    @Test
    void getInvoicesByCustomerIdReturnsInvoices() {
        Long customerId = 1L;
        List<InvoiceDTO> expectedInvoices = List.of(new InvoiceDTO(), new InvoiceDTO());

        when(invoiceFacade.getInvoicesByCustomerId(customerId, 0, 10)).thenReturn(expectedInvoices);

        List<InvoiceDTO> actualInvoices = invoiceController.getInvoicesByCustomerId(customerId, 0, 10);

        verify(invoiceFacade, times(1)).getInvoicesByCustomerId(customerId, 0, 10);
        assertEquals(expectedInvoices, actualInvoices);
    }

    @Test
    void getInvoicesByCustomerIdWithNonExistingIdReturnsEmptyList() {
        Long customerId = 1L;

        when(invoiceFacade.getInvoicesByCustomerId(customerId, 0, 10)).thenReturn(List.of());

        List<InvoiceDTO> actualInvoices = invoiceController.getInvoicesByCustomerId(customerId, 0, 10);

        verify(invoiceFacade, times(1)).getInvoicesByCustomerId(customerId, 0, 10);
        assertTrue(actualInvoices.isEmpty());
    }

    @Test
    void generateInvoiceDocumentReturnsDocument() {
        String invoiceId = "1";
        byte[] expectedDocument = new byte[0];

        when(invoiceFacade.generateInvoiceDocument(invoiceId)).thenReturn(expectedDocument);

        byte[] actualDocument = invoiceController.generateInvoiceDocument(invoiceId);

        verify(invoiceFacade, times(1)).generateInvoiceDocument(invoiceId);
        assertArrayEquals(expectedDocument, actualDocument);
    }

    @Test
    void generateInvoiceDocumentWithNonExistingIdThrowsNotFound() {
        String invoiceId = "1";

        when(invoiceFacade.generateInvoiceDocument(invoiceId)).thenThrow(new ResourceNotFoundException("Invoice not found"));

        assertThrows(ResourceNotFoundException.class, () ->
            invoiceController.generateInvoiceDocument(invoiceId)
        );
    }
}
