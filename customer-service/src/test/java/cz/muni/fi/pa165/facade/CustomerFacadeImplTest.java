package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dto.CustomerDTO;
import cz.muni.fi.pa165.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerFacadeImplTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerFacadeImpl customerFacade;

    @Test
    void findCustomerByIdReturnsCustomer() {
        long customerId = 1L;
        CustomerDTO expectedCustomer = createSampleCustomerDTO();

        when(customerService.findCustomerById(customerId)).thenReturn(createSampleCustomerDAO());

        CustomerDTO foundCustomer = customerFacade.findCustomerById(customerId);

        verify(customerService, times(1)).findCustomerById(customerId);
        assertEquals(expectedCustomer, foundCustomer);
    }

    @Test
    void findCustomerByIdWithNonExistingIdThrowsNotFound() {
        long nonExistingCustomerId = 100L;

        when(customerService.findCustomerById(nonExistingCustomerId)).thenThrow(new ResourceNotFoundException("Customer not found"));

        assertThrows(ResourceNotFoundException.class, () -> customerFacade.findCustomerById(nonExistingCustomerId));
    }

    @Test
    void createCustomerSavesSuccessfully() {
        CustomerDTO customerDTO = createSampleCustomerDTO();

        customerFacade.createCustomer(customerDTO);

        verify(customerService, times(1)).createCustomer(customerDTO);
    }

    private CustomerDTO createSampleCustomerDTO() {
        return new CustomerDTO(1L, "John", "Doe", "john.doe@example.com", "123 Main St, Anytown");
    }

    private CustomerDAO createSampleCustomerDAO() {
        CustomerDAO customerDAO = new CustomerDAO();
        customerDAO.setId(1L);
        customerDAO.setFirstName("John");
        customerDAO.setLastName("Doe");
        customerDAO.setEmail("john.doe@example.com");
        customerDAO.setAddress("123 Main St, Anytown");
        return customerDAO;
    }
}
