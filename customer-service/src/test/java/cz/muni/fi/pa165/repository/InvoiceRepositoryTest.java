package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.InvoiceDAO;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class InvoiceRepositoryTest {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private InvoiceRepository invoiceRepository;

    private Long customerId = 1L;

    @BeforeEach
    void initData() {
        CustomerDAO customer = new CustomerDAO();
        customer.setAddress("address");
        customer.setEmail("email");
        customer.setFirstName("firstName");
        customer.setLastName("lastName");

        InvoiceDAO invoice1 = new InvoiceDAO();
        invoice1.setPrice(100);
        invoice1.setDescription("description");
        invoice1.setEmail("email");
        invoice1.setPhone("phone");
        invoice1.setAddress("address");
        invoice1.setCity("city");
        invoice1.setCustomer(customer);

        InvoiceDAO invoice2 = new InvoiceDAO();
        invoice2.setPrice(200);
        invoice2.setDescription("description");
        invoice2.setEmail("email");
        invoice2.setPhone("phone");
        invoice2.setAddress("address");
        invoice2.setCity("city");
        invoice2.setCustomer(customer);

        customer.setInvoices(List.of(invoice1, invoice2));

        entityManager.persist(customer);
        customerId = customer.getId();
    }

    @Test
    void findByCustomerId_existingCustomer_returnsInvoices() {
        List<InvoiceDAO> invoices = invoiceRepository.findByCustomerId(customerId, Pageable.ofSize(10).withPage(0)).getContent();

        assertFalse(invoices.isEmpty());
        assertThat(invoices.size()).isEqualTo(2);
        InvoiceDAO invoice1 = invoices.getFirst();
        InvoiceDAO invoice2 = invoices.get(1);
        assertThat(invoice1.getCustomer().getId()).isEqualTo(customerId);
        assertThat(invoice1.getPrice()).isEqualTo(100);
        assertThat(invoice1.getDescription()).isEqualTo("description");
        assertThat(invoice1.getEmail()).isEqualTo("email");
        assertThat(invoice1.getPhone()).isEqualTo("phone");
        assertThat(invoice1.getAddress()).isEqualTo("address");
        assertThat(invoice1.getCity()).isEqualTo("city");
        assertThat(invoice2.getCustomer().getId()).isEqualTo(customerId);
        assertThat(invoice2.getPrice()).isEqualTo(200);
        assertThat(invoice2.getDescription()).isEqualTo("description");
        assertThat(invoice2.getEmail()).isEqualTo("email");
        assertThat(invoice2.getPhone()).isEqualTo("phone");
        assertThat(invoice2.getAddress()).isEqualTo("address");
        assertThat(invoice2.getCity()).isEqualTo("city");
    }

    @Test
    void findByCustomerId_nonExistingCustomer_returnsEmptyList() {
        List<InvoiceDAO> invoices = invoiceRepository.findByCustomerId(3L, Pageable.ofSize(10).withPage(0)).getContent();

        assertThat(invoices).isEmpty();
    }
}
