package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.PurchaseDAO;
import cz.muni.fi.pa165.model.dto.PurchaseDTO;
import cz.muni.fi.pa165.repository.PurchaseRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PurchaseServiceImplTest {

    @Mock
    private PurchaseRepository purchaseRepository;

    @InjectMocks
    private PurchaseServiceImpl purchaseService;

    @Test
    void purchaseWineBottle_validPurchase_returnsPurchase() {
        // Arrange
        PurchaseDTO purchaseDTO = createSamplePurchaseDTO();
        PurchaseDAO expectedPurchase = createSamplePurchaseDAO();
        when(purchaseRepository.save(any(PurchaseDAO.class))).thenReturn(expectedPurchase);

        // Act
        PurchaseDAO result = purchaseService.purchaseWineBottle(purchaseDTO);

        // Assert
        assertEquals(expectedPurchase, result);
        verify(purchaseRepository, times(1)).save(any(PurchaseDAO.class));
    }

    @Test
    void purchaseMultipleWineBottles_validPurchases_returnsListOfPurchases() {
        // Arrange
        List<PurchaseDTO> purchaseDTOList = Arrays.asList(createSamplePurchaseDTO());
        List<PurchaseDAO> expectedPurchases = Arrays.asList(createSamplePurchaseDAO());
        when(purchaseRepository.saveAll(any())).thenReturn(expectedPurchases);

        // Act
        List<PurchaseDAO> result = purchaseService.purchaseMultipleWineBottles(purchaseDTOList);

        // Assert
        assertEquals(expectedPurchases, result);
        verify(purchaseRepository, times(1)).saveAll(any());
    }

    // Helper methods to create sample instances
    private PurchaseDTO createSamplePurchaseDTO() {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setId("id");
        return purchaseDTO;
    }

    private PurchaseDAO createSamplePurchaseDAO() {
        PurchaseDAO purchaseDAO = new PurchaseDAO();
        purchaseDAO.setId("id");
        return purchaseDAO;
    }
}
