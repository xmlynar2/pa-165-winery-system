package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dto.CustomerDTO;
import cz.muni.fi.pa165.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * @author Patrik Palovcik
 */
@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Test
    void findCustomerById_existingCustomer_returnsCustomer() {
        // Arrange
        long customerId = 1L;
        CustomerDAO expectedCustomer = createSampleCustomerDAO();
        Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.of(expectedCustomer));

        // Act
        CustomerDAO foundCustomer = customerService.findCustomerById(customerId);

        // Assert
        assertEquals(expectedCustomer, foundCustomer);
        verify(customerRepository, times(1)).findById(customerId); // Verify that findById method was called once with the correct argument
    }

    @Test
    void findCustomerById_nonExistingCustomer_returnsNull() {
        // Arrange
        long nonExistingCustomerId = 100L;
        Mockito.when(customerRepository.findById(nonExistingCustomerId)).thenReturn(Optional.empty());

        // Assert
        assertThrows(ResourceNotFoundException.class, () -> customerService.findCustomerById(nonExistingCustomerId));
    }

    @Test
    void createCustomer_validCustomerDTO_savesSuccessfully() {
        // Arrange
        CustomerDTO customerDTO = createSampleCustomerDTO();
        CustomerDAO savedCustomer = new CustomerDAO();
        Mockito.when(customerRepository.save(any(CustomerDAO.class))).thenReturn(savedCustomer);

        // Act
        customerService.createCustomer(customerDTO);

        // Assert
        verify(customerRepository, times(1)).save(any(CustomerDAO.class));
    }

    private CustomerDAO createSampleCustomerDAO() {
        CustomerDAO customerDAO = new CustomerDAO();
        customerDAO.setId(1L);
        customerDAO.setFirstName("John");
        customerDAO.setLastName("Doe");
        customerDAO.setEmail("john.doe@example.com");
        customerDAO.setAddress("123 Main St, Anytown");
        return customerDAO;
    }

    private CustomerDTO createSampleCustomerDTO() {
        return new CustomerDTO(1L, "John", "Doe", "john.doe@example.com", "123 Main St, Anytown");
    }
}
