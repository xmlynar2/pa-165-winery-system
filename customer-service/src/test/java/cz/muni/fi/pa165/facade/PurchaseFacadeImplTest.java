package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.client.InventoryManagementClient;
import cz.muni.fi.pa165.model.dao.PurchaseDAO;
import cz.muni.fi.pa165.model.dto.PurchaseDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import cz.muni.fi.pa165.service.PurchaseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PurchaseFacadeImplTest {

    @Mock
    private PurchaseService purchaseService;

    @Mock
    private InventoryManagementClient inventoryClient;

    @InjectMocks
    private PurchaseFacadeImpl purchaseFacade;

    @Test
    void purchaseWineBottle() {
        // Given
        PurchaseDTO purchaseDTO = new PurchaseDTO();

        // Mocking behavior
        when(purchaseService.purchaseWineBottle(any(PurchaseDTO.class))).thenReturn(new PurchaseDAO());

        // When
        PurchaseDTO result = purchaseFacade.purchaseWineBottle(purchaseDTO);

        // Then
        assertEquals(purchaseDTO, result);
        verify(inventoryClient, times(1)).updateWineStock(any(TransactionDTO.class));
    }

    @Test
    void purchaseWineBottles() {
        // Given
        List<PurchaseDTO> purchases = new ArrayList<>();
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchases.add(purchaseDTO);

        // Mocking behavior
        when(purchaseService.purchaseMultipleWineBottles(anyList())).thenReturn(List.of(new PurchaseDAO()));

        // When
        List<PurchaseDTO> result = purchaseFacade.purchaseWineBottles(purchases);

        // Then
        assertEquals(purchases, result);
        verify(inventoryClient, times(1)).updateWineStock(any(TransactionDTO.class));
    }
}
