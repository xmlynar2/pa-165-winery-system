package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.InvoiceDAO;
import cz.muni.fi.pa165.model.dto.InvoiceDTO;
import cz.muni.fi.pa165.repository.InvoiceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InvoiceServiceImplTest {
    @Mock
    private InvoiceRepository invoiceRepository;

    @InjectMocks
    private InvoiceServiceImpl invoiceService;

    @Test
    void createInvoice_createsAndReturnsInvoice() {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        CustomerDAO customerDAO = new CustomerDAO();
        InvoiceDAO expectedInvoice = new InvoiceDAO();
        when(invoiceRepository.save(any(InvoiceDAO.class))).thenReturn(expectedInvoice);

        InvoiceDAO actualInvoice = invoiceService.createInvoice(invoiceDTO, customerDAO);

        assertEquals(expectedInvoice, actualInvoice);
    }

    @Test
    void getInvoiceById_existingId_returnsInvoice() {
        String id = "1";
        InvoiceDAO expectedInvoice = new InvoiceDAO();
        when(invoiceRepository.findById(id)).thenReturn(Optional.of(expectedInvoice));

        InvoiceDAO actualInvoice = invoiceService.getInvoiceById(id);

        assertEquals(expectedInvoice, actualInvoice);
    }

    @Test
    void getInvoiceById_nonExistingId_throwsResourceNotFoundException() {
        String id = "1";
        when(invoiceRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> invoiceService.getInvoiceById(id));
    }

    @Test
    void getInvoicesByCustomerId_existingCustomer_returnsInvoices() {
        Long customerId = 1L;
        List<InvoiceDAO> expectedInvoices = List.of(new InvoiceDAO());
        when(invoiceRepository.findByCustomerId(customerId, Pageable.ofSize(10).withPage(0))).thenReturn(new PageImpl<>(expectedInvoices));

        List<InvoiceDAO> actualInvoices = invoiceService.getInvoicesByCustomerId(customerId, 0, 10);

        assertEquals(expectedInvoices, actualInvoices);
    }

    @Test
    void getInvoicesByCustomerId_nonExistingCustomer_returnsEmptyList() {
        Long customerId = 1L;
        when(invoiceRepository.findByCustomerId(customerId, Pageable.ofSize(10).withPage(0))).thenReturn(new PageImpl<>(List.of()));

        List<InvoiceDAO> actualInvoices = invoiceService.getInvoicesByCustomerId(customerId, 0, 10);

        assertTrue(actualInvoices.isEmpty());
    }
}
