package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.FeedbackFacade;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.time.Instant;


import org.mockito.Mock;
import org.springframework.dao.DataIntegrityViolationException;


import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class FeedbackControllerTest {

    @Mock
    private FeedbackFacade feedbackFacade;

    @InjectMocks
    private FeedbackController feedbackController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsFeedbackWhenPresent() {
        FeedbackDTO feedbackDTO = new FeedbackDTO(1L, 0.0d, Instant.now(), "Sample feedback text", 1L);
        when(feedbackFacade.findById(1L)).thenReturn(feedbackDTO);

        FeedbackDTO result = feedbackController.getFeedbackById(1L);

        assertEquals(feedbackDTO, result);
    }

    @Test
    void getByIdReturnsThrowsResourceNotFound() {
        when(feedbackFacade.findById(1L)).thenThrow(new ResourceNotFoundException("Feedback with id 1 not found"));

        assertThrows(ResourceNotFoundException.class, () ->
                feedbackController.getFeedbackById(1L)
        );
    }

    @Test
    void createReturnsCreatedFeedback() {
        FeedbackDTO feedbackDTO = new FeedbackDTO(1L, 0.0d, Instant.now(), "Sample feedback text", 1L);

        // Mocking the behavior of feedbackFacade.createFeedback() to return the same feedbackDTO
        doNothing().when(feedbackFacade).createFeedback(feedbackDTO, 1L);

        // Call the method under test
        feedbackController.createFeedback(1L, feedbackDTO);

        // Verify that feedbackFacade.createFeedback() is called with the correct parameters
        verify(feedbackFacade, times(1)).createFeedback(feedbackDTO, 1L);
    }

    @Test
    void createFeedbackThrowsConflict() {
        FeedbackDTO feedbackDTO = new FeedbackDTO(1L, 0.0d, Instant.now(), "Sample feedback text", 1L);

        doThrow(new DataIntegrityViolationException("Feedback already exists"))
                .when(feedbackFacade).createFeedback(feedbackDTO, 1L);

        assertThrows(DataIntegrityViolationException.class, () ->
            feedbackController.createFeedback(1L, feedbackDTO)
        );
    }

    @Test
    void updateReturnsUpdatedFeedback() {
        FeedbackDTO feedbackDTO = new FeedbackDTO(1L, 0.0d, Instant.now(), "Sample feedback text", 1L);
        when(feedbackFacade.updateFeedback(feedbackDTO)).thenReturn(feedbackDTO);

        FeedbackDTO result = feedbackController.updateFeedback(feedbackDTO);

        assertEquals(feedbackDTO, result);
    }

    @Test
    void updateFeedbackThrowsNotFound() {
        FeedbackDTO feedbackDTO = new FeedbackDTO(1L, 0.0d, Instant.now(), "Sample feedback text", 1L);

        doThrow(new ResourceNotFoundException("Feedback not found"))
                .when(feedbackFacade).updateFeedback(feedbackDTO);

        assertThrows(ResourceNotFoundException.class, () ->
            feedbackController.updateFeedback(feedbackDTO)
        );
    }
}
