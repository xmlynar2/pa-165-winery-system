package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.CustomerFacade;
import cz.muni.fi.pa165.model.dto.CustomerDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class CustomerControllerTest {

    @Mock
    private CustomerFacade customerFacade;

    @InjectMocks
    private CustomerController customerController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createReturnsCreatedCustomer() {
        CustomerDTO customerDTO = new CustomerDTO(1L, "John", "Doe", "john.doe@example.com", "123 Main St");

        // Mocking the behavior of customerService.createCustomer to not throw any exceptions
        doNothing().when(customerFacade).createCustomer(any(CustomerDTO.class));

        // Call the method under test
        customerController.createCustomer(customerDTO);

        // Verify that customerService.createCustomer() is called with the correct parameters
        verify(customerFacade, times(1)).createCustomer(eq(customerDTO));
    }

    @Test
    void createCustomerHandlesDataIntegrityViolation() {
        CustomerDTO customerDTO = new CustomerDTO(1L, "John", "Doe", "john.doe@example.com", "123 Main St");

        // Mocking the behavior of customerFacade.createCustomer to throw a DataIntegrityViolationException
        doThrow(new DataIntegrityViolationException("Data integrity violation")).when(customerFacade).createCustomer(any(CustomerDTO.class));

        // Call the method under test and catch any exception
        try {
            customerController.createCustomer(customerDTO);
            fail("Expected DataIntegrityViolationException was not thrown");
        } catch (DataIntegrityViolationException e) {
            // Verify that the exception was properly handled
            verify(customerFacade, times(1)).createCustomer(eq(customerDTO));
            assertEquals("Data integrity violation", e.getMessage());
        }
    }

    @Test
    void createHandlesFailure() {
        CustomerDTO customerDTO = new CustomerDTO(1L, "John", "Doe", "john.doe@example.com", "123 Main St");

        // Mocking the behavior of customerService.createCustomer to throw an exception
        doThrow(new RuntimeException("Failed to create customer")).when(customerFacade).createCustomer(any(CustomerDTO.class));

        // Call the method under test and catch any exception
        try {
            customerController.createCustomer(customerDTO);
            fail("Expected RuntimeException was not thrown");
        } catch (RuntimeException e) {
            // Verify that the exception was properly handled
            verify(customerFacade, times(1)).createCustomer(eq(customerDTO));
            assertEquals("Failed to create customer", e.getMessage());
        }
    }

    @Test
    void getCustomerByIdReturnsCustomer() {
        Long customerId = 1L;
        CustomerDTO expectedCustomer = new CustomerDTO(customerId, "John", "Doe", "john.doe@example.com", "123 Main St");

        when(customerFacade.findCustomerById(customerId)).thenReturn(expectedCustomer);

        CustomerDTO actualCustomer = customerController.getCustomerById(customerId);

        verify(customerFacade, times(1)).findCustomerById(customerId);
        assertEquals(expectedCustomer, actualCustomer);
    }

    @Test
    void getCustomerByIdWithNonExistingId() {
        Long customerId = 1L;

        when(customerFacade.findCustomerById(customerId)).thenThrow(new ResourceNotFoundException("Customer not found"));

        try {
            customerController.getCustomerById(customerId);
            fail("Expected ResourceNotFoundException was not thrown");
        } catch (ResourceNotFoundException e) {
            verify(customerFacade, times(1)).findCustomerById(customerId);
            assertEquals("Customer not found", e.getMessage());
        }
    }

    @Test
    void getCustomerByIdWithNullId() {
        when(customerFacade.findCustomerById(null)).thenThrow(new IllegalArgumentException("Customer ID cannot be null"));
        try {
            customerController.getCustomerById(null);
            fail("Expected IllegalArgumentException was not thrown");
        } catch (IllegalArgumentException e) {
            verify(customerFacade, times(1)).findCustomerById(null);
            assertEquals("Customer ID cannot be null", e.getMessage());
        }
    }
}
