package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.client.InventoryManagementClient;
import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.InvoiceDAO;
import cz.muni.fi.pa165.model.dao.InvoiceItemDAO;
import cz.muni.fi.pa165.model.dto.InvoiceDTO;
import cz.muni.fi.pa165.model.dto.WineDTO;
import cz.muni.fi.pa165.service.CustomerService;
import cz.muni.fi.pa165.service.InvoiceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class InvoiceFacadeImplTest {

    @Mock
    private InvoiceService invoiceService;

    @Mock
    private CustomerService customerService;

    @Mock
    private InventoryManagementClient inventoryManagementClient;

    @InjectMocks
    private InvoiceFacadeImpl invoiceFacade;

    @Test
    void createInvoiceReturnsInvoice() {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        Long customerId = 1L;
        CustomerDAO customerDAO = new CustomerDAO();
        InvoiceDAO invoiceDAO = new InvoiceDAO();

        when(customerService.findCustomerById(customerId)).thenReturn(customerDAO);
        when(invoiceService.createInvoice(invoiceDTO, customerDAO)).thenReturn(invoiceDAO);

        InvoiceDTO actualInvoice = invoiceFacade.createInvoice(invoiceDTO, customerId);

        verify(customerService, times(1)).findCustomerById(customerId);
        verify(invoiceService, times(1)).createInvoice(invoiceDTO, customerDAO);
        assertNotNull(actualInvoice);
    }

    @Test
    void createInvoiceWithNonExistingCustomerThrowsException() {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        Long customerId = 1L;

        when(customerService.findCustomerById(customerId)).thenThrow(new ResourceNotFoundException("Customer not found"));

        assertThrows(ResourceNotFoundException.class, () ->
            invoiceFacade.createInvoice(invoiceDTO, customerId)
        );
    }

    @Test
    void getInvoiceByIdReturnsInvoice() {
        String invoiceId = "1";
        InvoiceDTO expectedInvoice = new InvoiceDTO();
        InvoiceDAO invoiceDAO = new InvoiceDAO();

        when(invoiceService.getInvoiceById(invoiceId)).thenReturn(invoiceDAO);

        InvoiceDTO actualInvoice = invoiceFacade.getInvoiceById(invoiceId);

        verify(invoiceService, times(1)).getInvoiceById(invoiceId);
        assertEquals(expectedInvoice, actualInvoice);
    }

    @Test
    void getInvoiceByIdWithNonExistingIdThrowsNotFound() {
        String invoiceId = "1";

        when(invoiceService.getInvoiceById(invoiceId)).thenThrow(new ResourceNotFoundException("Invoice not found"));

        assertThrows(ResourceNotFoundException.class, () -> {
            invoiceFacade.getInvoiceById(invoiceId);
        });
    }

    @Test
    void getInvoicesByCustomerIdReturnsInvoices() {
        Long customerId = 1L;
        int page = 0;
        int size = 5;
        InvoiceDAO invoiceDAO = new  InvoiceDAO();

        when(invoiceService.getInvoicesByCustomerId(customerId, page, size)).thenReturn(List.of(invoiceDAO));

        List<InvoiceDTO> actualInvoices = invoiceFacade.getInvoicesByCustomerId(customerId, page, size);

        verify(invoiceService, times(1)).getInvoicesByCustomerId(customerId, page, size);
        assertFalse(actualInvoices.isEmpty());
    }

    @Test
    void getInvoicesByCustomerIdWithNonExistingIdReturnsEmptyList() {
        Long customerId = 1L;
        int page = 0;
        int size = 5;

        when(invoiceService.getInvoicesByCustomerId(customerId, page, size)).thenReturn(List.of());

        List<InvoiceDTO> actualInvoices = invoiceFacade.getInvoicesByCustomerId(customerId, page, size);

        verify(invoiceService, times(1)).getInvoicesByCustomerId(customerId, page, size);
        assertTrue(actualInvoices.isEmpty());
    }

    @Test
    void generateInvoiceDocumentReturnsDocument() {
        String invoiceId = "1";
        InvoiceDAO invoiceDAO = new InvoiceDAO();
        InvoiceItemDAO invoiceItemDAO = new InvoiceItemDAO();
        invoiceDAO.setItems(List.of(invoiceItemDAO));
        WineDTO wineDTO = new WineDTO();

        when(invoiceService.getInvoiceById(invoiceId)).thenReturn(invoiceDAO);
        when(inventoryManagementClient.getWineProduct(anyLong())).thenReturn(wineDTO);

        byte[] actualDocument = invoiceFacade.generateInvoiceDocument(invoiceId);

        verify(invoiceService, times(1)).getInvoiceById(invoiceId);
        verify(inventoryManagementClient, times(1)).getWineProduct(anyLong());
        assertNotNull(actualDocument);
    }

    @Test
    void generateInvoiceDocumentWithNonExistingIdThrowsNotFound() {
        String invoiceId = "1";

        when(invoiceService.getInvoiceById(invoiceId)).thenThrow(new ResourceNotFoundException("Invoice not found"));

        assertThrows(ResourceNotFoundException.class, () ->
            invoiceFacade.generateInvoiceDocument(invoiceId)
        );
    }
}
