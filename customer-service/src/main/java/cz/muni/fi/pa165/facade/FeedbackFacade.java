package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dto.FeedbackDTO;

import java.util.List;


public interface FeedbackFacade {
    FeedbackDTO findById(Long feedbackId);

    List<FeedbackDTO> findByCustomerId(Long customerId, int page, int size);

    List<FeedbackDTO> findByWineId(Long wineId, int page, int size);

    void createFeedback(FeedbackDTO feedbackDTO, Long customerId);

    FeedbackDTO updateFeedback(FeedbackDTO feedbackDTO);
}
