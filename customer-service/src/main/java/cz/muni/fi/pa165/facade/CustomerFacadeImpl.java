package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dto.CustomerDTO;
import cz.muni.fi.pa165.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerFacadeImpl implements cz.muni.fi.pa165.facade.CustomerFacade {

    private final CustomerService customerService;

    @Autowired
    public CustomerFacadeImpl(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public CustomerDTO findCustomerById(Long id) {
        return mapToCustomerDTO(customerService.findCustomerById(id));
    }

    @Override
    public void createCustomer(CustomerDTO customerDTO) {
        customerService.createCustomer(customerDTO);
    }

    private CustomerDTO mapToCustomerDTO(CustomerDAO customerDAO) {
        return new CustomerDTO(
                customerDAO.getId(),
                customerDAO.getFirstName(),
                customerDAO.getLastName(),
                customerDAO.getEmail(),
                customerDAO.getAddress()
        );
    }
}
