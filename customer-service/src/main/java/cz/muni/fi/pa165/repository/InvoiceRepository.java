package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.InvoiceDAO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<InvoiceDAO, String> {
    @Query("SELECT i FROM InvoiceDAO i WHERE i.customer.id = :customerId")
    Page<InvoiceDAO> findByCustomerId(Long customerId, Pageable pageable);
}
