package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dto.PurchaseDTO;

import java.util.List;

public interface PurchaseFacade {
    PurchaseDTO purchaseWineBottle(PurchaseDTO purchase);

    List<PurchaseDTO> purchaseWineBottles(List<PurchaseDTO> purchases);
}
