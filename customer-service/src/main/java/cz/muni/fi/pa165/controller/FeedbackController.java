package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.FeedbackFacade;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Feedback API", description = "Provides API for manipulating with feedbacks on wine bottles.")
@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    private final FeedbackFacade feedbackFacade;

    @Autowired
    public FeedbackController(FeedbackFacade feedbackFacade) {
        this.feedbackFacade = feedbackFacade;
    }

    @Operation(summary = "Get feedback detail", description = "Returns detail of feedback by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the feedback",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FeedbackDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Feedback not found",
                    content = @Content(mediaType = "application/json"))
    })
    @GetMapping("/{feedbackId}")
    public FeedbackDTO getFeedbackById(
            @Parameter(description = "Feedback ID", required = true) @PathVariable Long feedbackId
    ) {
        return feedbackFacade.findById(feedbackId);
    }

    @Operation(summary = "Get customer feedbacks details", description = "Returns all customer feedbacks details.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All feedbacks of customer returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = List.class))})
    })
    @GetMapping("/customer")
    public List<FeedbackDTO> getFeedbackByCustomerId(
            @Parameter(description = "Customer ID", required = true) @RequestParam Long customerId,
            @Parameter(description = "Page") @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "Size") @RequestParam(defaultValue = "10") int size
    ) {
        return feedbackFacade.findByCustomerId(customerId, page, size);
    }

    @Operation(summary = "Get all feedbacks of a wine", description = "Return all feedbacks of a wine by wine Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All feedbacks of wine returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = List.class))})
    })
    @GetMapping("/wine-bottle")
    public List<FeedbackDTO> getFeedbackByWineBottleId(
            @Parameter(description = "External id of wine") @RequestParam Long wineBottleId,
            @Parameter(description = "Page") @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "Size") @RequestParam(defaultValue = "10") int size
    ) {
        return feedbackFacade.findByWineId(wineBottleId, page, size);
    }

    @Operation(summary = "Create feedback", description = "Create feedback as a customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created feedback",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "Feedback already exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping("/{customerId}")
    public ResponseEntity<String> createFeedback(
            @Parameter(description = "Customer ID", required = true) @PathVariable Long customerId,
            @Parameter(description = "Feedback to create", required = true) @RequestBody FeedbackDTO feedback
    ) {
        feedbackFacade.createFeedback(feedback, customerId);
        return ResponseEntity.status(201).body("Feedback created");
    }

    @Operation(summary = "Update feedback", description = "Update feedback by external Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Feedback updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FeedbackDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Feedback not found",
                    content = @Content(mediaType = "application/json"))
    })
    @PutMapping
    public FeedbackDTO updateFeedback(
            @Parameter(description = "Feedback data to update", required = true) @RequestBody FeedbackDTO feedback
    ) {
        return feedbackFacade.updateFeedback(feedback);
    }
}
