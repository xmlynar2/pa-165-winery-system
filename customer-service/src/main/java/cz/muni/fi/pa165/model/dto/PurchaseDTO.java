package cz.muni.fi.pa165.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.Instant;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class PurchaseDTO {
    @NotNull
    private String id;
    @NotNull
    private Long customerId;
    private Instant purchasedAt;
    @NotNull
    private String wineBottleExtId;
    private int quantity;
    private double price;
}
