package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;
import cz.muni.fi.pa165.service.CustomerService;
import cz.muni.fi.pa165.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackFacadeImpl implements FeedbackFacade {

    private final FeedbackService feedbackService;
    private final CustomerService customerService;

    @Autowired
    public FeedbackFacadeImpl(FeedbackService feedbackService, CustomerService customerService) {
        this.feedbackService = feedbackService;
        this.customerService = customerService;
    }


    @Override
    public FeedbackDTO findById(Long feedbackId) {
        return mapToFeedbackDTO(feedbackService.findById(feedbackId));
    }

    @Override
    public List<FeedbackDTO> findByCustomerId(Long customerId, int page, int size) {
        return feedbackService.findByCustomerId(customerId, page, size).stream().map(this::mapToFeedbackDTO).toList();
    }

    @Override
    public List<FeedbackDTO> findByWineId(Long wineId, int page, int size) {
        return feedbackService.findByWineBottleId(wineId, page, size).stream().map(this::mapToFeedbackDTO).toList();
    }

    @Override
    public void createFeedback(FeedbackDTO feedbackDTO, Long customerId) {
        CustomerDAO customer = customerService.findCustomerById(customerId);
        feedbackService.createFeedback(feedbackDTO, customer);
    }

    @Override
    public FeedbackDTO updateFeedback(FeedbackDTO feedbackDTO) {
        return mapToFeedbackDTO(feedbackService.updateFeedbackById(feedbackDTO));
    }

    private FeedbackDTO mapToFeedbackDTO(FeedbackDAO feedbackDAO) {
        return new FeedbackDTO(
                feedbackDAO.getId(),
                feedbackDAO.getRating(),
                feedbackDAO.getCreatedAt(),
                feedbackDAO.getFeedbackText(),
                feedbackDAO.getWineExtId()
        );
    }
}
