package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dto.InvoiceDTO;

import java.util.List;

public interface InvoiceFacade {
    InvoiceDTO createInvoice(InvoiceDTO invoiceDTO, Long customerId);

    InvoiceDTO getInvoiceById(String id);

    List<InvoiceDTO> getInvoicesByCustomerId(Long customerId, int page, int size);

    byte[] generateInvoiceDocument(String id);
}
