package cz.muni.fi.pa165.model.dao;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;
import java.util.List;


@Setter
@Getter
@EqualsAndHashCode
@ToString(exclude = {"customer"})
@NoArgsConstructor
@Entity
@Table(name = "invoice")
public class InvoiceDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "invoice_id")
    private String id;
    @Column(name = "price")
    private double price;
    @Column(name = "description")
    private String description;
    @Column(name = "date")
    private Instant issuedDate;
    @Column(name = "due_date")
    private Instant dueDate;
    @Column(name = "email")
    private String email;
    @Column(name = "phone")
    private String phone;
    @Column(name = "zip_code")
    private String zipCode;
    @Column(name = "address")
    private String address;
    @Column(name = "city")
    private String city;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private CustomerDAO customer;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<InvoiceItemDAO> items;
}
