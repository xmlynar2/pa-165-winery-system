package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dto.CustomerDTO;

public interface CustomerService {
    CustomerDAO findCustomerById(Long id);

    void createCustomer(CustomerDTO customerDTO);
}
