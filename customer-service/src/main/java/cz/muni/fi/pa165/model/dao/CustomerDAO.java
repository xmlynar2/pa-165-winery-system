package cz.muni.fi.pa165.model.dao;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;


@Setter
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Entity
@Table(name = "customer")
public class CustomerDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email", unique = true)
    private String email;
    @Column(name = "address")
    private String address;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private List<FeedbackDAO> feedbacks;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer",
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE})
    private List<InvoiceDAO> invoices;
}
