package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dto.CustomerDTO;

public interface CustomerFacade {
    CustomerDTO findCustomerById(Long id);

    void createCustomer(CustomerDTO customerDTO);
}
