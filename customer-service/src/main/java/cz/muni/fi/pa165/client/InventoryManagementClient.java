package cz.muni.fi.pa165.client;

import cz.muni.fi.pa165.model.dto.TransactionDTO;
import cz.muni.fi.pa165.model.dto.WineDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "InventoryManagementClient", url = "${inventory-management.url}")
public interface InventoryManagementClient {
    @GetMapping("/wine/code/{wineExtId}")
    WineDTO getWineProduct(@PathVariable Long wineExtId);

    @PostMapping("/transaction/create")
    void updateWineStock(@RequestBody TransactionDTO wine);
}
