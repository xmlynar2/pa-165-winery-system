package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.client.InventoryManagementClient;
import cz.muni.fi.pa165.model.dao.PurchaseDAO;
import cz.muni.fi.pa165.model.dto.PurchaseDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import cz.muni.fi.pa165.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurchaseFacadeImpl implements PurchaseFacade {

    private final PurchaseService purchaseService;
    private final InventoryManagementClient inventoryClient;

    @Autowired
    public PurchaseFacadeImpl(PurchaseService purchaseService, InventoryManagementClient inventoryManagementClient) {
        this.purchaseService = purchaseService;
        this.inventoryClient = inventoryManagementClient;
    }

    @Override
    public PurchaseDTO purchaseWineBottle(PurchaseDTO purchase) {
        TransactionDTO transaction = new TransactionDTO();
        transaction.setCustomerId(purchase.getCustomerId());
        transaction.setWineExtId(purchase.getWineBottleExtId());
        transaction.setQuantity(purchase.getQuantity());
        inventoryClient.updateWineStock(transaction);

        return mapDAOtoDTO(purchaseService.purchaseWineBottle(purchase));
    }

    @Override
    public List<PurchaseDTO> purchaseWineBottles(List<PurchaseDTO> purchases) {
        // Note: this might need a batch send and batch update
        purchases.forEach(purchase -> {
            TransactionDTO transaction = new TransactionDTO();
            transaction.setCustomerId(purchase.getCustomerId());
            transaction.setWineExtId(purchase.getWineBottleExtId());
            transaction.setQuantity(purchase.getQuantity());
            inventoryClient.updateWineStock(transaction);
        });

        return purchaseService.purchaseMultipleWineBottles(purchases).stream().map(this::mapDAOtoDTO).toList();
    }

    private PurchaseDTO mapDAOtoDTO(PurchaseDAO purchase) {
        PurchaseDTO purchaseDTO = new PurchaseDTO();
        purchaseDTO.setId(purchase.getId());
        purchaseDTO.setCustomerId(purchase.getCustomerId());
        purchaseDTO.setPurchasedAt(purchase.getPurchasedAt());
        purchaseDTO.setWineBottleExtId(purchase.getWineBottleExtId());
        purchaseDTO.setQuantity(purchase.getQuantity());
        purchaseDTO.setPrice(purchase.getPrice());
        return purchaseDTO;
    }
}
