package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.PurchaseDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends JpaRepository<PurchaseDAO, String> {
}
