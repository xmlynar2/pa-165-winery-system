package cz.muni.fi.pa165.model.dao;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@Entity
@Table(name = "purchase_history")
public class PurchaseDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "purchase_id")
    private String id;
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "purchased_at")
    private Instant purchasedAt;
    @Column(name = "wine_ext_id")
    private String wineBottleExtId;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "price")
    private double price;
}
