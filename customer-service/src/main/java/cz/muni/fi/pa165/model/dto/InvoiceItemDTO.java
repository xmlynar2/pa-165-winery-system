package cz.muni.fi.pa165.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class InvoiceItemDTO {
    private String name;
    private int quantity;
    private double price;
    @NotNull
    private long wineExtId;
}
