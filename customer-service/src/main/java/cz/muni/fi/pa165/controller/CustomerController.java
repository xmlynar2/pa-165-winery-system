package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.CustomerFacade;
import cz.muni.fi.pa165.model.dto.CustomerDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Customer API", description = "Provides API to create a customer.")
@RestController
@RequestMapping("/customer")
public class CustomerController {
    // Note: this might not be needed and might be a user api

    private final CustomerFacade customerFacade;

    @Autowired
    public CustomerController(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    @Operation(summary = "Create customer", description = "Creates a customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created customer",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "Customer already exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping
    public ResponseEntity<String> createCustomer(
            @Parameter(description = "Customer data", required = true) @RequestBody CustomerDTO customerDTO
    ) {
        customerFacade.createCustomer(customerDTO);
        return ResponseEntity.status(201).body("Customer created");
    }

    @Operation(summary = "Get customer", description = "Get customer by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned customer",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Customer not found",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/{customerId}")
    public CustomerDTO getCustomerById(
            @Parameter(description = "Customer ID", required = true) @PathVariable Long customerId
    ) {
        return customerFacade.findCustomerById(customerId);
    }
}
