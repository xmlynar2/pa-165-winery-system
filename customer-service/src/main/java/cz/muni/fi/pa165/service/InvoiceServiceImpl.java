package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.InvoiceDAO;
import cz.muni.fi.pa165.model.dao.InvoiceItemDAO;
import cz.muni.fi.pa165.model.dto.InvoiceDTO;
import cz.muni.fi.pa165.model.dto.InvoiceItemDTO;
import cz.muni.fi.pa165.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class InvoiceServiceImpl implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    @Autowired
    public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    @Transactional
    public InvoiceDAO createInvoice(InvoiceDTO invoice, CustomerDAO customerDAO) {
        return invoiceRepository.save(mapToInvoiceDAO(invoice, customerDAO));
    }

    @Override
    public InvoiceDAO getInvoiceById(String id) {
        return invoiceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Invoice with id " + id + " not found"));
    }

    @Override
    public List<InvoiceDAO> getInvoicesByCustomerId(Long customerId, int page, int size) {
        Pageable pageable = Pageable.ofSize(size).withPage(page);
        return invoiceRepository.findByCustomerId(customerId, pageable).getContent();
    }

    private InvoiceDAO mapToInvoiceDAO(InvoiceDTO invoiceDTO, CustomerDAO customerDAO) {
        InvoiceDAO invoiceDAO = new InvoiceDAO();
        invoiceDAO.setPrice(invoiceDTO.getPrice());
        invoiceDAO.setDescription(invoiceDTO.getDescription());
        invoiceDAO.setIssuedDate(invoiceDTO.getIssuedDate());
        invoiceDAO.setDueDate(invoiceDTO.getDueDate());
        invoiceDAO.setEmail(invoiceDTO.getEmail());
        invoiceDAO.setPhone(invoiceDTO.getPhone());
        invoiceDAO.setZipCode(invoiceDTO.getZipCode());
        invoiceDAO.setAddress(invoiceDTO.getAddress());
        invoiceDAO.setCity(invoiceDTO.getCity());
        invoiceDAO.setCustomer(customerDAO);
        if (invoiceDTO.getItems() == null) {
            return invoiceDAO;
        }
        invoiceDAO.setItems(invoiceDTO.getItems().stream().map(this::mapToInvoiceItemDAO).toList());
        return invoiceDAO;
    }

    private InvoiceItemDAO mapToInvoiceItemDAO(InvoiceItemDTO invoiceItemDTO) {
        InvoiceItemDAO invoiceItemDAO = new InvoiceItemDAO();
        invoiceItemDAO.setName(invoiceItemDTO.getName());
        invoiceItemDAO.setPrice(invoiceItemDTO.getPrice());
        invoiceItemDAO.setQuantity(invoiceItemDTO.getQuantity());
        invoiceItemDAO.setWineExtId(invoiceItemDTO.getWineExtId());
        return invoiceItemDAO;
    }
}
