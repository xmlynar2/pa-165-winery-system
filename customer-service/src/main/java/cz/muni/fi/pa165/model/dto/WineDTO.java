package cz.muni.fi.pa165.model.dto;

import lombok.Data;

@Data
public class WineDTO {
    private String code;
    private String name;
    private String description;
    private int quantity;
    private double price;
}
