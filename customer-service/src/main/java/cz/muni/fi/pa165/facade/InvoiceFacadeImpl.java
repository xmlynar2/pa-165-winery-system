package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.client.InventoryManagementClient;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.InvoiceDAO;
import cz.muni.fi.pa165.model.dao.InvoiceItemDAO;
import cz.muni.fi.pa165.model.dto.InvoiceDTO;
import cz.muni.fi.pa165.model.dto.InvoiceItemDTO;
import cz.muni.fi.pa165.model.dto.WineDTO;
import cz.muni.fi.pa165.service.CustomerService;
import cz.muni.fi.pa165.service.InvoiceService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.Standard14Fonts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InvoiceFacadeImpl implements InvoiceFacade {

    private final InvoiceService invoiceService;
    private final CustomerService customerService;
    private final InventoryManagementClient inventoryManagementClient;

    @Autowired
    public InvoiceFacadeImpl(InvoiceService invoiceService, CustomerService customerService, InventoryManagementClient inventoryManagementClient) {
        this.invoiceService = invoiceService;
        this.customerService = customerService;
        this.inventoryManagementClient = inventoryManagementClient;
    }

    @Override
    public InvoiceDTO createInvoice(InvoiceDTO invoiceDTO, Long customerId) {
        CustomerDAO customer = customerService.findCustomerById(customerId);
        return mapToInvoiceDTO(invoiceService.createInvoice(invoiceDTO, customer));
    }

    @Override
    public InvoiceDTO getInvoiceById(String id) {
        return mapToInvoiceDTO(invoiceService.getInvoiceById(id));
    }

    @Override
    public List<InvoiceDTO> getInvoicesByCustomerId(Long customerId, int page, int size) {
        return invoiceService.getInvoicesByCustomerId(customerId, page, size).stream().map(this::mapToInvoiceDTO).collect(Collectors.toList());
    }

    @Override
    public byte[] generateInvoiceDocument(String invoiceId) {
        InvoiceDAO invoice = invoiceService.getInvoiceById(invoiceId);
        if (invoice.getItems().isEmpty()) {
            throw new RuntimeException("Invoice has no items");
        }
        List<WineDTO> wines = invoice.getItems().stream().map(item -> inventoryManagementClient.getWineProduct(item.getWineExtId())).toList();
        byte[] contentBytes;
        try (PDDocument document = new PDDocument()) {
            PDPage page = new PDPage();
            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            contentStream.setFont(new PDType1Font(Standard14Fonts.FontName.HELVETICA), 18);
            contentStream.beginText();
            contentStream.showText("Invoice of item " + invoiceId);
            contentStream.newLine();
            contentStream.setFont(new PDType1Font(Standard14Fonts.FontName.HELVETICA), 12);
            addInvoiceDetailsToDocument(contentStream, invoice);
            addInvoiceItemsToDocument(contentStream, wines);
            contentStream.endText();
            contentStream.close();


            PDStream content = new PDStream(document);
            OutputStream out = content.createOutputStream();
            document.save(out);
            out.close();
            contentBytes = content.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Failed to generate invoice document", e);
        }
        return contentBytes;
    }

    private void addInvoiceDetailsToDocument(PDPageContentStream contentStream, InvoiceDAO invoice) throws IOException {
        contentStream.showText("Customer details:");
        contentStream.newLine();
        contentStream.showText("Email: " + invoice.getEmail());
        contentStream.newLine();
        contentStream.showText("Phone: " + invoice.getPhone());
        contentStream.newLine();
        contentStream.showText("Zip code: " + invoice.getZipCode());
        contentStream.newLine();
        contentStream.showText("Address: " + invoice.getAddress());
        contentStream.newLine();
        contentStream.showText("City: " + invoice.getCity());
        contentStream.newLine();

        contentStream.showText("Description: " + invoice.getDescription());
        contentStream.newLine();
        contentStream.showText("Issued date: " + invoice.getIssuedDate());
        contentStream.showText("Due date: " + invoice.getDueDate());
        contentStream.newLine();
        contentStream.showText("Total Price: " + invoice.getPrice());
        contentStream.newLine();
    }

    private void addInvoiceItemsToDocument(PDPageContentStream contentStream, List<WineDTO> items) throws IOException {
        contentStream.showText("Items:");
        for (WineDTO item : items) {
            contentStream.showText("Item: " + item.getName() + "  " + item.getQuantity() + "  " + item.getPrice() + "  " + item.getQuantity() * item.getPrice());
            contentStream.newLine();
        }
    }

    private InvoiceDTO mapToInvoiceDTO(InvoiceDAO invoiceDAO) {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setId(invoiceDAO.getId());
        invoiceDTO.setPrice(invoiceDAO.getPrice());
        invoiceDTO.setDescription(invoiceDAO.getDescription());
        invoiceDTO.setIssuedDate(invoiceDAO.getIssuedDate());
        invoiceDTO.setDueDate(invoiceDAO.getDueDate());
        invoiceDTO.setEmail(invoiceDAO.getEmail());
        invoiceDTO.setPhone(invoiceDAO.getPhone());
        invoiceDTO.setZipCode(invoiceDAO.getZipCode());
        invoiceDTO.setAddress(invoiceDAO.getAddress());
        invoiceDTO.setCity(invoiceDAO.getCity());
        invoiceDTO.setItems(mapInvoiceItemsToDTO(invoiceDAO.getItems()));
        return invoiceDTO;
    }

    private List<InvoiceItemDTO> mapInvoiceItemsToDTO(List<InvoiceItemDAO> items) {
        if (items == null) {
            return null;
        }
        return items.stream().map(item -> {
            InvoiceItemDTO itemDTO = new InvoiceItemDTO();
            itemDTO.setName(item.getName());
            itemDTO.setPrice(item.getPrice());
            itemDTO.setQuantity(item.getQuantity());
            itemDTO.setWineExtId(item.getWineExtId());
            return itemDTO;
        }).collect(Collectors.toList());
    }

}
