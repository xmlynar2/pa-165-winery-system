package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.PurchaseDAO;
import cz.muni.fi.pa165.model.dto.PurchaseDTO;
import cz.muni.fi.pa165.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService {
    private final PurchaseRepository purchaseRepository;

    @Autowired
    public PurchaseServiceImpl(PurchaseRepository purchaseRepository) {
        this.purchaseRepository = purchaseRepository;
    }

    @Override
    public PurchaseDAO purchaseWineBottle(PurchaseDTO purchase) {
        return purchaseRepository.save(mapToDAO(purchase));
    }

    @Override
    public List<PurchaseDAO> purchaseMultipleWineBottles(List<PurchaseDTO> purchases) {
        return purchaseRepository.saveAll(purchases.stream().map(this::mapToDAO).toList());
    }

    private PurchaseDAO mapToDAO(PurchaseDTO purchase) {
        PurchaseDAO purchaseDAO = new PurchaseDAO();
        purchaseDAO.setId(purchase.getId());
        purchaseDAO.setCustomerId(purchase.getCustomerId());
        purchaseDAO.setPurchasedAt(purchase.getPurchasedAt());
        purchaseDAO.setWineBottleExtId(purchase.getWineBottleExtId());
        purchaseDAO.setQuantity(purchase.getQuantity());
        purchaseDAO.setPrice(purchase.getPrice());
        return purchaseDAO;
    }
}

