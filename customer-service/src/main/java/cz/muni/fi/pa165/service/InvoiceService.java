package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.InvoiceDAO;
import cz.muni.fi.pa165.model.dto.InvoiceDTO;

import java.util.List;

public interface InvoiceService {
    InvoiceDAO createInvoice(InvoiceDTO invoice, CustomerDAO customerDAO);

    InvoiceDAO getInvoiceById(String id);

    List<InvoiceDAO> getInvoicesByCustomerId(Long customerId, int page, int size);
}
