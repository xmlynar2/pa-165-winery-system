package cz.muni.fi.pa165.model.dao;


import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;


@Setter
@Getter
@EqualsAndHashCode
@ToString(exclude = {"customer"})
@NoArgsConstructor
@Entity
@Table(name = "feedback")
public class FeedbackDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "rating")
    private double rating;
    @Column(name = "created_at")
    private Instant createdAt;
    @Lob
    @Column(name = "feedback_text")
    private String feedbackText;
    @Column(name = "wine_ext_id", unique = true)
    private Long wineExtId;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "customer_id")
    private CustomerDAO customer;
}
