package cz.muni.fi.pa165.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TransactionDTO {
    private final String transactionType = "Sale";
    private Long customerId;
    @JsonAlias({"wineExtId", "productCode"})
    private String wineExtId;
    private int quantity;
}
