package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;

import java.util.List;

public interface FeedbackService {
    FeedbackDAO findById(Long feedbackId);

    List<FeedbackDAO> findByCustomerId(Long customerId, int page, int size);

    List<FeedbackDAO> findByWineBottleId(Long wineBottleId, int page, int size);

    void createFeedback(FeedbackDTO feedback, CustomerDAO customer);

    FeedbackDAO updateFeedbackById(FeedbackDTO feedbackDTO);
}
