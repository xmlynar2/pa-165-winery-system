package cz.muni.fi.pa165.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.Instant;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class InvoiceDTO {
    @NotNull
    private String id;
    private double price;
    private String description;
    private Instant issuedDate;
    private Instant dueDate;
    private String email;
    private String phone;
    private String zipCode;
    private String address;
    private String city;
    private List<InvoiceItemDTO> items;
}
