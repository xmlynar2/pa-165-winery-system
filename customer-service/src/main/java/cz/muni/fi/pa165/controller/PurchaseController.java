package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.PurchaseFacade;
import cz.muni.fi.pa165.model.dto.PurchaseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "Purchase API", description = "Provides API for creating purchases and creating invoices.")
@RestController
@RequestMapping("/purchase")
public class PurchaseController {

    private final PurchaseFacade purchaseFacade;

    @Autowired
    public PurchaseController(PurchaseFacade purchaseFacade) {
        this.purchaseFacade = purchaseFacade;
    }

    @Operation(summary = "Purchase a bottle", description = "Returns a purchased wine bottle by customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Purchased wine bottle",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PurchaseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Wine bottle not found"),
            @ApiResponse(responseCode = "500", description = "Unexpected error occurred")
    })
    @PostMapping("/single")
    public PurchaseDTO purchaseWineBottle(
            @Parameter(description = "Wine bottle purchase data", required = true) @RequestBody PurchaseDTO purchases
    ) {
        return purchaseFacade.purchaseWineBottle(purchases);
    }

    @Operation(summary = "Purchase bottles", description = "Returns all purchased wine bottles by customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Purchased wine bottles",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PurchaseDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Wine bottle not found"),
            @ApiResponse(responseCode = "500", description = "Unexpected error occurred")
    })
    @PostMapping("/multiple")
    public List<PurchaseDTO> purchaseMultipleWineBottles(
            @Parameter(description = "Wine bottles purchase data") @RequestBody List<PurchaseDTO> purchases
    ) {
        return purchaseFacade.purchaseWineBottles(purchases);
    }
}
