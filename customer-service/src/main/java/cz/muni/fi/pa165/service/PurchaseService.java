package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.PurchaseDAO;
import cz.muni.fi.pa165.model.dto.PurchaseDTO;

import java.util.List;

public interface PurchaseService {
    PurchaseDAO purchaseWineBottle(PurchaseDTO purchase);

    List<PurchaseDAO> purchaseMultipleWineBottles(List<PurchaseDTO> purchases);
}
