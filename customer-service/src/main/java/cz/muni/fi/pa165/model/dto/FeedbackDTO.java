package cz.muni.fi.pa165.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.time.Instant;


@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FeedbackDTO {
    @NotNull
    private Long id;
    @NotNull
    private double rating = 0.0d;
    @NotNull
    private Instant createdAt = Instant.now();
    @NotBlank
    @NotNull
    @Size(max = 5000)
    private String feedbackText;
    @NotNull
    private Long wineBottleExtId;
}
