package cz.muni.fi.pa165.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**", "/swagger-resources/**", "/v3/api-docs/**", "/").permitAll()
                        .requestMatchers("/actuator/**").permitAll()
                        .requestMatchers("/purchase").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/purchase/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/purchase/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/customer").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/customer/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/customer/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/feedback").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/feedback/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/feedback/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers("/invoice").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/invoice/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/invoice/**").hasAuthority("SCOPE_test_write")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
        ;
        return http.build();
    }
}
