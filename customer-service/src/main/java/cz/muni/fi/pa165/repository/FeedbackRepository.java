package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackRepository extends JpaRepository<FeedbackDAO, Long> {

    @Query("SELECT f FROM FeedbackDAO f WHERE f.customer.id = ?1")
    Page<FeedbackDAO> findAllByCustomerId(Long externalId, Pageable pageable);

    @Query("SELECT f FROM FeedbackDAO f WHERE f.wineExtId = ?1")
    Page<FeedbackDAO> findAllByWineExtId(Long wineExtId, Pageable pageable);
}
