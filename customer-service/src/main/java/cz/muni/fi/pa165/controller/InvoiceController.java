package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.InvoiceFacade;
import cz.muni.fi.pa165.model.dto.InvoiceDTO;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Invoice API", description = "Provides API for creating and displaying invoices.")
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    private final InvoiceFacade invoiceFacade;

    @Autowired
    public InvoiceController(InvoiceFacade invoiceFacade) {
        this.invoiceFacade = invoiceFacade;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Invoice created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = InvoiceDTO.class))}),
            @ApiResponse(responseCode = "409", description = "Invoice already exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping
    public InvoiceDTO createInvoice(
            @Parameter(description = "Invoice data", required = true) @RequestBody InvoiceDTO invoiceDTO,
            @Parameter(description = "Customer ID", required = true) @RequestParam Long customerId
    ) {
        return invoiceFacade.createInvoice(invoiceDTO, customerId);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the invoice",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = InvoiceDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Invoice not found",
                    content = @Content(mediaType = "application/json"))
    })
    @GetMapping("/{invoiceId}")
    public InvoiceDTO getInvoiceById(
            @Parameter(description = "Invoice ID", required = true) @PathVariable String invoiceId
    ) {
        return invoiceFacade.getInvoiceById(invoiceId);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All invoices of customer returned",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = List.class))})
    })
    @GetMapping("/customer/{customerId}")
    public List<InvoiceDTO> getInvoicesByCustomerId(
            @Parameter(description = "Customer ID", required = true) @PathVariable Long customerId,
            @Parameter(description = "Page") @RequestParam(defaultValue = "0") int page,
            @Parameter(description = "Size") @RequestParam(defaultValue = "10") int size
    ) {
        return invoiceFacade.getInvoicesByCustomerId(customerId, page, size);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Invoice document generated",
                    content = @Content(mediaType = "application/pdf")),
            @ApiResponse(responseCode = "404", description = "Invoice not found",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Error generating invoice document")
    })
    @GetMapping("/document/{invoiceId}")
    public byte[] generateInvoiceDocument(
            @Parameter(description = "Invoice ID", required = true) @PathVariable String invoiceId
    ) {
        return invoiceFacade.generateInvoiceDocument(invoiceId);
    }
}
