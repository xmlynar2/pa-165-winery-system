package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exception.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.CustomerDAO;
import cz.muni.fi.pa165.model.dao.FeedbackDAO;
import cz.muni.fi.pa165.model.dto.FeedbackDTO;
import cz.muni.fi.pa165.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    @Autowired
    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public FeedbackDAO findById(Long feedbackId) {
        return feedbackRepository.findById(feedbackId).orElseThrow(() -> new ResourceNotFoundException("Feedback with id " + feedbackId + " not found"));
    }

    @Override
    public List<FeedbackDAO> findByCustomerId(Long customerId, int page, int size) {
        Pageable pageable = Pageable.ofSize(size).withPage(page);
        return feedbackRepository.findAllByCustomerId(customerId, pageable).getContent();
    }

    @Override
    public List<FeedbackDAO> findByWineBottleId(Long wineBottleId, int page, int size) {
        Pageable pageable = Pageable.ofSize(size).withPage(page);
        return feedbackRepository.findAllByWineExtId(wineBottleId, pageable).getContent();
    }

    @Override
    @Transactional
    public void createFeedback(FeedbackDTO feedback, CustomerDAO customer) {
        FeedbackDAO feedbackDAO = mapToFeedbackDAO(feedback, customer);
        feedbackRepository.save(feedbackDAO);
    }

    @Override
    @Transactional
    public FeedbackDAO updateFeedbackById(FeedbackDTO feedbackDTO) {
        FeedbackDAO feedbackDAO = feedbackRepository.findById(feedbackDTO.getId()).orElseThrow(() -> new ResourceNotFoundException("Feedback with id " + feedbackDTO.getId() + " not found"));
        feedbackDAO.setRating(feedbackDTO.getRating());
        feedbackDAO.setFeedbackText(feedbackDTO.getFeedbackText());
        return feedbackRepository.save(feedbackDAO);
    }

    private FeedbackDAO mapToFeedbackDAO(FeedbackDTO feedbackDTO, CustomerDAO customerDAO) {
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        feedbackDAO.setRating(feedbackDTO.getRating());
        feedbackDAO.setCreatedAt(feedbackDTO.getCreatedAt());
        feedbackDAO.setFeedbackText(feedbackDTO.getFeedbackText());
        feedbackDAO.setWineExtId(feedbackDTO.getWineBottleExtId());
        feedbackDAO.setCustomer(customerDAO);
        return feedbackDAO;
    }
}
