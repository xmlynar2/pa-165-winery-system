package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.facades.ProductFacade;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class ProductController {
    private final ProductFacade productFacade;

    public ProductController(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    @Operation(summary = "Get product detail", description = "Returns detail of product by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned product",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "product not fround",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/{id}")
    public ProductDTO getById(@Parameter(description = "Product Id") @PathVariable String id) {
        return productFacade.getById(id);
    }

    @Operation(summary = "Create new product", description = "Create a new product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created product",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Product exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping
    public ProductDTO create(@Parameter(description = "Product to create") @RequestBody ProductDTO productDTO) {
        return productFacade.create(productDTO);
    }

    @Operation(summary = "Get product list", description = "Returns list of paginated products")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returning list of products",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProductDTO.class)))
                    })})
    @GetMapping
    public List<ProductDTO> getList(@RequestParam(name = "page", defaultValue = "0") int page,
                                    @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return productFacade.getList(page, pageSize);
    }

    @Operation(summary = "Update existing product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProductDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Product not found",
                    content = @Content(mediaType = "application/json"))
    })
    @PutMapping("/{id}")
    public ProductDTO update(
            @Parameter(description = "Product Id") @PathVariable String id,
            @Parameter(description = "Product to update") @RequestBody ProductDTO productDTO) {
        return productFacade.update(id, productDTO);
    }

    @Operation(summary = "Update existing product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product deleted",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404", description = "Product not found",
                    content = @Content(mediaType = "application/json"))
    })
    @DeleteMapping("/{id}")
    public void delete(
            @Parameter(description = "Product Id") @PathVariable String id) {
        productFacade.delete(id);
    }
}
