package cz.muni.fi.pa165.model;

public enum DurationUnit {
    SECONDS,
    HOURS,
    DAYS,
    WEEKS,
    MONTHS,
    YEARS
}
