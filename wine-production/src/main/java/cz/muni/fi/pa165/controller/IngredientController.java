package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.facades.IngredientFacade;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ingredient")
public class IngredientController {
    private final IngredientFacade ingredientFacade;

    public IngredientController(IngredientFacade ingredientFacade) {
        this.ingredientFacade = ingredientFacade;
    }

    @Operation(summary = "Get ingredient detail", description = "Returns detail of ingredient by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returned ingredient",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "Ingredient not fround",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/{id}")
    public IngredientDTO getById(@Parameter(description = "Ingredient Id") @PathVariable String id) {
        return ingredientFacade.getById(id);
    }

    @Operation(summary = "Get ingredient detail", description = "Returns detail of ingredient by name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returned ingredient",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "Ingredient not fround",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/name/{name}")
    public IngredientDTO getIngredientByName(@Parameter(description = "Ingredient name") @PathVariable String name) {
        return ingredientFacade.getIngredientByName(name);
    }

    @Operation(summary = "Get ingredient detail", description = "Returns detail of ingredient by quantity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returned ingredient",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "Ingredient not fround",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/quantity/{quantity}")
    public List<IngredientDTO> getByQuantityMoreThanEqual(@Parameter(description = "Ingredient quantity") @PathVariable Double quantity) {
        return ingredientFacade.getByQuantityMoreThanEqual(quantity);
    }

    @Operation(summary = "Create new ingredient", description = "Create a new ingredient")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created ingredient",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Ingredient exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping
    public IngredientDTO create(@Parameter(description = "Ingredient to create") @RequestBody IngredientDTO ingredient) {
        return ingredientFacade.create(ingredient);
    }

    @Operation(summary = "Get ingredient list", description = "Returns list of paginated ingredients")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returning list of ingredients",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = IngredientDTO.class)))
                    })})
    @GetMapping
    public List<IngredientDTO> getList(@RequestParam(name = "page", defaultValue = "0") int page,
                                       @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return ingredientFacade.getList(page, pageSize);
    }

    @Operation(summary = "Update existing ingredient")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ingredient updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = IngredientDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Ingredient not found",
                    content = @Content(mediaType = "application/json"))
    })
    @PutMapping("/{id}")
    public IngredientDTO update(
            @Parameter(description = "Ingredient Id") @PathVariable String id,
            @Parameter(description = "Ingredient to update") @RequestBody IngredientDTO ingredient) {
        return ingredientFacade.update(id, ingredient);
    }

    @Operation(summary = "Update existing ingredient")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ingredient deleted",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404", description = "Ingredient not found",
                    content = @Content(mediaType = "application/json"))
    })
    @DeleteMapping("/{id}")
    public void delete(
            @Parameter(description = "Ingredient Id") @PathVariable String id) {
        ingredientFacade.delete(id);
    }
}

