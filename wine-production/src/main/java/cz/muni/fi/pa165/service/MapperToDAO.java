package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.*;
import cz.muni.fi.pa165.model.dto.*;
import org.springframework.stereotype.Service;

@Service
public class MapperToDAO {
    public GrapeDAO mapToGrapeDAO(GrapeDTO grapeDTO) {
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setName(grapeDTO.getName());
        grapeDAO.setCode(grapeDTO.getCode());
        grapeDAO.setQuantity(grapeDTO.getQuantity());
        grapeDAO.setQuantityUnit(grapeDTO.getQuantityUnit());
        grapeDAO.setDescription(grapeDTO.getDescription());
        return grapeDAO;
    }

    public ProductDAO mapToProductDAO(ProductDTO productDTO) {
        ProductDAO productDAO = new ProductDAO();
        productDAO.setCode(productDTO.getCode());
        productDAO.setName(productDTO.getName());
        productDAO.setDescription(productDTO.getDescription());
        productDAO.setQuantity(productDTO.getQuantity());
        productDAO.setQuantityUnit(productDTO.getQuantityUnit());
        productDAO.setPrice(productDTO.getPrice());
        return productDAO;
    }

    public IngredientDAO mapToIngredientDAO(IngredientDTO ingredientDTO) {
        IngredientDAO ingredientDAO = new IngredientDAO();
        ingredientDAO.setName(ingredientDTO.getName());
        ingredientDAO.setQuantity(ingredientDTO.getQuantity());
        ingredientDAO.setQuantityUnit(ingredientDTO.getQuantityUnit());
        return ingredientDAO;
    }

    public ProductionStepDAO mapToProductionStepDAO(ProductionStepDTO productionStepDTO) {
        ProductionStepDAO productionStepDAO = new ProductionStepDAO();
        productionStepDAO.setName(productionStepDTO.getName());
        productionStepDAO.setDuration(productionStepDTO.getDuration());
        productionStepDAO.setIngredients(productionStepDTO.getIngredients().stream().map(this::mapToIngredientDAO).toList());
        return productionStepDAO;
    }

    public ProductionProcessDAO mapToProductionProcessDAO(ProductionProcessDTO productionProcessDTO) {
        ProductionProcessDAO productionProcessDAO = new ProductionProcessDAO();
        productionProcessDAO.setName(productionProcessDTO.getName());
        productionProcessDAO.setProductionSteps(productionProcessDTO.getProductionSteps().stream().map(this::mapToProductionStepDAO).toList());
        productionProcessDAO.setDifficulty(productionProcessDTO.getDifficulty());
        return productionProcessDAO;
    }
}
