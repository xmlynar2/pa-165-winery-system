package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.facades.ProductionProcessFacade;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.model.dto.ProductionProcessDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/productionProcess")
public class ProductionProcessController {

    private final ProductionProcessFacade productionProcessFacade;

    public ProductionProcessController(ProductionProcessFacade productionProcessFacade) {
        this.productionProcessFacade = productionProcessFacade;
    }

    @Operation(summary = "Get ProductionProcess by id", description = "Returns detail of production process by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returned process",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "Process not fround",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/{id}")
    public ProductionProcessDTO getById(@Parameter(description = "Process Id") @PathVariable String id) {
        return productionProcessFacade.getById(id);
    }

    @Operation(summary = "Create new productionProcess")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created process",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Process exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping
    public ProductionProcessDTO create(@Parameter(description = "ProductionProcess to create")
                                       @RequestBody ProductionProcessDTO productionProcessDTO) {
        return productionProcessFacade.create(productionProcessDTO);
    }

    @Operation(summary = "Get productionProcess list", description = "Returns list of paginated productionProcess")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returning list of productionProcess",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProductionProcessDTO.class)))
                    })})
    @GetMapping
    public List<ProductionProcessDTO> getList(@RequestParam(name = "page", defaultValue = "0") int page,
                                              @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return productionProcessFacade.getList(page, pageSize);
    }

    @Operation(summary = "Get possible productionProcess list", description = "Returns list of possible productionProcess")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returning list of possible productionProcess",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProductionProcessDTO.class)))
                    })})

    @GetMapping("/possible")
    public List<ProductionProcessDTO> getPossibleProcessList() {
        return productionProcessFacade.getAllPossibleProcesses();
    }

    @Operation(summary = "Get current resource quantity", description = "Returns summary of all resources (grapes, ingredients) from the inventory" +
            "of the production process")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns summary of all resources (grapes, ingredients) from the inventory" +
                    "of the production process",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Map.class))
                    })
    })
    @GetMapping("/resources")
    public Map<String, Map<String, Double>> getCurrentResourceQuantityOfProcess(@Parameter(description = "ProductionProcess Id") @RequestParam String id) {
        return productionProcessFacade.getCurrentResourceQuantityOfProcess(id);
    }

    @Operation(summary = "Update existing production process")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Process updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProductionProcessDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Process not found",
                    content = @Content(mediaType = "application/json"))
    })
    @PutMapping("/{id}")
    public ProductionProcessDTO update(
            @Parameter(description = "ProductionProcess id") @PathVariable String id,
            @Parameter(description = "ProductionProcess to update") @RequestBody ProductionProcessDTO productionProcessDTO) {
        return productionProcessFacade.update(id, productionProcessDTO);
    }

    @Operation(summary = "Update existing ProductionProcess")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Process deleted",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404", description = "Process not found",
                    content = @Content(mediaType = "application/json"))
    })
    @DeleteMapping("/{id}")
    public void delete(
            @Parameter(description = "ProductionProcess Id") @PathVariable String id) {
        productionProcessFacade.delete(id);
    }

    @Operation(summary = "Produce Product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Produce sucessful",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404", description = "Process not found",
                    content = @Content(mediaType = "application/json"))
    })
    @PostMapping("/produce/{id}")
    public ProductDTO produce(
            @Parameter(description = "ProductionProcess Id") @PathVariable String id) {
        return productionProcessFacade.produce(id);
    }

    ;


}
