package cz.muni.fi.pa165.model;

public enum Difficulty {
    EASY,
    MEDIUM,
    HARD
}
