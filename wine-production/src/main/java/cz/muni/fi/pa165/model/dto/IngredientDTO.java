package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.QuantityUnit;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class IngredientDTO {

    @NotBlank
    @Size(max = 30)
    private String name;
    @NotBlank
    private Double quantity;
    @NotBlank
    private QuantityUnit quantityUnit;
}
