package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.ProductionProcessDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductionProcessRepository extends JpaRepository<ProductionProcessDAO, String> {
}
