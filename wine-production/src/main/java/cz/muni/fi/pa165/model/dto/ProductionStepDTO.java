package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.DurationUnit;
import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dao.IngredientDAO;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;


@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class ProductionStepDTO {
    @NotBlank
    @Size(max = 30)
    private String name;
    @NotBlank
    private Double duration;
    @NotBlank
    private DurationUnit durationUnit;
    private List<IngredientDTO> ingredients;
    private List<GrapeDTO> grapes;
}
