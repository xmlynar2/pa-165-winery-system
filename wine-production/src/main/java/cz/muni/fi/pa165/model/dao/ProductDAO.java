package cz.muni.fi.pa165.model.dao;

import cz.muni.fi.pa165.model.QuantityUnit;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Table(name = "product")
public class ProductDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    String id;
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "quantity")
    private Double quantity;
    @Column(name = "quantityUnit")
    private QuantityUnit quantityUnit;
    @Column(name = "price")
    private double price;

}
