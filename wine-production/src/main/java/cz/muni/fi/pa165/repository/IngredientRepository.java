package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.IngredientDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends JpaRepository<IngredientDAO, String> {
    @Query("SELECT i FROM IngredientDAO i WHERE i.name = ?1")
    IngredientDAO getByName(String name);

    @Query("SELECT i FROM IngredientDAO i WHERE i.quantity >= ?1")
    List<IngredientDAO> getByQuantityMoreThanEqual(Double quantity);

}
