package cz.muni.fi.pa165.service.services;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.repository.GrapeRepository;
import cz.muni.fi.pa165.service.MapperToDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class GrapeService {

    private final GrapeRepository grapeRepository;
    private final MapperToDAO mapper;

    @Autowired
    public GrapeService(GrapeRepository grapeRepository, MapperToDAO mapper) {
        this.grapeRepository = grapeRepository;
        this.mapper = mapper;
    }

    public GrapeDAO getById(String id) {
        return grapeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Grape with id " + id + " not found"));
    }

    @Transactional
    public GrapeDAO create(GrapeDTO grapeDTO) {
        GrapeDAO grapeDAO = mapper.mapToGrapeDAO(grapeDTO);
        return grapeRepository.save(grapeDAO);
    }

    public List<GrapeDAO> getList(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return grapeRepository.findAll(pageable).getContent();
    }

    @Transactional
    public GrapeDAO update(String id, GrapeDTO grapeDTO) {
        GrapeDAO grapeDAO = grapeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Grape with id " + id + " not found"));
        grapeDAO.setName(mapper.mapToGrapeDAO(grapeDTO).getName());
        grapeDAO.setQuantity(mapper.mapToGrapeDAO(grapeDTO).getQuantity());
        grapeDAO.setQuantityUnit(mapper.mapToGrapeDAO(grapeDTO).getQuantityUnit());
        return grapeRepository.save(grapeDAO);
    }

    @Transactional
    public void delete(String id) {
        grapeRepository.deleteById(id);
    }

}
