package cz.muni.fi.pa165.facade.facades;


import cz.muni.fi.pa165.facade.MapperToDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.service.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductFacade {
    private final ProductService productService;

    private final MapperToDTO mapper;


    @Autowired
    public ProductFacade(ProductService productService, MapperToDTO mapper) {
        this.productService = productService;
        this.mapper = mapper;
    }

    public ProductDTO getById(String id) {
        return mapper.mapToProductDTO(productService.getById(id));
    }


    public ProductDTO create(ProductDTO productDTO) {
        return mapper.mapToProductDTO(productService.create(productDTO));
    }

    public List<ProductDTO> getList(int page, int pageSize) {
        return mapper.mapToProductDTOList(productService.getList(page, pageSize));
    }

    public ProductDTO update(String id, ProductDTO productDTO) {
        return mapper.mapToProductDTO(productService.update(id, productDTO));
    }

    public void delete(String id) {
        productService.delete(id);
    }

}
