package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dao.*;
import cz.muni.fi.pa165.model.dto.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MapperToDTO {

    public IngredientDTO mapToIngredientDTO(IngredientDAO ingredientDAO) {
        return new IngredientDTO(
                ingredientDAO.getName(),
                ingredientDAO.getQuantity(),
                ingredientDAO.getQuantityUnit()
        );
    }

    public List<IngredientDTO> mapToIngredientDTOList(List<IngredientDAO> daoList) {
        return daoList.stream()
                .map(this::mapToIngredientDTO)
                .collect(Collectors.toList());
    }

    public GrapeDTO mapToGrapeDTO(GrapeDAO grapeDAO) {
        return new GrapeDTO(
                grapeDAO.getCode(),
                grapeDAO.getQuantity(),
                grapeDAO.getQuantityUnit(),
                grapeDAO.getName(),
                grapeDAO.getDescription()
        );
    }

    public List<GrapeDTO> mapToGrapeDTOList(List<GrapeDAO> daoList) {
        return daoList.stream()
                .map(this::mapToGrapeDTO)
                .collect(Collectors.toList());
    }

    public ProductDTO mapToProductDTO(ProductDAO productDAO) {
        return new ProductDTO(
                productDAO.getCode(),
                productDAO.getName(),
                productDAO.getDescription(),
                productDAO.getQuantity(),
                productDAO.getQuantityUnit(),
                productDAO.getPrice()
        );
    }

    public List<ProductDTO> mapToProductDTOList(List<ProductDAO> daoList) {
        return daoList.stream()
                .map(this::mapToProductDTO)
                .collect(Collectors.toList());
    }



    public ProductionProcessDTO mapToProductionProcessDTO(ProductionProcessDAO productionProcessDAO) {
        return new ProductionProcessDTO(
                productionProcessDAO.getName(),
                productionProcessDAO.getProductionSteps().stream().map(this::mapToProductionStepDTO).collect(Collectors.toList()),
                productionProcessDAO.getDifficulty()
        );
    }

    public List<ProductionProcessDTO> mapToProductionProcessDTOList(List<ProductionProcessDAO> daoList) {
        return daoList.stream()
                .map(this::mapToProductionProcessDTO)
                .collect(Collectors.toList());
    }

    public ProductionStepDTO mapToProductionStepDTO(ProductionStepDAO productionStepDAO) {
        return new ProductionStepDTO(
                productionStepDAO.getName(),
                productionStepDAO.getDuration(),
                productionStepDAO.getDurationUnit(),
                productionStepDAO.getIngredients().stream().map(this::mapToIngredientDTO).collect(Collectors.toList()),
                productionStepDAO.getGrapes().stream().map(this::mapToGrapeDTO).collect(Collectors.toList())
        );
    }

    public List<ProductionStepDTO> mapToProductionStepDTOList(List<ProductionStepDAO> daoList) {
        return daoList.stream()
                .map(this::mapToProductionStepDTO)
                .collect(Collectors.toList());
    }


}
