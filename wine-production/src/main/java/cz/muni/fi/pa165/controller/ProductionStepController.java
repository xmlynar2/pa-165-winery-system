package cz.muni.fi.pa165.controller;


import cz.muni.fi.pa165.facade.facades.ProductionStepFacade;
import cz.muni.fi.pa165.model.dto.ProductionStepDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productionStep")
public class ProductionStepController {

    private final ProductionStepFacade productionStepFacade;

    public ProductionStepController(ProductionStepFacade productionStepFacade) {
        this.productionStepFacade = productionStepFacade;
    }

    @Operation(summary = "Get productionStep detail", description = "Returns detail of productionStep by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returned productionStep",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "ProductionStep not fround",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/{id}")
    public ProductionStepDTO getById(@Parameter(description = "productionStep Id") @PathVariable String id) {
        return productionStepFacade.getById(id);
    }

    @Operation(summary = "Create new productionStep")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created productionStep",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "ProductionStep exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping
    public ProductionStepDTO create(@Parameter(description = "ProductionStep to create")
                                    @RequestBody ProductionStepDTO productionStepDTO) {
        return productionStepFacade.create(productionStepDTO);
    }


    @Operation(summary = "Get productionStep list", description = "Returns list of paginated productionStep")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returning list of productionStep",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProductionStepDTO.class)))
                    })})
    @GetMapping
    public List<ProductionStepDTO> getList(@RequestParam(name = "page", defaultValue = "0") int page,
                                           @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return productionStepFacade.getList(page, pageSize);
    }

    @Operation(summary = "Update existing productionStep")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "ProductionStep updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProductionStepDTO.class))}),
            @ApiResponse(responseCode = "404", description = "ProductionStep not found",
                    content = @Content(mediaType = "application/json"))
    })
    @PutMapping("/{id}")
    public ProductionStepDTO update(
            @Parameter(description = "Production id") @PathVariable String id,
            @Parameter(description = "ProductionStep to update") @RequestBody ProductionStepDTO productionStepDTO) {
        return productionStepFacade.update(id, productionStepDTO);
    }

    @Operation(summary = "Update existing ProductionStep")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "ProductionStep deleted",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404", description = "ProductionStep not found",
                    content = @Content(mediaType = "application/json"))
    })
    @DeleteMapping("/{id}")
    public void delete(
            @Parameter(description = "ProductionStep Id") @PathVariable String id) {
        productionStepFacade.delete(id);
    }

}
