package cz.muni.fi.pa165.exceptions;

public class ResourceNotAvailableException extends RuntimeException {

    public ResourceNotAvailableException(String message) {
        super("Resource not available: " + message);
    }
}
