package cz.muni.fi.pa165.facade.facades;

import cz.muni.fi.pa165.facade.MapperToDTO;
import cz.muni.fi.pa165.model.dto.ProductionStepDTO;
import cz.muni.fi.pa165.service.services.ProductionStepService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductionStepFacade {

    private final ProductionStepService productionStepService;

    private final MapperToDTO mapper;

    public ProductionStepFacade(ProductionStepService productionStepService, MapperToDTO mapper) {
        this.productionStepService = productionStepService;
        this.mapper = mapper;
    }


    public ProductionStepDTO getById(String id) {
        return mapper.mapToProductionStepDTO(productionStepService.getById(id));
    }


    public ProductionStepDTO create(ProductionStepDTO productionStepDTO) {
        return mapper.mapToProductionStepDTO(productionStepService.create(productionStepDTO));
    }


    public List<ProductionStepDTO> getList(int page, int pageSize) {
        return mapper.mapToProductionStepDTOList(productionStepService.getList(page, pageSize));
    }


    public ProductionStepDTO update(String id, ProductionStepDTO productionStepDTO) {
        return mapper.mapToProductionStepDTO(productionStepService.update(id, productionStepDTO));
    }


    public void delete(String id) {
        productionStepService.delete(id);
    }

}
