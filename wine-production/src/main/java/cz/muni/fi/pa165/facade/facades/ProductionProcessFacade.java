package cz.muni.fi.pa165.facade.facades;

import cz.muni.fi.pa165.client.InventoryManagementClient;
import cz.muni.fi.pa165.exceptions.InsufficientResourcesException;
import cz.muni.fi.pa165.facade.MapperToDTO;
import cz.muni.fi.pa165.model.QuantityUnit;
import cz.muni.fi.pa165.model.dao.ProductionProcessDAO;
import cz.muni.fi.pa165.model.dto.*;
import cz.muni.fi.pa165.service.services.ProductService;
import cz.muni.fi.pa165.service.services.ProductionProcessService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductionProcessFacade {

    private final ProductionProcessService productionProcessService;

    private final ProductService productService;
    private final InventoryManagementClient inventoryManagementClient;
    private final MapperToDTO mapper;

    public ProductionProcessFacade(ProductionProcessService productionProcessService, ProductService productService, InventoryManagementClient inventoryManagementClient, MapperToDTO mapper) {
        this.productionProcessService = productionProcessService;
        this.productService = productService;
        this.inventoryManagementClient = inventoryManagementClient;
        this.mapper = mapper;
    }

    public ProductionProcessDTO getById(String id) {
        return mapper.mapToProductionProcessDTO(productionProcessService.getById(id));
    }

    public ProductionProcessDTO create(ProductionProcessDTO productionProcessDTO) {
        return mapper.mapToProductionProcessDTO(productionProcessService.create(productionProcessDTO));
    }

    public List<ProductionProcessDTO> getList(int page, int pageSize) {
        List<ProductionProcessDAO> productionProcesses = productionProcessService.getList(page, pageSize);
        return mapper.mapToProductionProcessDTOList(productionProcesses);
    }

    public ProductionProcessDTO update(String id, ProductionProcessDTO productionProcessDTO) {
        return mapper.mapToProductionProcessDTO(productionProcessService.update(id, productionProcessDTO));
    }

    public void delete(String id) {
        productionProcessService.delete(id);
    }

    public ProductDTO produce(String id) {
        ProductionProcessDTO productionProcessDTO = mapper.mapToProductionProcessDTO(productionProcessService.getById(id));
        if (!allStepsPossible(productionProcessDTO)) {
            throw new InsufficientResourcesException("Not enough resources to produce wine");
        }
        ProductDTO newProduct = generateProduct(productionProcessDTO.getProductionSteps());
        System.out.println("New generated product" + newProduct);
        inventoryManagementClient.addWine(newProduct);

        updateInventory(productionProcessDTO.getProductionSteps());

        return mapper.mapToProductDTO(productService.create(newProduct));
    }

    private ProductDTO generateProduct(List<ProductionStepDTO> productionSteps) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setCode(generateProductCode(productionSteps));
        productDTO.setName(generateProductName(productionSteps));
        productDTO.setDescription("Product generated from production process");
        productDTO.setQuantity(calculateProductQuantity(productionSteps));
        productDTO.setQuantityUnit(QuantityUnit.Liter);
        productDTO.setPrice(calculatePrice(productionSteps));
        return productDTO;
    }

    private String generateProductCode(List<ProductionStepDTO> productionSteps) {
        StringBuilder codeBuilder = new StringBuilder("x_");
        for (ProductionStepDTO productionStepDTO : productionSteps) {
            for (GrapeDTO grapeDTO : productionStepDTO.getGrapes()) {
                codeBuilder.append(grapeDTO.getCode());
            }
        }
        return codeBuilder.toString();
    }

    private String generateProductName(List<ProductionStepDTO> productionSteps) {
        StringBuilder nameBuilder = new StringBuilder("product_");
        for (ProductionStepDTO productionStepDTO : productionSteps) {
            for (GrapeDTO grapeDTO : productionStepDTO.getGrapes()) {
                nameBuilder.append(grapeDTO.getName());
            }
            for (IngredientDTO ingredientDTO : productionStepDTO.getIngredients()) {
                nameBuilder.append(ingredientDTO.getName());
            }
        }
        return nameBuilder.toString();
    }

    private Double calculateProductQuantity(List<ProductionStepDTO> productionSteps) {
        Double productQuantity = 0.0;
        for (ProductionStepDTO productionStepDTO : productionSteps) {
            for (GrapeDTO grapeDTO : productionStepDTO.getGrapes()) {
                productQuantity += grapeDTO.getQuantity();
            }
            for (IngredientDTO ingredientDTO : productionStepDTO.getIngredients()) {
                productQuantity += ingredientDTO.getQuantity();
            }
        }
        return productQuantity;
    }

    ;

    private Double calculatePrice(List<ProductionStepDTO> productionSteps) {
        Double price = 0.0;
        for (ProductionStepDTO ignored : productionSteps) {
            price += 5;
        }
        return price;
    }

    ;

    private void updateInventory(List<ProductionStepDTO> productionSteps) {
        System.out.println("Updating inventory" + productionSteps);
        for (ProductionStepDTO productionStepDTO : productionSteps) {
            for (GrapeDTO grapeDTO : productionStepDTO.getGrapes()) {
                GrapeDTO fetchedGrape = fetchGrapeByCode(grapeDTO.getCode());
                fetchedGrape.setQuantity(-(grapeDTO.getQuantity()));
                inventoryManagementClient.restockGrape(fetchedGrape);
            }
            for (IngredientDTO ingredientDTO : productionStepDTO.getIngredients()) {
                IngredientDTO fetchedIngredient = fetchIngredientByName(ingredientDTO.getName());
                fetchedIngredient.setQuantity(-(ingredientDTO.getQuantity()));
                inventoryManagementClient.restockIngredient(fetchedIngredient);
            }
        }

    }

    public List<ProductionProcessDTO> getAllPossibleProcesses() {
        List<ProductionProcessDTO> ProductionProcesses = mapper.mapToProductionProcessDTOList(productionProcessService.getAll());
        List<ProductionProcessDTO> possibleProcesses = new ArrayList<>();

        for (ProductionProcessDTO productionProcessDTO : ProductionProcesses) {
            if (allStepsPossible(productionProcessDTO)) {
                possibleProcesses.add(productionProcessDTO);
            }
        }
        return possibleProcesses;
    }

    private Boolean allStepsPossible(ProductionProcessDTO productionProcessDTO) {
        for (ProductionStepDTO productionStepDTO : productionProcessDTO.getProductionSteps()) {
            if (!stepPossible(productionStepDTO)) {
                return false;
            }
        }
        return true;
    }

    private Boolean stepPossible(ProductionStepDTO productionStepDTO) {
        return allIngredientsAvailable(productionStepDTO.getIngredients()) &&
                allGrapesAvailable(productionStepDTO.getGrapes());
    }

    private Boolean allIngredientsAvailable(List<IngredientDTO> ingredients) {
        for (IngredientDTO ingredient : ingredients) {
            IngredientDTO fetchedIngredient = fetchIngredientByName(ingredient.getName());
            if (fetchedIngredient == null || !ingredientAvailable(ingredient, fetchedIngredient)) {
                return false;
            }
        }
        return true;
    }

    private Boolean allGrapesAvailable(List<GrapeDTO> grapes) {
        for (GrapeDTO grape : grapes) {
            GrapeDTO fetchedGrape = fetchGrapeByCode(grape.getCode());
            if (fetchedGrape == null || !grapeAvailable(grape, fetchedGrape)) {
                return false;
            }
        }
        return true;
    }

    private GrapeDTO fetchGrapeByCode(String code) {
        return inventoryManagementClient.getGrapeByCode(code);
    }

    private IngredientDTO fetchIngredientByName(String name) {
        return inventoryManagementClient.getIngredientByName(name);
    }

    private Boolean grapeAvailable(GrapeDTO required, GrapeDTO provided) {
        return required.getQuantity() <= provided.getQuantity();
    }

    private Boolean ingredientAvailable(IngredientDTO required, IngredientDTO provided) {
        return required.getQuantity() <= provided.getQuantity();
    }

    public Map<String, Map<String, Double>> getCurrentResourceQuantityOfProcess(String id) {
        ProductionProcessDTO productionProcessDTO = mapper.mapToProductionProcessDTO(productionProcessService.getById(id));
        Map<String, Map<String, Double>> ingredientsAndGrapesQuantity = new HashMap<>();
        ingredientsAndGrapesQuantity.put("ingredients", getProductionIngredientsQuantity(productionProcessDTO));
        ingredientsAndGrapesQuantity.put("grapes", getProductionGrapesQuantity(productionProcessDTO));
        return ingredientsAndGrapesQuantity;
    }

    private Map<String, Double> getProductionIngredientsQuantity(ProductionProcessDTO productionProcessDTO) {
        Map<String, Double> ingredientsQuantity = new HashMap<>();

        List<String> ingredientNames = getProductionIngredientsNames(productionProcessDTO);

        for (String ingredientName : ingredientNames) {
            ingredientsQuantity.put(ingredientName, inventoryManagementClient.getIngredientByName(ingredientName).getQuantity());
        }
        return ingredientsQuantity;
    }

    private Map<String, Double> getProductionGrapesQuantity(ProductionProcessDTO productionProcessDTO) {
        Map<String, Double> grapesQuantity = new HashMap<>();

        List<String> grapesCodes = getProductionGrapesCodes(productionProcessDTO);

        for (String grapeCode : grapesCodes) {
            grapesQuantity.put(grapeCode, inventoryManagementClient.getGrapeByCode(grapeCode).getQuantity());
        }
        return grapesQuantity;
    }


    private List<String> getProductionIngredientsNames(ProductionProcessDTO productionProcessDTO) {
        List<String> ingredientsNames = new ArrayList<>();
        for (ProductionStepDTO productionStepDTO : productionProcessDTO.getProductionSteps()) {
            for (IngredientDTO ingredientDTO : productionStepDTO.getIngredients()) {
                ingredientsNames.add(ingredientDTO.getName());
            }
        }
        return ingredientsNames;
    }

    private List<String> getProductionGrapesCodes(ProductionProcessDTO productionProcessDTO) {
        List<String> grapesCodes = new ArrayList<>();
        for (ProductionStepDTO productionStepDTO : productionProcessDTO.getProductionSteps()) {
            for (GrapeDTO grapeDTO : productionStepDTO.getGrapes()) {
                grapesCodes.add(grapeDTO.getCode());
            }
        }
        return grapesCodes;
    }


}
