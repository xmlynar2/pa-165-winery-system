package cz.muni.fi.pa165.exceptions;

public class InsufficientResourcesException extends RuntimeException {

    public InsufficientResourcesException(String message) {
        super("Insufficient resources for production step: " + message);
    }
}
