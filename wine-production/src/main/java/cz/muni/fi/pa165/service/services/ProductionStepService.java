package cz.muni.fi.pa165.service.services;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.ProductionStepDAO;
import cz.muni.fi.pa165.model.dto.ProductionStepDTO;
import cz.muni.fi.pa165.repository.ProductionStepRepository;
import cz.muni.fi.pa165.service.MapperToDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductionStepService {

    private final ProductionStepRepository productionStepRepository;
    private final MapperToDAO mapper;

    @Autowired
    public ProductionStepService(ProductionStepRepository productionStepRepository, MapperToDAO mapper) {
        this.productionStepRepository = productionStepRepository;
        this.mapper = mapper;
    }


    public ProductionStepDAO getById(String id) {
        return productionStepRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Production step with id " + id + " not found"));
    }

    @Transactional
    public ProductionStepDAO create(ProductionStepDTO productionStepDTO) {
        ProductionStepDAO productionStepDAO = mapper.mapToProductionStepDAO(productionStepDTO);
        return productionStepRepository.save(productionStepDAO);
    }


    public List<ProductionStepDAO> getList(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return productionStepRepository.findAll(pageable).getContent();
    }

    @Transactional
    public ProductionStepDAO update(String id, ProductionStepDTO productionStepDTO) {
        ProductionStepDAO productionStepDAO = productionStepRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Production step with id " + id + " not found"));
        productionStepDAO.setName(mapper.mapToProductionStepDAO(productionStepDTO).getName());
        productionStepDAO.setDuration(mapper.mapToProductionStepDAO(productionStepDTO).getDuration());
        productionStepDAO.setIngredients(mapper.mapToProductionStepDAO(productionStepDTO).getIngredients());
        return productionStepRepository.save(productionStepDAO);

    }

    @Transactional
    public void delete(String id) {
        productionStepRepository.deleteById(id);
    }

}
