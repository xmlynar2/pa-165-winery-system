package cz.muni.fi.pa165.facade.facades;

import cz.muni.fi.pa165.facade.MapperToDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.service.services.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientFacade {
    private final IngredientService ingredientService;

    private final MapperToDTO mapper;


    @Autowired
    public IngredientFacade(IngredientService ingredientService, MapperToDTO mapper) {
        this.ingredientService = ingredientService;
        this.mapper = mapper;
    }

    public IngredientDTO getById(String id) {
        return mapper.mapToIngredientDTO(ingredientService.getById(id));
    }

    public IngredientDTO getIngredientByName(String name) {
        return mapper.mapToIngredientDTO(ingredientService.getIngredientByName(name));
    }

    public List<IngredientDTO> getByQuantityMoreThanEqual(Double quantity) {
        return mapper.mapToIngredientDTOList(ingredientService.getByQuantityMoreThanEqual(quantity));
    }

    public IngredientDTO create(IngredientDTO ingredientDTO) {
        return mapper.mapToIngredientDTO(ingredientService.create(ingredientDTO));
    }

    public List<IngredientDTO> getList(int page, int pageSize) {
        return mapper.mapToIngredientDTOList(ingredientService.getList(page, pageSize));
    }

    public IngredientDTO update(String id, IngredientDTO ingredientDTO) {
        return mapper.mapToIngredientDTO(ingredientService.update(id, ingredientDTO));
    }

    public void delete(String id) {
        ingredientService.delete(id);
    }

}
