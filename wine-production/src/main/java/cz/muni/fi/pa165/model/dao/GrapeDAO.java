package cz.muni.fi.pa165.model.dao;

import cz.muni.fi.pa165.model.QuantityUnit;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@ToString
@Table(name = "grape")
public class GrapeDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "code")
    private String code;
    @Column(name = "quantity")
    private Double quantity;
    @Column(name = "quantityUnit")
    private QuantityUnit quantityUnit;
    @Column(name = "description")
    private String description;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productionStep_id")
    private ProductionStepDAO productionStep;


}
