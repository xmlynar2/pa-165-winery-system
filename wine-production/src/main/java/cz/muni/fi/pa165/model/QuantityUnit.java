package cz.muni.fi.pa165.model;

public enum QuantityUnit {
    Ks,
    Kg,
    Liter
}
