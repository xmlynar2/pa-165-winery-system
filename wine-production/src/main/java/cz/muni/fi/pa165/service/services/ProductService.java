package cz.muni.fi.pa165.service.services;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.ProductDAO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.repository.ProductRepository;
import cz.muni.fi.pa165.service.MapperToDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductService {

    private final ProductRepository productRepository;
    private final MapperToDAO mapper;

    @Autowired
    public ProductService(ProductRepository productRepository, MapperToDAO mapper) {
        this.productRepository = productRepository;
        this.mapper = mapper;
    }

    public ProductDAO getById(String id) {
        return productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Grape with id " + id + " not found"));
    }


    @Transactional
    public ProductDAO create(ProductDTO productDTO) {
        ProductDAO productDAO = mapper.mapToProductDAO(productDTO);
        return productRepository.save(productDAO);
    }


    public List<ProductDAO> getList(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return productRepository.findAll(pageable).getContent();
    }

    @Transactional
    public ProductDAO update(String id, ProductDTO productDTO) {
        ProductDAO productDAO = productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Grape with id " + id + " not found"));
        productDAO.setName(mapper.mapToProductDAO(productDTO).getName());
        productDAO.setCode(mapper.mapToProductDAO(productDTO).getCode());
        productDAO.setDescription(mapper.mapToProductDAO(productDTO).getDescription());
        productDAO.setQuantity(mapper.mapToProductDAO(productDTO).getQuantity());
        productDAO.setQuantityUnit(mapper.mapToProductDAO(productDTO).getQuantityUnit());
        productDAO.setPrice(mapper.mapToProductDAO(productDTO).getPrice());
        return productRepository.save(productDAO);
    }

    @Transactional
    public void delete(String id) {
        productRepository.deleteById(id);
    }

}
