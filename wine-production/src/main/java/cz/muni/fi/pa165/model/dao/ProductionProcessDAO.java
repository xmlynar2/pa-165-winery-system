package cz.muni.fi.pa165.model.dao;

import cz.muni.fi.pa165.model.Difficulty;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "productionProcess")
public class ProductionProcessDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "name")
    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productionProcess", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProductionStepDAO> productionSteps;
    @Column(name = "difficulty")
    private Difficulty difficulty;
}
