package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.QuantityUnit;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class GrapeDTO {
    @NotNull
    private String code;
    @NotNull
    private Double quantity;
    @NotNull
    private QuantityUnit quantityUnit;
    @NotNull
    private String name;
    @NotNull
    private String description;
}
