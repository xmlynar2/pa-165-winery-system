package cz.muni.fi.pa165.model.dto;


import cz.muni.fi.pa165.model.QuantityUnit;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class ProductDTO {
    @NotNull
    @Size(min = 1, max = 100)
    private String code;
    @NotNull
    @Size(max = 30)
    private String name;
    @NotNull
    private String description;
    @NotNull
    private Double quantity;
    @NotNull
    private QuantityUnit quantityUnit;
    @NotNull
    private double price;

    public ProductDTO() {
    }

}
