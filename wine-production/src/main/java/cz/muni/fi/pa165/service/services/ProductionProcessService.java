package cz.muni.fi.pa165.service.services;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.ProductionProcessDAO;
import cz.muni.fi.pa165.model.dto.ProductionProcessDTO;
import cz.muni.fi.pa165.repository.ProductRepository;
import cz.muni.fi.pa165.repository.ProductionProcessRepository;
import cz.muni.fi.pa165.service.MapperToDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductionProcessService {

    private final ProductionProcessRepository productionProcessRepository;

    private final ProductRepository productRepository;

    private final MapperToDAO mapper;

    @Autowired
    public ProductionProcessService(ProductionProcessRepository productionProcessRepository, ProductRepository productRepository, MapperToDAO mapper) {
        this.productionProcessRepository = productionProcessRepository;
        this.productRepository = productRepository;
        this.mapper = mapper;
    }


    public ProductionProcessDAO getById(String id) {
        return productionProcessRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Production process with id " + id + " not found"));
    }

    @Transactional
    public ProductionProcessDAO create(ProductionProcessDTO productionProcessDTO) {
        ProductionProcessDAO productionProcessDAO = mapper.mapToProductionProcessDAO(productionProcessDTO);
        return productionProcessRepository.save(productionProcessDAO);
    }

    public List<ProductionProcessDAO> getList(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return productionProcessRepository.findAll(pageable).getContent();
    }

    public List<ProductionProcessDAO> getAll() {
        return productionProcessRepository.findAll();
    }

    @Transactional
    public ProductionProcessDAO update(String id, ProductionProcessDTO productionProcessDTO) {
        ProductionProcessDAO productionProcessDAO = productionProcessRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Production process with id " + id + " not found"));
        productionProcessDAO.setName(mapper.mapToProductionProcessDAO(productionProcessDTO).getName());
        productionProcessDAO.setProductionSteps(mapper.mapToProductionProcessDAO(productionProcessDTO).getProductionSteps());
        productionProcessDAO.setDifficulty(mapper.mapToProductionProcessDAO(productionProcessDTO).getDifficulty());
        return productionProcessRepository.save(productionProcessDAO);
    }

    @Transactional
    public void delete(String id) {
        productionProcessRepository.deleteById(id);
    }


}

