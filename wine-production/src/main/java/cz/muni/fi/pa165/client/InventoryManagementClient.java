package cz.muni.fi.pa165.client;

import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "InventoryManagementClient", url = "${inventory-management.url}")
public interface InventoryManagementClient {
    @GetMapping("/ingredient/{name}")
    IngredientDTO getIngredientByName(@PathVariable String name);

    @GetMapping("/grape/{code}")
    GrapeDTO getGrapeByCode(@PathVariable String code);

    @PostMapping("/grape/restock")
    GrapeDTO restockGrape(GrapeDTO grapeDTO);

    @PostMapping("/ingredient/restock")
    IngredientDTO restockIngredient(IngredientDTO ingredientDTO);

    @PostMapping("/wine/create")
    ProductDTO addWine(ProductDTO productDTO);

}
