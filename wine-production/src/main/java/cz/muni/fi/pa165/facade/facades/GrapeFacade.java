package cz.muni.fi.pa165.facade.facades;

import cz.muni.fi.pa165.facade.MapperToDTO;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.service.services.GrapeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GrapeFacade {
    private final GrapeService grapeService;

    private final MapperToDTO mapper;


    @Autowired
    public GrapeFacade(GrapeService grapeService, MapperToDTO mapper) {
        this.grapeService = grapeService;
        this.mapper = mapper;
    }

    public GrapeDTO getById(String id) {
        return mapper.mapToGrapeDTO(grapeService.getById(id));
    }


    public GrapeDTO create(GrapeDTO grapeDTO) {
        return mapper.mapToGrapeDTO(grapeService.create(grapeDTO));
    }

    public List<GrapeDTO> getList(int page, int pageSize) {
        return mapper.mapToGrapeDTOList(grapeService.getList(page, pageSize));
    }

    public GrapeDTO update(String id, GrapeDTO grapeDTO) {
        return mapper.mapToGrapeDTO(grapeService.update(id, grapeDTO));
    }

    public void delete(String id) {
        grapeService.delete(id);
    }

}
