package cz.muni.fi.pa165.model.dao;

import cz.muni.fi.pa165.model.DurationUnit;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Table(name = "productionStep")
public class ProductionStepDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private Double duration;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "productionProcess_id")
    private ProductionProcessDAO productionProcess;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productionStep")
    private List<IngredientDAO> ingredients;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productionStep")
    private List<GrapeDAO> grapes;
    @Column(name = "durationUnit")
    private DurationUnit durationUnit;
}
