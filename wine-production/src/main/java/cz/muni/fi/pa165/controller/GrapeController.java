package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.facades.GrapeFacade;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/grape")
public class GrapeController {
    private final GrapeFacade grapeFacade;

    public GrapeController(GrapeFacade grapeFacade) {
        this.grapeFacade = grapeFacade;
    }

    @Operation(summary = "Get grape detail", description = "Returns detail of grape by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned grape",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "409", description = "Grape not fround",
                    content = @Content(mediaType = "application/json")),
    })
    @GetMapping("/{id}")
    public GrapeDTO getById(@Parameter(description = "Grape Id") @PathVariable String id) {
        return grapeFacade.getById(id);
    }

    @Operation(summary = "Create new grape", description = "Create a new grape")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created grape",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Grape exists",
                    content = @Content(mediaType = "application/json")),
    })
    @PostMapping
    public GrapeDTO create(@Parameter(description = "Grape to create") @RequestBody GrapeDTO grapeDTO) {
        return grapeFacade.create(grapeDTO);
    }

    @Operation(summary = "Get grape list", description = "Returns list of paginated grapes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returning list of grapes",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = GrapeDTO.class)))
                    })})
    @GetMapping
    public List<GrapeDTO> getList(@RequestParam(name = "page", defaultValue = "0") int page,
                                  @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return grapeFacade.getList(page, pageSize);
    }

    @Operation(summary = "Update existing grape")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GrapeDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Grape not found",
                    content = @Content(mediaType = "application/json"))
    })
    @PutMapping("/{id}")
    public GrapeDTO update(
            @Parameter(description = "Grape Id") @PathVariable String id,
            @Parameter(description = "Grape to update") @RequestBody GrapeDTO grapeDTO) {
        return grapeFacade.update(id, grapeDTO);
    }

    @Operation(summary = "Update existing grape")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape deleted",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404", description = "Grape not found",
                    content = @Content(mediaType = "application/json"))
    })
    @DeleteMapping("/{id}")
    public void delete(
            @Parameter(description = "Grape Id") @PathVariable String id) {
        grapeFacade.delete(id);
    }
}

