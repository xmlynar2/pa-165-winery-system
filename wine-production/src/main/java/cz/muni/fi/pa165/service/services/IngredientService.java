package cz.muni.fi.pa165.service.services;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.IngredientDAO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.repository.IngredientRepository;
import cz.muni.fi.pa165.service.MapperToDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class IngredientService {

    private final IngredientRepository ingredientRepository;
    private final MapperToDAO mapper;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository, MapperToDAO mapper) {
        this.ingredientRepository = ingredientRepository;
        this.mapper = mapper;
    }

    public IngredientDAO getById(String id) {
        return ingredientRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Ingredient with id " + id + " not found"));
    }


    public IngredientDAO getIngredientByName(String name) {
        return ingredientRepository.getByName(name);
    }


    public List<IngredientDAO> getByQuantityMoreThanEqual(Double quantity) {
        return ingredientRepository.getByQuantityMoreThanEqual(quantity);
    }

    @Transactional
    public IngredientDAO create(IngredientDTO ingredientDTO) {
        IngredientDAO ingredientDAO = mapper.mapToIngredientDAO(ingredientDTO);
        return ingredientRepository.save(ingredientDAO);
    }


    public List<IngredientDAO> getList(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return ingredientRepository.findAll(pageable).getContent();
    }

    @Transactional
    public IngredientDAO update(String id, IngredientDTO ingredientDTO) {
        IngredientDAO ingredientDAO = ingredientRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Ingredient with id " + id + " not found"));
        ingredientDAO.setName(mapper.mapToIngredientDAO(ingredientDTO).getName());
        ingredientDAO.setQuantity(mapper.mapToIngredientDAO(ingredientDTO).getQuantity());
        ingredientDAO.setQuantityUnit(mapper.mapToIngredientDAO(ingredientDTO).getQuantityUnit());
        return ingredientRepository.save(ingredientDAO);
    }

    @Transactional
    public void delete(String id) {
        ingredientRepository.deleteById(id);
    }

}
