package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.Difficulty;
import cz.muni.fi.pa165.model.dao.ProductionStepDAO;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class ProductionProcessDTO {
    @NotBlank
    @Size(max = 30)
    private String name;
    private List<ProductionStepDTO> productionSteps;
    @NotBlank
    private Difficulty difficulty;
}
