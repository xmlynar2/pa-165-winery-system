package cz.muni.fi.pa165;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EntityScan(basePackages = {"cz.muni.fi.pa165"})
@SpringBootApplication
@EnableFeignClients
public class WineProduction {
    public static void main(String[] args) {
        SpringApplication.run(WineProduction.class, args);
    }
}
