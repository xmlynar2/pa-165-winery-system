package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dao.IngredientDAO;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GrapeRepository extends JpaRepository<GrapeDAO, String> {
}
