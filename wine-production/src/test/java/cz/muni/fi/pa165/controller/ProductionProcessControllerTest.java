package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.facades.ProductionProcessFacade;
import cz.muni.fi.pa165.model.Difficulty;
import cz.muni.fi.pa165.model.dto.ProductionProcessDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;

import static com.jayway.jsonpath.internal.path.PathCompiler.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@WithMockUser(authorities = {"SCOPE_test_read", "SCOPE_test_write"})
class ProductionProcessControllerTest {

    @Mock
    private ProductionProcessFacade productionProcessFacade;

    @InjectMocks
    private ProductionProcessController productionProcessController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsProcessWhenPresent() {
        ProductionProcessDTO productionProcessDTO = new ProductionProcessDTO("null", new ArrayList<>(), Difficulty.MEDIUM);
        when(productionProcessFacade.getById("1")).thenReturn(productionProcessDTO);

        ProductionProcessDTO result = productionProcessController.getById("1");

        assertEquals(productionProcessDTO, result);
    }

    @Test
    void getByIdWithNonExistingId() {
        String productionProcessId = "1";
        when(productionProcessFacade.getById(productionProcessId)).thenThrow(new ResourceNotFoundException("ProductionProcess not found"));
        try {
            productionProcessController.getById(productionProcessId);
            fail("Expected ResourceNotFoundException was not thrown");
        } catch (ResourceNotFoundException e) {
            verify(productionProcessFacade, times(1)).getById(productionProcessId);
            assertEquals("ProductionProcess not found", e.getMessage());
        }
    }

    @Test
    void getByIdReturnsNullWhenAbsent() {
        when(productionProcessFacade.getById("1")).thenReturn(null);

        ProductionProcessDTO result = productionProcessController.getById("1");

        assertEquals(null, result);
    }

    @Test
    void createReturnsCreatedProcess() {
        ProductionProcessDTO productionProcessDTO = new ProductionProcessDTO("null", new ArrayList<>(), Difficulty.MEDIUM);
        when(productionProcessFacade.create(any(ProductionProcessDTO.class))).thenReturn(productionProcessDTO);

        ProductionProcessDTO result = productionProcessController.create(productionProcessDTO);

        assertEquals(productionProcessDTO, result);
    }

    @Test
    void createHandlesFailure() {
        ProductionProcessDTO productionProcessDTO = new ProductionProcessDTO("null", new ArrayList<>(), Difficulty.MEDIUM);

        doThrow(new RuntimeException("Failed to create productionStep")).when(productionProcessFacade).create(any(ProductionProcessDTO.class));
        try {
            productionProcessController.create(productionProcessDTO);
            fail("Expected RuntimeException was not thrown");
        } catch (RuntimeException e) {
            verify(productionProcessFacade, times(1)).create(eq(productionProcessDTO));
            assertEquals("Failed to create productionStep", e.getMessage());
        }
    }

    @Test
    void createProductionProcessHandlesDataIntegrityViolation() {
        ProductionProcessDTO productionProcessDTO = new ProductionProcessDTO("null", new ArrayList<>(), Difficulty.MEDIUM);

        doThrow(new DataIntegrityViolationException("Data integrity violation")).when(productionProcessFacade).create(any(ProductionProcessDTO.class));

        try {
            productionProcessController.create(productionProcessDTO);
            fail("Expected DataIntegrityViolationException was not thrown");
        } catch (DataIntegrityViolationException e) {
            verify(productionProcessFacade, times(1)).create(eq(productionProcessDTO));
            assertEquals("Data integrity violation", e.getMessage());
        }
    }

    @Test
    void updateReturnsUpdatedProcess() {
        ProductionProcessDTO productionProcessDTO = new ProductionProcessDTO("null", new ArrayList<>(), Difficulty.MEDIUM);
        when(productionProcessFacade.update(anyString(), any(ProductionProcessDTO.class))).thenReturn(productionProcessDTO);

        ProductionProcessDTO result = productionProcessController.update("1", productionProcessDTO);

        assertEquals(productionProcessDTO, result);
    }
}
