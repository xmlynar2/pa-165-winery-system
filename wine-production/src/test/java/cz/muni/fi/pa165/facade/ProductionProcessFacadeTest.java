package cz.muni.fi.pa165.facade;
import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.facades.ProductionProcessFacade;
import cz.muni.fi.pa165.model.Difficulty;
import cz.muni.fi.pa165.model.DurationUnit;
import cz.muni.fi.pa165.model.dao.ProductionProcessDAO;
import cz.muni.fi.pa165.model.dao.ProductionStepDAO;
import cz.muni.fi.pa165.model.dto.ProductionProcessDTO;
import cz.muni.fi.pa165.model.dto.ProductionStepDTO;
import cz.muni.fi.pa165.service.services.ProductionProcessService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ProductionProcessFacadeTest {

    @Mock
    private ProductionProcessService productionProcessService;

    @Mock
    private MapperToDTO mapper;

    @InjectMocks
    private ProductionProcessFacade productionProcessFacade;

    private ProductionProcessDAO productionProcessDAO;
    private ProductionProcessDTO productionProcessDTO;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        productionProcessDAO = new ProductionProcessDAO();
        productionProcessDAO.setName("Fermentation");
        productionProcessDAO.setProductionSteps(Arrays.asList(new ProductionStepDAO(), new ProductionStepDAO()));
        productionProcessDAO.setDifficulty(Difficulty.MEDIUM);

        productionProcessDTO = new ProductionProcessDTO("Fermentation",
                Arrays.asList(
                new ProductionStepDTO("test1", 1.0, DurationUnit.WEEKS, List.of(), List.of()),
                new ProductionStepDTO("test2", 2.0, DurationUnit.WEEKS, List.of(), List.of())),
                Difficulty.MEDIUM
        );
    }

    @Test
    public void getByIdReturnsCorrectProcess() {
        when(productionProcessService.getById("1")).thenReturn(productionProcessDAO);
        when(mapper.mapToProductionProcessDTO(productionProcessDAO)).thenReturn(productionProcessDTO);

        ProductionProcessDTO result = productionProcessFacade.getById("1");

        assertEquals(productionProcessDTO, result);
        verify(productionProcessService, times(1)).getById("1");
    }

    @Test
    void getProcessByIdWithNonExistingIdThrowsNotFound() {
        String nonExistingId = "999";
        when(productionProcessService.getById(nonExistingId)).thenThrow(new ResourceNotFoundException("Process not found"));

        assertThrows(ResourceNotFoundException.class, () -> productionProcessFacade.getById(nonExistingId));
    }
    @Test
    void createProcessSavesSuccessfully() {
        ProductionProcessDTO productionProcessDTO = productionProcessFacade.create(new ProductionProcessDTO(
                "Sugar",
                Arrays.asList(
                        new ProductionStepDTO("test1", 1.0, DurationUnit.WEEKS, List.of(), List.of()),
                        new ProductionStepDTO("test2", 2.0, DurationUnit.WEEKS, List.of(), List.of())),
                Difficulty.MEDIUM
        ));

        productionProcessFacade.create(productionProcessDTO);

        verify(productionProcessService, times(1)).create(productionProcessDTO);
    }
    @Test
    void createReturnsCorrectProcess() {
        when(productionProcessService.create(any(ProductionProcessDTO.class))).thenReturn(productionProcessDAO);

        when(mapper.mapToProductionProcessDTO(productionProcessDAO)).thenReturn(productionProcessDTO);

        ProductionProcessDTO result = productionProcessFacade.create(new ProductionProcessDTO(
                "Sugar",
                Arrays.asList(
                        new ProductionStepDTO("test1", 1.0, DurationUnit.WEEKS, List.of(), List.of()),
                        new ProductionStepDTO("test2", 2.0, DurationUnit.WEEKS, List.of(), List.of())),
                Difficulty.MEDIUM
        ));

        assertEquals(productionProcessDTO, result);

        verify(productionProcessService, times(1)).create(any(ProductionProcessDTO.class));
    }
}
