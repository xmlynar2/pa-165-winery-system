package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.Difficulty;
import cz.muni.fi.pa165.model.dao.ProductionProcessDAO;
import cz.muni.fi.pa165.model.dto.ProductionProcessDTO;
import cz.muni.fi.pa165.repository.ProductionProcessRepository;
import cz.muni.fi.pa165.service.services.ProductionProcessService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Optional;

import static com.jayway.jsonpath.internal.path.PathCompiler.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class ProductionProcessServiceTest {

    @Mock
    private ProductionProcessRepository productionProcessRepository;
    @Mock
    private MapperToDAO mapper;
    @InjectMocks
    private ProductionProcessService productionProcessService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsProcessWhenPresent() {
        ProductionProcessDAO productionProcessDAO = new ProductionProcessDAO();
        when(productionProcessRepository.findById("1")).thenReturn(Optional.of(productionProcessDAO));

        ProductionProcessDAO result = productionProcessService.getById("1");

        assertEquals(productionProcessDAO, result);
    }

    @Test
    public void getByIdThrowsResourceNotFoundExceptionWhenAbsent() {
        when(productionProcessRepository.findById("1")).thenReturn(Optional.empty());
        try {
            productionProcessService.getById("1");
            fail("Expected ResourceNotFOundException but no exception was thrown");
        } catch (ResourceNotFoundException e) {
            assertEquals("Production process with id 1 not found", e.getMessage());
        }
    }

    @Test
    void createReturnsSavedProcess() {
        ProductionProcessDTO productionProcessDTO = new ProductionProcessDTO("null", Collections.emptyList(), Difficulty.MEDIUM);
        ProductionProcessDAO productionProcessDAO = new ProductionProcessDAO();
        when(mapper.mapToProductionProcessDAO(productionProcessDTO)).thenReturn(productionProcessDAO);
        when(productionProcessRepository.save(any(ProductionProcessDAO.class))).thenReturn(productionProcessDAO);

        ProductionProcessDAO result = productionProcessService.create(productionProcessDTO);

        assertEquals(productionProcessDAO, result);
    }


//    @Test
//    void getAllReturnsEmptyListWhenNoProcesses() {
//        when(productionProcessRepository.findAll()).thenReturn(Collections.emptyList());
//
//        List<ProductionProcessDAO> result = productionProcessService.getList(   1, 10);
//
//        assertEquals(0, result.size());
//    }
}
