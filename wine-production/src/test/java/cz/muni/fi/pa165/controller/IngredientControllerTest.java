package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.facades.IngredientFacade;
import cz.muni.fi.pa165.model.QuantityUnit;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.test.context.support.WithMockUser;

import static com.jayway.jsonpath.internal.path.PathCompiler.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@WithMockUser(authorities = {"SCOPE_test_read", "SCOPE_test_write"})
class IngredientControllerTest {

    @Mock
    private IngredientFacade ingredientFacade;

    @InjectMocks
    private IngredientController ingredientController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsIngredientWhenPresent() {
        IngredientDTO ingredientDTO = new IngredientDTO("null", Double.valueOf(0), QuantityUnit.Kg);
        when(ingredientFacade.getById("1")).thenReturn(ingredientDTO);

        IngredientDTO result = ingredientController.getById("1");

        assertEquals(ingredientDTO, result);
    }

    @Test
    void getByIdWithNonExistingId() {
        String ingredientId = "1";

        when(ingredientFacade.getById(ingredientId)).thenThrow(new ResourceNotFoundException("Ingredient not found"));
        try {
            ingredientController.getById(ingredientId);
            fail("Expected ResourceNotFoundException was not thrown");
        } catch (ResourceNotFoundException e) {
            verify(ingredientFacade, times(1)).getById(ingredientId);
            assertEquals("Ingredient not found", e.getMessage());
        }
    }

    @Test
    void getByIdReturnsNullWhenAbsent() {
        when(ingredientFacade.getById("1")).thenReturn(null);

        IngredientDTO result = ingredientController.getById("1");

        assertEquals(null, result);
    }

    @Test
    void createReturnsCreatedIngredient() {
        IngredientDTO ingredientDTO = new IngredientDTO("null", Double.valueOf(0), QuantityUnit.Kg);
        when(ingredientFacade.create(any(IngredientDTO.class))).thenReturn(ingredientDTO);

        IngredientDTO result = ingredientController.create(ingredientDTO);

        assertEquals(ingredientDTO, result);
    }
    @Test
    void createHandlesFailure() {
        IngredientDTO ingredientDTO = new IngredientDTO("null", Double.valueOf(0), QuantityUnit.Kg);

        // Mocking the behavior of ingredientFacade.create to throw an exception
        doThrow(new RuntimeException("Failed to create ingredient")).when(ingredientFacade).create(any(IngredientDTO.class));

        // Call the method under test and catch any exception
        try {
            ingredientController.create(ingredientDTO);
            fail("Expected RuntimeException was not thrown");
        } catch (RuntimeException e) {
            // Verify that the exception was properly handled
            verify(ingredientFacade, times(1)).create(eq(ingredientDTO));
            assertEquals("Failed to create ingredient", e.getMessage());
        }
    }

    @Test
    void createIngredientHandlesDataIntegrityViolation() {
        IngredientDTO ingredientDTO = new IngredientDTO("null", Double.valueOf(0), QuantityUnit.Kg);

        doThrow(new DataIntegrityViolationException("Data integrity violation")).when(ingredientFacade).create(any(IngredientDTO.class));

        try {
            ingredientController.create(ingredientDTO);
            fail("Expected DataIntegrityViolationException was not thrown");
        } catch (DataIntegrityViolationException e) {
            verify(ingredientFacade, times(1)).create(eq(ingredientDTO));
            assertEquals("Data integrity violation", e.getMessage());
        }
    }
    @Test
    void updateReturnsUpdatedIngredient() {
        IngredientDTO ingredientDTO = new IngredientDTO("null", Double.valueOf(0), QuantityUnit.Kg);
        when(ingredientFacade.update(anyString(), any(IngredientDTO.class))).thenReturn(ingredientDTO);

        IngredientDTO result = ingredientController.update("1", ingredientDTO);

        assertEquals(ingredientDTO, result);
    }
}
