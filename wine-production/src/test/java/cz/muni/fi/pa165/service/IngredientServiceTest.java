package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.dao.IngredientDAO;
import cz.muni.fi.pa165.model.QuantityUnit;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.repository.IngredientRepository;
import cz.muni.fi.pa165.service.services.IngredientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static com.jayway.jsonpath.internal.path.PathCompiler.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class IngredientServiceTest {

    @Mock
    private IngredientRepository ingredientRepository;
    @Mock
    private MapperToDAO mapper;
    @InjectMocks
    private IngredientService ingredientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsIngredientWhenPresent() {
        IngredientDAO ingredientDAO = new IngredientDAO();
        when(ingredientRepository.findById("1")).thenReturn(Optional.of(ingredientDAO));

        IngredientDAO result = ingredientService.getById("1");

        assertEquals(ingredientDAO, result);
    }

    @Test
    public void getByIdThrowsResourceNotFoundExceptionWhenAbsent() {
        when(ingredientRepository.findById("1")).thenReturn(Optional.empty());


        try {
            ingredientService.getById("1");
            fail("Expected ResourceNotFOundException but no exception was thrown");
        } catch (ResourceNotFoundException e) {
            assertEquals("Ingredient with id 1 not found", e.getMessage());
        }
    }

    @Test
    void createReturnsSavedIngredient() {
        IngredientDTO ingredientDTO = new IngredientDTO("null", Double.valueOf(0), QuantityUnit.Kg);
        IngredientDAO ingredientDAO = new IngredientDAO();
        when(mapper.mapToIngredientDAO(ingredientDTO)).thenReturn(ingredientDAO);
        when(ingredientRepository.save(any(IngredientDAO.class))).thenReturn(ingredientDAO);

        IngredientDAO result = ingredientService.create(ingredientDTO);

        assertEquals(ingredientDAO, result);
    }


}
