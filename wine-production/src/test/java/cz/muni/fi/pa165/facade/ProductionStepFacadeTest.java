package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.facades.ProductionStepFacade;
import cz.muni.fi.pa165.model.DurationUnit;
import cz.muni.fi.pa165.model.dao.ProductionStepDAO;
import cz.muni.fi.pa165.model.dto.ProductionStepDTO;
import cz.muni.fi.pa165.service.services.ProductionStepService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ProductionStepFacadeTest {

    @Mock
    private ProductionStepService productionStepService;

    @Mock
    private MapperToDTO mapper;

    @InjectMocks
    private ProductionStepFacade productionStepFacade;

    private ProductionStepDAO productionStepDAO;
    private ProductionStepDTO productionStepDTO;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        productionStepDAO = new ProductionStepDAO();
        productionStepDAO.setName("Fermentation Step");
        productionStepDAO.setDuration(Double.valueOf(60));
        productionStepDAO.setDurationUnit(DurationUnit.DAYS);

        productionStepDTO = new ProductionStepDTO("Fermentation Step", 60.0, DurationUnit.DAYS, null, null);
    }

    @Test
    public void getByIdReturnsCorrectProcess() {
        when(productionStepService.getById("1")).thenReturn(productionStepDAO);
        when(mapper.mapToProductionStepDTO(productionStepDAO)).thenReturn(productionStepDTO);

        ProductionStepDTO result = productionStepFacade.getById("1");

        assertEquals(productionStepDTO, result);
        verify(productionStepService, times(1)).getById("1");
    }
    @Test
    void getStepByIdWithNonExistingIdThrowsNotFound() {
        String nonExistingId = "999";
        when(productionStepService.getById(nonExistingId)).thenThrow(new ResourceNotFoundException("Step not found"));

        assertThrows(ResourceNotFoundException.class, () -> productionStepFacade.getById(nonExistingId));
    }
    @Test
    void createStepSavesSuccessfully() {
        ProductionStepDTO productionStepDTO = productionStepFacade.create(new ProductionStepDTO("Fermentation Step", 60.0, DurationUnit.DAYS, null, null));

        productionStepFacade.create(productionStepDTO);

        verify(productionStepService, times(1)).create(productionStepDTO);
    }
    @Test
    void createReturnsCorrectStep() {
        when(productionStepService.create(any(ProductionStepDTO.class))).thenReturn(productionStepDAO);

        when(mapper.mapToProductionStepDTO(productionStepDAO)).thenReturn(productionStepDTO);

        ProductionStepDTO result = productionStepFacade.create(new ProductionStepDTO("Fermentation Step", 60.0, DurationUnit.DAYS, null,null));

        assertEquals(productionStepDTO, result);

        verify(productionStepService, times(1)).create(any(ProductionStepDTO.class));
    }

}
