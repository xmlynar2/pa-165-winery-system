package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.facades.IngredientFacade;
import cz.muni.fi.pa165.model.dao.IngredientDAO;
import cz.muni.fi.pa165.model.QuantityUnit;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.service.services.IngredientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class IngredientFacadeTest {

    @Mock
    private IngredientService ingredientService;

    @Mock
    private MapperToDTO mapper;
    @InjectMocks
    private IngredientFacade ingredientFacade;

    private IngredientDAO ingredientDAO;
    private IngredientDTO ingredientDTO;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        ingredientDAO = new IngredientDAO();
        ingredientDAO.setName("Sugar");
        ingredientDAO.setQuantity(100.0);
        ingredientDAO.setQuantityUnit(QuantityUnit.Kg);

        ingredientDTO = new IngredientDTO("Sugar", Double.valueOf(100), QuantityUnit.Kg);
    }

    @Test
    public void getByIdReturnsCorrectIngredient() {
        when(ingredientService.getById("1")).thenReturn(ingredientDAO);
        when(mapper.mapToIngredientDTO(ingredientDAO)).thenReturn(ingredientDTO);

        IngredientDTO result = ingredientFacade.getById("1");

        assertEquals(ingredientDTO, result);
        verify(ingredientService, times(1)).getById("1");
    }

    @Test
    void getIngredientByIdWithNonExistingIdThrowsNotFound() {
        String nonExistingId = "999";
        when(ingredientService.getById(nonExistingId)).thenThrow(new ResourceNotFoundException("Ingredient not found"));

        assertThrows(ResourceNotFoundException.class, () -> ingredientFacade.getById(nonExistingId));
    }

    @Test
    void createIngredientSavesSuccessfully() {
        IngredientDTO ingredientDTO = ingredientFacade.create(new IngredientDTO("Sugar", 100.0, QuantityUnit.Kg));

        ingredientFacade.create(ingredientDTO);

        verify(ingredientService, times(1)).create(ingredientDTO);
    }

    @Test
    void createReturnsCorrectIngredient() {
        when(ingredientService.create(any(IngredientDTO.class))).thenReturn(ingredientDAO);

        when(mapper.mapToIngredientDTO(ingredientDAO)).thenReturn(ingredientDTO);

        IngredientDTO result = ingredientFacade.create(new IngredientDTO("Sugar", 100.0, QuantityUnit.Kg));

        assertEquals(ingredientDTO, result);

        verify(ingredientService, times(1)).create(any(IngredientDTO.class));
    }

}
