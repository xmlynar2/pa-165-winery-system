package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.model.DurationUnit;
import cz.muni.fi.pa165.model.dao.ProductionStepDAO;
import cz.muni.fi.pa165.model.dto.ProductionStepDTO;
import cz.muni.fi.pa165.repository.ProductionStepRepository;
import cz.muni.fi.pa165.service.services.ProductionStepService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Optional;

import static com.jayway.jsonpath.internal.path.PathCompiler.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class ProductionStepServiceTest {

    @Mock
    private ProductionStepRepository productionStepRepository;
    @Mock
    private MapperToDAO mapper;
    @InjectMocks
    private ProductionStepService productionStepService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsStepWhenPresent() {
        ProductionStepDAO productionStepDAO = new ProductionStepDAO();
        when(productionStepRepository.findById("1")).thenReturn(Optional.of(productionStepDAO));

        ProductionStepDAO result = productionStepService.getById("1");

        assertEquals(productionStepDAO, result);
    }

    @Test
    public void getByIdThrowsResourceNotFoundExceptionWhenAbsent() {
        // Arrange
        when(productionStepRepository.findById("1")).thenReturn(Optional.empty());

        // Act & Assert
        try {
            productionStepService.getById("1");
            // If no exception is thrown, fail the test
            fail("Expected ResourceNotFOundException but no exception was thrown");
        } catch (ResourceNotFoundException e) {
            // Assert
            assertEquals("Production step with id 1 not found", e.getMessage());
        }
    }

    @Test
    void createReturnsSavedStep() {
        ProductionStepDTO productionStepDTO = new ProductionStepDTO("null", Double.valueOf(0), DurationUnit.MONTHS, Collections.emptyList(), Collections.emptyList());
        ProductionStepDAO productionStepDAO = new ProductionStepDAO();
        when(mapper.mapToProductionStepDAO(productionStepDTO)).thenReturn(productionStepDAO);
        when(productionStepRepository.save(any(ProductionStepDAO.class))).thenReturn(productionStepDAO);

        ProductionStepDAO result = productionStepService.create(productionStepDTO);

        assertEquals(productionStepDAO, result);
    }



//    @Test
//    void getAllReturnsEmptyListWhenNoSteps() {
//        when(productionStepRepository.findAll()).thenReturn(Collections.emptyList());
//
//        List<ProductionStepDAO> result = productionStepService.getALl();
//
//        assertEquals(0, result.size());
//    }
}
