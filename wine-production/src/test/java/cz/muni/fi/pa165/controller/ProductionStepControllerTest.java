package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.exceptions.ResourceNotFoundException;
import cz.muni.fi.pa165.facade.facades.ProductionStepFacade;
import cz.muni.fi.pa165.model.DurationUnit;
import cz.muni.fi.pa165.model.dto.ProductionStepDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;

import static com.jayway.jsonpath.internal.path.PathCompiler.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@WithMockUser(authorities = {"SCOPE_test_read", "SCOPE_test_write"})
class ProductionStepControllerTest {

    @Mock
    private ProductionStepFacade productionStepFacade;

    @InjectMocks
    private ProductionStepController productionStepController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsStepWhenPresent() {
        ProductionStepDTO productionStepDTO = new ProductionStepDTO("1", Double.valueOf(0), DurationUnit.MONTHS, new ArrayList<>(),new ArrayList<>());
        when(productionStepFacade.getById("1")).thenReturn(productionStepDTO);

        ProductionStepDTO result = productionStepController.getById("1");

        assertEquals(productionStepDTO, result);
    }
    @Test
    void getByIdWithNonExistingId() {
        String productionStepId = "1";

        when(productionStepFacade.getById(productionStepId)).thenThrow(new ResourceNotFoundException("ProductionStep not found"));
        try {
            productionStepController.getById(productionStepId);
            fail("Expected ResourceNotFoundException was not thrown");
        } catch (ResourceNotFoundException e) {
            verify(productionStepFacade, times(1)).getById(productionStepId);
            assertEquals("ProductionStep not found", e.getMessage());
        }
    }

    @Test
    void getByIdReturnsNullWhenAbsent() {
        when(productionStepFacade.getById("1")).thenReturn(null);

        ProductionStepDTO result = productionStepController.getById("1");

        assertEquals(null, result);
    }

    @Test
    void createReturnsCreatedStep() {
        ProductionStepDTO productionStepDTO = new ProductionStepDTO("1", Double.valueOf(0), DurationUnit.MONTHS, new ArrayList<>(), new ArrayList<>());
        when(productionStepFacade.create(any(ProductionStepDTO.class))).thenReturn(productionStepDTO);

        ProductionStepDTO result = productionStepController.create(productionStepDTO);

        assertEquals(productionStepDTO, result);
    }

    @Test
    void createHandlesFailure() {
        ProductionStepDTO productionStepDTO = new ProductionStepDTO("null", Double.valueOf(0), DurationUnit.DAYS, new ArrayList<>(), new ArrayList<>());

        doThrow(new RuntimeException("Failed to create productionStep")).when(productionStepFacade).create(any(ProductionStepDTO.class));
        try {
            productionStepController.create(productionStepDTO);
            fail("Expected RuntimeException was not thrown");
        } catch (RuntimeException e) {
            verify(productionStepFacade, times(1)).create(eq(productionStepDTO));
            assertEquals("Failed to create productionStep", e.getMessage());
        }
    }

    @Test
    void createProductionStepHandlesDataIntegrityViolation() {
        ProductionStepDTO productionStepDTO = new ProductionStepDTO("null", Double.valueOf(0), DurationUnit.DAYS, new ArrayList<>(), new ArrayList<>());

        doThrow(new DataIntegrityViolationException("Data integrity violation")).when(productionStepFacade).create(any(ProductionStepDTO.class));

        try {
            productionStepController.create(productionStepDTO);
            fail("Expected DataIntegrityViolationException was not thrown");
        } catch (DataIntegrityViolationException e) {
            verify(productionStepFacade, times(1)).create(eq(productionStepDTO));
            assertEquals("Data integrity violation", e.getMessage());
        }
    }

    @Test
    void updateReturnsUpdatedStep() {
        ProductionStepDTO productionStepDTO = new ProductionStepDTO("1", Double.valueOf(0), DurationUnit.MONTHS, new ArrayList<>(), new ArrayList<>());
        when(productionStepFacade.update(anyString(), any(ProductionStepDTO.class))).thenReturn(productionStepDTO);

        ProductionStepDTO result = productionStepController.update("1", productionStepDTO);

        assertEquals(productionStepDTO, result);
    }
}
