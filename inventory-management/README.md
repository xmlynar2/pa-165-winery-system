## Inventory Management
This is a simple inventory management system that allows to add, update, delete, and view wine products. It also manages transactions of the products and 
provides a basic report of the sales;

## How to run
1. `docker-compose up`
2. `mvn clean install`
3. `mvn spring-boot:run`
