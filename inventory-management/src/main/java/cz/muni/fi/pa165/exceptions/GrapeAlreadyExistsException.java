package cz.muni.fi.pa165.exceptions;

public class GrapeAlreadyExistsException extends Exception {
    public GrapeAlreadyExistsException(String message) {
        super(message);
    }
}
