package cz.muni.fi.pa165.model.dto;

import lombok.Data;

@Data
public class CustomerDTO {
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private String address;
}
