package cz.muni.fi.pa165.model.dto;


import cz.muni.fi.pa165.model.validator.interfaces.AddGrapeValidationGroup;
import cz.muni.fi.pa165.model.validator.interfaces.RestockGrapeValidationGroup;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class GrapeDTO {
    @NotNull(message = "Grape code cannot be null", groups = {AddGrapeValidationGroup.class, RestockGrapeValidationGroup.class})
    @Size(min = 1, max = 100, message = "Grape code must be between 1 and 100 characters", groups = {AddGrapeValidationGroup.class, RestockGrapeValidationGroup.class})
    private String code;
    @NotNull(message = "Grape quantity cannot be null", groups = RestockGrapeValidationGroup.class)
    private int quantity;
    @NotNull(message = "Grape name cannot be null", groups = AddGrapeValidationGroup.class)
    private String name;
    @NotNull(message = "Grape description cannot be null", groups = AddGrapeValidationGroup.class)
    private String description;
}
