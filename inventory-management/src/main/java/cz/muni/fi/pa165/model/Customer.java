package cz.muni.fi.pa165.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "customer_id")
    //id used for database
    private Long id;
    //id form customer microservice
    private Long externalId;
    private String firstName;
    private String lastName;
    private String email;
    private String address;

}
