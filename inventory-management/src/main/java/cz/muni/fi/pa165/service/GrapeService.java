package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.GrapeAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.GrapeDoesNotExistException;
import cz.muni.fi.pa165.model.Grape;
import cz.muni.fi.pa165.repository.GrapeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class GrapeService {
    private final GrapeRepository grapeRepository;

    @Autowired
    public GrapeService(GrapeRepository grapeRepository) {
        this.grapeRepository = grapeRepository;
    }

    public List<Grape> getGrapeList(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return grapeRepository.findAll(pageable).getContent();
    }

    public Grape getGrapeByCode(String code) {
        return grapeRepository.findByCode(code);
    }

    @Transactional
    public void restockGrape(Grape grape) throws GrapeDoesNotExistException {
        Grape found = grapeRepository.findByCode(grape.getCode());
        if (found == null) {
            throw new GrapeDoesNotExistException("Grape with code " + grape.getCode() + " does not exist");
        } else {
            found.setQuantityAvailable(found.getQuantityAvailable() + grape.getQuantityAvailable());
            grapeRepository.save(found);
        }
    }

    @Transactional
    public void createGrape(Grape grape) throws GrapeAlreadyExistsException {
        Grape found = grapeRepository.findByCode(grape.getCode());
        if (found == null) {
            grapeRepository.save(grape);
        } else {
            throw new GrapeAlreadyExistsException("Grape with code " + grape.getCode() + " already exists");
        }
    }
}
