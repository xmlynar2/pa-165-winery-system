package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.*;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Mapper {

    private final CustomerService customerService;
    private final ProductService productService;

    @Autowired
    public Mapper(CustomerService customerService, ProductService productService) {
        this.customerService = customerService;
        this.productService = productService;
    }

    public Product mapProductDTOToProduct(ProductDTO productDTO) {
        Product product = new Product();
        product.setProductCode(productDTO.getCode());
        product.setProductName(productDTO.getName());
        product.setPrice(productDTO.getPrice());
        product.setQuantityAvailable(productDTO.getQuantity());
        product.setDescription(productDTO.getDescription());
        return product;
    }

    public List<ProductDTO> mapProductListToDTOList(List<Product> wineList) {

        return wineList.stream().map(this::mapProductToDTO).toList();
    }

    public ProductDTO mapProductToDTO(Product wineById) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setCode(wineById.getProductCode());
        productDTO.setName(wineById.getProductName());
        productDTO.setDescription(wineById.getDescription());
        productDTO.setQuantity(wineById.getQuantityAvailable());
        productDTO.setPrice(wineById.getPrice());

        return productDTO;
    }

    public Transaction mapTransactionDtoToTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction();
        //Just for now, we can't add customer
//        transaction.setCustomer(customerService.getCustomerByExternalId(transactionDTO.getCustomerId()));
        transaction.setProduct(productService.getProductByCode(transactionDTO.getProductCode()));
        transaction.setQuantity(transactionDTO.getQuantity());
        transaction.setTransactionType(Enums.TransactionType.valueOf(transactionDTO.getTransactionType()));
        return transaction;
    }

    public GrapeDTO mapGrapeToDTO(Grape grapeByCode) {
        GrapeDTO grapeDTO = new GrapeDTO();
        grapeDTO.setCode(grapeByCode.getCode());
        grapeDTO.setName(grapeByCode.getName());
        grapeDTO.setDescription(grapeByCode.getDescription());
        grapeDTO.setQuantity(grapeByCode.getQuantityAvailable());
        return grapeDTO;
    }

    public IngredientDTO mapIngredientToDTO(Ingredient ingredientByName) {
        IngredientDTO ingredientDTO = new IngredientDTO();
        ingredientDTO.setName(ingredientByName.getName());
        ingredientDTO.setQuantity(ingredientByName.getQuantity());
        ingredientDTO.setUnit(ingredientByName.getUnit());
        return ingredientDTO;
    }

    public Ingredient mapIngredientDTOToIngredient(IngredientDTO ingredientDTO) {
        Ingredient ingredient = new Ingredient();
        ingredient.setName(ingredientDTO.getName());
        ingredient.setQuantity(ingredientDTO.getQuantity());
        ingredient.setUnit(ingredientDTO.getUnit());
        return ingredient;
    }

    public Grape mapGrapeDTOToGrape(GrapeDTO grapeDTO) {
        Grape grape = new Grape();
        grape.setCode(grapeDTO.getCode());
        grape.setQuantityAvailable(grapeDTO.getQuantity());
        grape.setName(grapeDTO.getName());
        grape.setDescription(grapeDTO.getDescription());
        return grape;
    }
}
