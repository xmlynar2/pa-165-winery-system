package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.ProductNotFoundException;
import cz.muni.fi.pa165.model.Enums;
import cz.muni.fi.pa165.model.InventoryItem;
import cz.muni.fi.pa165.model.Product;
import cz.muni.fi.pa165.model.Transaction;
import cz.muni.fi.pa165.repository.InventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class InventoryService {

    private final InventoryItemRepository inventoryRepository;
    private final ProductService productService;

    @Autowired
    public InventoryService(InventoryItemRepository inventoryRepository, ProductService productService) {
        this.inventoryRepository = inventoryRepository;
        this.productService = productService;
    }

    @Transactional
    public void restockWine(String productCode, int quantity) throws ProductNotFoundException {
        Product product = productService.getProductByCode(productCode);
        if (product == null)
            throw new ProductNotFoundException("Product with code " + productCode + " not found in inventory");
        InventoryItem item = inventoryRepository.findByProductProductCode(productCode);
        if (item == null) {
            item = new InventoryItem(product, 0);
        }
        item.setQuantity(item.getQuantity() + quantity);
        inventoryRepository.save(item);
    }

    @Transactional
    public void commitTransaction(Transaction transaction) {
        InventoryItem item = inventoryRepository.findByProductProductCode(transaction.getProduct().getProductCode());
        //in case of sale we subtract quantity, in case of purchase or adjustment we add quantity
        int updatedQuantity = item.getQuantity() + (transaction.getTransactionType() == Enums.TransactionType.Sale || transaction.getTransactionType() == Enums.TransactionType.Adjustment ? -transaction.getQuantity() : transaction.getQuantity());
        item.setQuantity(updatedQuantity);
        inventoryRepository.save(item);
    }
}
