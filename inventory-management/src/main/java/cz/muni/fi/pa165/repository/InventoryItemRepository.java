package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.InventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryItemRepository extends JpaRepository<InventoryItem, Long> {
    @Query("SELECT i FROM InventoryItem i WHERE i.product.productCode = ?1")
    InventoryItem findByProductProductCode(String productCode);
}
