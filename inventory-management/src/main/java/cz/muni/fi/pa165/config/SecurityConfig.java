package cz.muni.fi.pa165.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**", "/swagger-resources/**", "/v3/api-docs/**", "/").permitAll()
                        .requestMatchers("/actuator/**").permitAll()
                        .requestMatchers( "/wine").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/wine/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/wine/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers( "/transaction").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/transaction/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/transaction/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers( "/grape").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/grape/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/grape/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers( "/ingredient").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/ingredient/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/ingredient/**").hasAuthority("SCOPE_test_write")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
        ;
        return http.build();
    }
}
