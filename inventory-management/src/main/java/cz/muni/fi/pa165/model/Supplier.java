package cz.muni.fi.pa165.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * Model used to store supplier information
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Supplier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //    @ManyToMany(mappedBy = "suppliers")
//    private List<InventoryItem> inventoryItem;
    private String supplierName;
    private String contactInfo;
    private String region;

}
