package cz.muni.fi.pa165;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"cz.muni.fi.pa165"})
@SpringBootApplication
public class InventoryManagement {

    public static void main(String[] args) {
        SpringApplication.run(InventoryManagement.class, args);
    }
}
