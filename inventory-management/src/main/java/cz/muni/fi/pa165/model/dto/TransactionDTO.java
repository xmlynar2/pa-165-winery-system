package cz.muni.fi.pa165.model.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class TransactionDTO {
    private Long customerId;
    @NotNull(message = "Product code cannot be null")
    @Size(min = 1, max = 100, message = "Product code must be between 1 and 100 characters")
    @JsonAlias({"wineExtId", "productCode"})
    private String productCode;
    @NotNull(message = "Quantity cannot be null")
    private int quantity;
    @NotNull(message = "Transaction type cannot be null")
    @Pattern(regexp = "Sale|Purchase|Adjustment", message = "Transaction type must be either Sale, Purchase, or Adjustment")
    private String transactionType;
}
