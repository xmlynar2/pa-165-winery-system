package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT c FROM Customer c WHERE c.externalId = ?1")
    Customer findByExternalId(Long externalId);
}
