package cz.muni.fi.pa165.exceptions;

public class GrapeDoesNotExistException extends Exception {
    public GrapeDoesNotExistException(String message) {
        super(message);
    }
}
