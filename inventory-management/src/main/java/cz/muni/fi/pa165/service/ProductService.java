package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.ProductAlreadyExistsException;
import cz.muni.fi.pa165.model.Product;
import cz.muni.fi.pa165.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Year;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getWineList(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return productRepository.findAll(pageable).getContent();
    }

    public Product getWineById(Long id) {

        return productRepository.findById(id).orElse(null);
    }

    @Transactional
    public void addWine(Product product) throws ProductAlreadyExistsException {
        if (productRepository.findByProductCode(product.getProductCode()) != null) {
            throw new ProductAlreadyExistsException("Product with code " + product.getProductCode() + " already exists");
        }
        product.setVintageYear(Year.now().getValue());
        productRepository.save(product);
    }

    public Product getProductByCode(String code) {
        return productRepository.findByProductCode(code);
    }


}
