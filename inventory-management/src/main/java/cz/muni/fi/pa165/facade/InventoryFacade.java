package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.exceptions.GrapeAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.GrapeDoesNotExistException;
import cz.muni.fi.pa165.exceptions.ProductAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.ProductNotFoundException;
import cz.muni.fi.pa165.model.Product;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import cz.muni.fi.pa165.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryFacade {

    private final ProductService productService;
    private final InventoryService inventoryService;
    private final TransactionService transactionService;
    private final GrapeService grapeService;
    private final IngredientService ingredientService;
    private final Mapper mapper;

    @Autowired
    public InventoryFacade(ProductService productService, InventoryService inventoryService, TransactionService transactionService, GrapeService grapeService, IngredientService ingredientService, Mapper mapper) {
        this.productService = productService;
        this.inventoryService = inventoryService;
        this.transactionService = transactionService;
        this.grapeService = grapeService;
        this.ingredientService = ingredientService;
        this.mapper = mapper;
    }

    public List<ProductDTO> getWineList(int page, int pageSize) {
        List<Product> wineList = productService.getWineList(page, pageSize);
        return mapper.mapProductListToDTOList(wineList);

    }

    public ProductDTO getWineById(Long id) {
        Product wineById = productService.getWineById(id);
        return wineById == null ? null : mapper.mapProductToDTO(wineById);
    }

    public void addWine(ProductDTO productDTO) throws ProductAlreadyExistsException {
        productService.addWine(mapper.mapProductDTOToProduct(productDTO));
    }

    public void restockWine(ProductDTO productDTO) throws ProductNotFoundException {
        inventoryService.restockWine(productDTO.getCode(), productDTO.getQuantity());
    }

    public void createTransaction(TransactionDTO transactionDTO) {
        transactionService.createTransaction(mapper.mapTransactionDtoToTransaction(transactionDTO));
    }

    public ProductDTO getWineByCode(String code) {
        Product product = productService.getProductByCode(code);
        if (product == null) {
            return null;
        }
        return mapper.mapProductToDTO(product);
    }

    public List<GrapeDTO> getGrapeList(int page, int pageSize) {
        return grapeService.getGrapeList(page, pageSize).stream().map(mapper::mapGrapeToDTO).toList();
    }

    public GrapeDTO getGrapeByCode(String code) {
        return grapeService.getGrapeByCode(code) == null ? null : mapper.mapGrapeToDTO(grapeService.getGrapeByCode(code));
    }

    public void restockGrape(GrapeDTO grapeDTO) throws GrapeDoesNotExistException {
        grapeService.restockGrape(mapper.mapGrapeDTOToGrape(grapeDTO));
    }

    public IngredientDTO getIngredientByName(String name) {
        return ingredientService.getIngredientByName(name) == null ? null : mapper.mapIngredientToDTO(ingredientService.getIngredientByName(name));
    }

    public List<IngredientDTO> getIngredientList(int page, int pageSize) {
        return ingredientService.getIngredientList(page, pageSize).stream().map(mapper::mapIngredientToDTO).toList();
    }

    public void restockIngredient(IngredientDTO ingredientDTO) {
        ingredientService.restockIngredient(mapper.mapIngredientDTOToIngredient(ingredientDTO));
    }

    public void createGrape(GrapeDTO grapeDTO) throws GrapeAlreadyExistsException {
        grapeService.createGrape(mapper.mapGrapeDTOToGrape(grapeDTO));
    }
}
