package cz.muni.fi.pa165.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;


/**
 * Model used to store transactions with wine e.g. purchase of some type of wine
 */
@Data
@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Enums.TransactionType transactionType; // e.g., "Sale," "Purchase," "Stock Adjustment"

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    private int quantity;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime timestamp;

}
