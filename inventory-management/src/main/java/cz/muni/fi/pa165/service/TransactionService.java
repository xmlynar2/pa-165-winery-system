package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.Transaction;
import cz.muni.fi.pa165.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional(readOnly = true)
public class TransactionService {

    private final InventoryService inventoryService;

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(InventoryService inventoryService, TransactionRepository transactionRepository) {
        this.inventoryService = inventoryService;
        this.transactionRepository = transactionRepository;
    }

    @Transactional
    public void createTransaction(Transaction transaction) {
        transaction.setTimestamp(LocalDateTime.now());
        transactionRepository.save(transaction);
        inventoryService.commitTransaction(transaction);
    }
}
