package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IngredientRepository extends JpaRepository<Ingredient, Long> {
    @Query("SELECT i FROM Ingredient i WHERE i.name = ?1")
    Ingredient findByName(String name);
}
