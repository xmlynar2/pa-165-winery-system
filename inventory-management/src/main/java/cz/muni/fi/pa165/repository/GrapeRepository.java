package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Grape;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GrapeRepository extends JpaRepository<Grape, Long> {
    @Query("SELECT g FROM Grape g WHERE g.code = ?1")
    Grape findByCode(String code);
}
