package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.exceptions.GrapeAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.GrapeDoesNotExistException;
import cz.muni.fi.pa165.exceptions.ProductAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.ProductNotFoundException;
import cz.muni.fi.pa165.facade.InventoryFacade;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import cz.muni.fi.pa165.model.validator.interfaces.AddGrapeValidationGroup;
import cz.muni.fi.pa165.model.validator.interfaces.AddWineValidationGroup;
import cz.muni.fi.pa165.model.validator.interfaces.RestockGrapeValidationGroup;
import cz.muni.fi.pa165.model.validator.interfaces.RestockWineValidationGroup;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InventoryManagementController {

    private final InventoryFacade inventoryFacade;

    @Autowired
    public InventoryManagementController(InventoryFacade inventoryFacade) {
        this.inventoryFacade = inventoryFacade;
    }

    @Operation(summary = "Get wine list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of wines"),
    })
    @GetMapping("/wine/list")
    public ResponseEntity<List<ProductDTO>> getWineList(@RequestParam(name = "page", defaultValue = "0") int page, @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return new ResponseEntity<>(inventoryFacade.getWineList(page, pageSize), HttpStatus.OK);
    }

    @Operation(summary = "Get wine by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Wine with given id"),
            @ApiResponse(responseCode = "404", description = "Wine with given id not found")
    })
    @GetMapping("/wine/{id}")
    public ResponseEntity<ProductDTO> getWineById(@PathVariable Long id) {
        ProductDTO wineById = inventoryFacade.getWineById(id);
        return new ResponseEntity<>(wineById, wineById == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @Operation(summary = "Get wine by product code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Wine with given product code"),
            @ApiResponse(responseCode = "404", description = "Wine with given product code not found")
    })
    @GetMapping("/wine/code/{code}")
    public ResponseEntity<ProductDTO> getWineByCode(@PathVariable String code) {
        ProductDTO product = inventoryFacade.getWineByCode(code);
        return new ResponseEntity<>(product, product == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @Operation(summary = "Add wine")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Wine added"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Wine with provided product code already exists")
    })
    @PostMapping("/wine/create")
    public ResponseEntity<HttpStatus> addWine(@Validated(AddWineValidationGroup.class) @RequestBody ProductDTO productDTO) {
        try {
            inventoryFacade.addWine(productDTO);
        } catch (ProductAlreadyExistsException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Restock wine")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Wine restocked"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Wine with provided product code not found")
    })
    @PostMapping("/wine/restock")
    public ResponseEntity<HttpStatus> restockWine(@Validated(RestockWineValidationGroup.class) @RequestBody ProductDTO productDTO) {
        try {
            inventoryFacade.restockWine(productDTO);
        } catch (ProductNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Create transaction")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Transaction created"),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    @PostMapping("/transaction/create")
    public ResponseEntity<HttpStatus> createTransaction(@Validated @RequestBody TransactionDTO transactionDTO) {
        inventoryFacade.createTransaction(transactionDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Get grape list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of grapes")
    })
    @GetMapping("/grape/list")

    public ResponseEntity<List<GrapeDTO>> getGrapeList(@RequestParam(name = "page", defaultValue = "0") int page, @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return new ResponseEntity<>(inventoryFacade.getGrapeList(page, pageSize), HttpStatus.OK);
    }

    @Operation(summary = "Get grape by code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape with given code"),
            @ApiResponse(responseCode = "404", description = "Grape with given code does not exist"),
    })
    @GetMapping("/grape/{code}")
    public ResponseEntity<GrapeDTO> getGrapeByCode(@PathVariable String code) {
        GrapeDTO grapeByCode = inventoryFacade.getGrapeByCode(code);
        return new ResponseEntity<>(grapeByCode, grapeByCode == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @Operation(summary = "Create grape")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Grape created"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Grape with provided code already exists")
    })
    @PostMapping("/grape")
    public ResponseEntity<HttpStatus> createGrape(@Validated(AddGrapeValidationGroup.class) @RequestBody GrapeDTO grapeDTO) {
        try {
            inventoryFacade.createGrape(grapeDTO);
        } catch (GrapeAlreadyExistsException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Stock grape")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Grape stocked"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Grape with provided code does not exist")
    })
    @PostMapping("/grape/restock")
    public ResponseEntity<HttpStatus> restockGrape(@Validated(RestockGrapeValidationGroup.class) @RequestBody GrapeDTO grapeDTO) {
        try {
            inventoryFacade.restockGrape(grapeDTO);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (GrapeDoesNotExistException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Get ingredient list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of ingredients")
    })
    @GetMapping("/ingredient/list")
    public ResponseEntity<List<IngredientDTO>> getIngredientList(@RequestParam(name = "page", defaultValue = "0") int page, @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return new ResponseEntity<>(inventoryFacade.getIngredientList(page, pageSize), HttpStatus.OK);
    }

    @Operation(summary = "Get ingredient by name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ingredient with given name"),
            @ApiResponse(responseCode = "404", description = "Ingredient with given name does not exist")
    })
    @GetMapping("/ingredient/{name}")
    public ResponseEntity<IngredientDTO> getIngredientByName(@PathVariable String name) {
        IngredientDTO ingredientByName = inventoryFacade.getIngredientByName(name);
        return new ResponseEntity<>(ingredientByName, ingredientByName == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    @Operation(summary = "Stock ingredient")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Ingredient stocked"),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    @PostMapping("/ingredient/restock")
    public ResponseEntity<HttpStatus> restockIngredient(@Validated @RequestBody IngredientDTO ingredientDTO) {
        inventoryFacade.restockIngredient(ingredientDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}
