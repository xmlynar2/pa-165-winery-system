package cz.muni.fi.pa165.model;

public class Enums {
    public enum TransactionType {
        Sale,
        Purchase,
        Adjustment
    }
}
