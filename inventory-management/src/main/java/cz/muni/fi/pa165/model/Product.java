package cz.muni.fi.pa165.model;


import jakarta.persistence.*;
import lombok.Data;

/**
 * Model used for storing each type of wine with its information
 */
@Entity
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long id;
    private String productCode;
    private String productName;
    private String description;
    private double price;
    private int quantityAvailable;
    private int vintageYear;

    public Product(String productCode, String name, String description, double price, int quantityAvailable) {
        this.productCode = productCode;
        this.productName = name;
        this.description = description;
        this.price = price;
        this.quantityAvailable = quantityAvailable;
    }

    public Product(String code, int quantityAvailable) {
        this.productCode = code;
        this.quantityAvailable = quantityAvailable;
    }

    public Product() {
    }
}
