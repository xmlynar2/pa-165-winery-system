package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.Customer;
import cz.muni.fi.pa165.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public void createCustomer() {
        // create customer
    }

    public void deleteCustomer() {
        // delete customer
    }

    public void updateCustomer() {
        // update customer
    }

    public void getCustomerById() {
        // get customer
    }

    public Customer getCustomerByExternalId(Long externalId) {
        return customerRepository.findByExternalId(externalId);
    }
}
