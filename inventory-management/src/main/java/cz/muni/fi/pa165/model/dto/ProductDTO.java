package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.validator.interfaces.AddWineValidationGroup;
import cz.muni.fi.pa165.model.validator.interfaces.RestockWineValidationGroup;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class ProductDTO {
    @NotNull(message = "Product code cannot be null", groups = {AddWineValidationGroup.class, RestockWineValidationGroup.class})
    @Size(min = 1, max = 100, message = "Product code must be between 1 and 100 characters", groups = {AddWineValidationGroup.class, RestockWineValidationGroup.class})
    private String code;
    @NotNull(message = "Product name cannot be null", groups = AddWineValidationGroup.class)
    @Size(min = 1, max = 100, message = "Product name must be between 1 and 100 characters", groups = AddWineValidationGroup.class)
    private String name;
    @NotNull(message = "Product description cannot be null", groups = AddWineValidationGroup.class)
    private String description;
    @NotNull(message = "Product quantity cannot be null", groups = RestockWineValidationGroup.class)
    private int quantity;
    @NotNull(message = "Product price cannot be null", groups = AddWineValidationGroup.class)
    private double price;

    public ProductDTO(String name, String description, int quantity, String code) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.code = code;
    }

    public ProductDTO() {
    }
}
