package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("SELECT p FROM Product p WHERE p.productCode = ?1")
    Product findByProductCode(String code);

}
