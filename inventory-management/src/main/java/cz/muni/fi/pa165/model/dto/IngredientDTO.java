package cz.muni.fi.pa165.model.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class IngredientDTO {
    @NotNull(message = "Ingredient name cannot be null")
    private String name;
    @NotNull(message = "Ingredient quantity cannot be null")
    private int quantity;
    private String unit;
}
