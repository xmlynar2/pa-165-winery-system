package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.Ingredient;
import cz.muni.fi.pa165.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class IngredientService {

    private final IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public Ingredient getIngredientByName(String name) {
        return ingredientRepository.findByName(name);
    }

    public List<Ingredient> getIngredientList(int page, int pageSize) {
        Pageable pageable = Pageable.ofSize(pageSize).withPage(page);
        return ingredientRepository.findAll(pageable).getContent();
    }

    @Transactional
    public void restockIngredient(Ingredient ingredient) {
        Ingredient found = ingredientRepository.findByName(ingredient.getName());
        if (found == null) {
            ingredientRepository.save(ingredient);
        } else {
            found.setQuantity(found.getQuantity() + ingredient.getQuantity());
            ingredientRepository.save(found);
        }
    }
}
