package cz.muni.fi.pa165.model.dto;

import lombok.Data;

@Data
public class SupplierDTO {

    private String supplierName;
    private String contactInfo;
    private String region;
}
