package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Ingredient;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class IngredientRepositoryTest {

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private IngredientRepository ingredientRepository;

  @Test
  void findByName_Success() {
    // given
    Ingredient ingredient = new Ingredient();
    ingredient.setName("Ingredient1");
    entityManager.persist(ingredient);
    entityManager.flush();

    // when
    Ingredient found = ingredientRepository.findByName(ingredient.getName());

    // then
    assertNotNull(found);
    assertEquals(ingredient.getName(), found.getName());
  }

  @Test
  void findByName_NotFound() {
    // given
    String nonExistentName = "NON_EXISTENT_NAME";

    // when
    Ingredient found = ingredientRepository.findByName(nonExistentName);

    // then
    assertNull(found);
  }
}
