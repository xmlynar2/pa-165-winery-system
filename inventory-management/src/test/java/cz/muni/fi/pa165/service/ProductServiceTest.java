package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.ProductAlreadyExistsException;
import cz.muni.fi.pa165.model.Product;
import cz.muni.fi.pa165.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetWineList() {
        Product wine1 = new Product();
        Product wine2 = new Product();
        List<Product> wineList = new ArrayList<>();
        wineList.add(wine1);
        wineList.add(wine2);

        Page<Product> pro= Mockito.mock(Page.class);
        Pageable pageable = PageRequest.of(0,10);

        when(productRepository.findAll(pageable)).thenReturn(pro);
        when(pro.getContent()).thenReturn(wineList);

        List<Product> result = productService.getWineList(0, 10);

        verify(productRepository, times(1)).findAll(pageable);
        assertEquals(2, result.size());
        assertEquals(wine1, result.get(0));
        assertEquals(wine2, result.get(1));
    }

    @Test
    public void testGetWineById() {
        long wineId = 1L;
        Product wine = new Product();
        wine.setId(wineId);

        when(productRepository.findById(wineId)).thenReturn(Optional.of(wine));

        Product result = productService.getWineById(wineId);

        verify(productRepository, times(1)).findById(wineId);
        assertEquals(wine, result);
    }

    @Test
    public void testAddWine_Success() throws ProductAlreadyExistsException {
        String productCode = "WINE001";
        Product wine = new Product();
        wine.setProductCode(productCode);

        when(productRepository.findByProductCode(productCode)).thenReturn(null);

        productService.addWine(wine);

        verify(productRepository, times(1)).findByProductCode(productCode);
        verify(productRepository, times(1)).save(wine);
        assertEquals(Year.now().getValue(), wine.getVintageYear());
    }

    @Test
    public void testAddWine_ProductAlreadyExists() {
        String productCode = "WINE001";
        Product wine = new Product();
        wine.setProductCode(productCode);

        when(productRepository.findByProductCode(productCode)).thenReturn(wine);

        assertThrows(ProductAlreadyExistsException.class, () -> {
            productService.addWine(wine);
        });

        verify(productRepository, times(1)).findByProductCode(productCode);
        verify(productRepository, never()).save(wine);
    }

    @Test
    public void testGetProductByCode() {
        String productCode = "WINE001";
        Product wine = new Product();
        wine.setProductCode(productCode);

        when(productRepository.findByProductCode(productCode)).thenReturn(wine);

        Product result = productService.getProductByCode(productCode);

        verify(productRepository, times(1)).findByProductCode(productCode);
        assertEquals(wine, result);
    }
}