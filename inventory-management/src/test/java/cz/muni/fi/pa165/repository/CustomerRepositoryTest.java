package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Customer;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class CustomerRepositoryTest {

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private CustomerRepository customerRepository;

  @Test
  void findByExternalId_Success() {
    // given
    Customer customer = new Customer();
    customer.setExternalId(123L);
    entityManager.persist(customer);
    entityManager.flush();

    // when
    Customer found = customerRepository.findByExternalId(customer.getExternalId());

    // then
    assertNotNull(found);
    assertEquals(customer.getExternalId(), found.getExternalId());
  }
  @Test
  void findByExternalId_NotFound() {
    // given
    Long nonExistentId = 999L;

    // when
    Customer found = customerRepository.findByExternalId(nonExistentId);

    // then
    assertNull(found);
  }
}
