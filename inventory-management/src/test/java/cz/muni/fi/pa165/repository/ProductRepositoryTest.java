package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Transaction;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class TransactionRepositoryTest {

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private TransactionRepository transactionRepository;

  @Test
  void findById_Success() {
    // given
    Transaction transaction = new Transaction();
    entityManager.persist(transaction);
    entityManager.flush();

    // when
    Transaction found = transactionRepository.findById(transaction.getId()).orElse(null);

    // then
    assertNotNull(found);
    assertEquals(transaction.getId(), found.getId());
  }

  @Test
  void findById_NotFound() {
    // given
    Long nonExistentId = 999L;

    // when
    Transaction found = transactionRepository.findById(nonExistentId).orElse(null);

    // then
    assertNull(found);
  }
}
