package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.InventoryFacade;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class InventoryManagementTest {

    @Mock
    private InventoryFacade inventoryFacade;

    @InjectMocks
    private InventoryManagementController inventoryManagement;

    private final ProductDTO testProduct = new ProductDTO("test", "test", 2, "code");
    private final GrapeDTO testGrape = new GrapeDTO();

    private final IngredientDTO testIngredient = new IngredientDTO();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getWineList() {
        when(inventoryFacade.getWineList(0, 10)).thenReturn(List.of(testProduct));

        List<ProductDTO> result = inventoryManagement.getWineList(0,10).getBody();

        assertEquals(List.of(testProduct), result);
    }

    @Test
    void getWineById() {
        when(inventoryFacade.getWineById(1L)).thenReturn(testProduct);

        ProductDTO result = inventoryManagement.getWineById(1L).getBody();

        assertEquals(testProduct, result);
    }

    @Test
    void getWineByCode() {
        when(inventoryFacade.getWineByCode("test-code")).thenReturn(testProduct);

        ProductDTO result = inventoryManagement.getWineByCode("test-code").getBody();

        assertEquals(testProduct, result);
    }

    @Test
    void getGrapeList() {
        when(inventoryFacade.getGrapeList(0,10)).thenReturn(List.of(testGrape));

        List<GrapeDTO> result = inventoryManagement.getGrapeList(0,10).getBody();

        assertEquals(List.of(testGrape), result);
    }

    @Test
    void getGrapeByCode() {
        when(inventoryFacade.getGrapeByCode("test-code")).thenReturn(testGrape);

        GrapeDTO result = inventoryManagement.getGrapeByCode("test-code").getBody();

        assertEquals(testGrape, result);
    }

    @Test
    void getIngredientList() {
        when(inventoryFacade.getIngredientList(0,10)).thenReturn(List.of(testIngredient));

        List<IngredientDTO> result = inventoryManagement.getIngredientList(0,10).getBody();

        assertEquals(List.of(testIngredient), result);
    }

    @Test
    void getIngredientByName() {
        when(inventoryFacade.getIngredientByName("test-name")).thenReturn(testIngredient);

        IngredientDTO result = inventoryManagement.getIngredientByName("test-name").getBody();

        assertEquals(testIngredient, result);
    }
}
