package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.Ingredient;
import cz.muni.fi.pa165.repository.IngredientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class IngredientServiceTest {

    @Mock
    private IngredientRepository ingredientRepository;

    @InjectMocks
    private IngredientService ingredientService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetIngredientByName() {
        Ingredient ingredient = new Ingredient();
        ingredient.setName("Salt");

        when(ingredientRepository.findByName("Salt")).thenReturn(ingredient);

        Ingredient result = ingredientService.getIngredientByName("Salt");

        verify(ingredientRepository, times(1)).findByName("Salt");
        assertEquals("Salt", result.getName());
    }

    @Test
    public void testGetIngredientList() {
        Ingredient salt = new Ingredient();
        salt.setName("Salt");

        Ingredient sugar = new Ingredient();
        sugar.setName("Sugar");

        List<Ingredient> ingredients = List.of(salt, sugar);

        Page<Ingredient> pro= Mockito.mock(Page.class);
        Pageable pageable = PageRequest.of(0,10);

        when(ingredientRepository.findAll(pageable)).thenReturn(pro);
        when(pro.getContent()).thenReturn(ingredients);

        List<Ingredient> result = ingredientService.getIngredientList(0,10);

        verify(ingredientRepository, times(1)).findAll(pageable);
        assertEquals(2, result.size());
        assertEquals("Salt", result.get(0).getName());
        assertEquals("Sugar", result.get(1).getName());
    }

    @Test
    public void testRestockIngredient() {
        Ingredient salt = new Ingredient();
        salt.setName("Salt");
        salt.setQuantity(100);

        when(ingredientRepository.findByName("Salt")).thenReturn(salt);

        ingredientService.restockIngredient(salt);

        verify(ingredientRepository, times(1)).findByName("Salt");
        verify(ingredientRepository, times(1)).save(salt);
        assertEquals(200, salt.getQuantity());
    }
}