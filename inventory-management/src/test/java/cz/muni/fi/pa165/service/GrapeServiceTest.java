package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.GrapeAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.GrapeDoesNotExistException;
import cz.muni.fi.pa165.model.Grape;
import cz.muni.fi.pa165.repository.GrapeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GrapeServiceTest {

    @Mock
    private GrapeRepository grapeRepository;

    @InjectMocks
    private GrapeService grapeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetGrapeList() {
        Grape grape1 = new Grape();
        Grape grape2 = new Grape();
        List<Grape> grapeList = new ArrayList<>();
        grapeList.add(grape1);
        grapeList.add(grape2);


        Page<Grape> pro= Mockito.mock(Page.class);
        Pageable pageable = PageRequest.of(0,10);

        when(grapeRepository.findAll(pageable)).thenReturn(pro);
        when(pro.getContent()).thenReturn(grapeList);

        List<Grape> result = grapeService.getGrapeList(0,10);

        verify(grapeRepository, times(1)).findAll(pageable);
        assertEquals(2, result.size());
        assertEquals(grape1, result.get(0));
        assertEquals(grape2, result.get(1));
    }

    @Test
    public void testGetGrapeByCode() {
        String grapeCode = "GRAPE001";
        Grape grape = new Grape();
        grape.setCode(grapeCode);

        when(grapeRepository.findByCode(grapeCode)).thenReturn(grape);

        Grape result = grapeService.getGrapeByCode(grapeCode);

        verify(grapeRepository, times(1)).findByCode(grapeCode);
        assertEquals(grape, result);
    }

    @Test
    public void testRestockGrape() throws GrapeDoesNotExistException {
        String grapeCode = "GRAPE001";
        Grape grape = new Grape();
        grape.setCode(grapeCode);
        grape.setQuantityAvailable(10);

        Grape existingGrape = new Grape();
        existingGrape.setCode(grapeCode);
        existingGrape.setQuantityAvailable(5);

        when(grapeRepository.findByCode(grapeCode)).thenReturn(existingGrape);

        grapeService.restockGrape(grape);

        verify(grapeRepository, times(1)).findByCode(grapeCode);
        verify(grapeRepository, times(1)).save(existingGrape);
        assertEquals(15, existingGrape.getQuantityAvailable()); // 5 (initial quantity) + 10 (restocked quantity)
    }

    @Test
    public void testCreateGrape_Success() {
        String grapeCode = "GRAPE001";
        Grape grape = new Grape();
        grape.setCode(grapeCode);

        when(grapeRepository.findByCode(grapeCode)).thenReturn(null);

        try {
            grapeService.createGrape(grape);
        } catch (GrapeAlreadyExistsException e) {
            throw new RuntimeException(e);
        }

        verify(grapeRepository, times(1)).findByCode(grapeCode);
        verify(grapeRepository, times(1)).save(grape);
    }
}