package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.exceptions.GrapeDoesNotExistException;
import cz.muni.fi.pa165.exceptions.ProductAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.ProductNotFoundException;
import cz.muni.fi.pa165.model.Grape;
import cz.muni.fi.pa165.model.Ingredient;
import cz.muni.fi.pa165.model.Product;
import cz.muni.fi.pa165.model.Transaction;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import cz.muni.fi.pa165.service.GrapeService;
import cz.muni.fi.pa165.service.IngredientService;
import cz.muni.fi.pa165.service.InventoryService;
import cz.muni.fi.pa165.service.Mapper;
import cz.muni.fi.pa165.service.ProductService;
import cz.muni.fi.pa165.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InventoryFacadeTest {

    @Mock
    private ProductService productService;
    @Mock
    private InventoryService inventoryService;
    @Mock
    private TransactionService transactionService;
    @Mock
    private GrapeService grapeService;
    @Mock
    private IngredientService ingredientService;
    @Mock
    private Mapper mapper;
    @InjectMocks
    private InventoryFacade inventoryFacade;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldReturnEmptyListWhenNoWinesAvailable() {
        // arrange
        when(productService.getWineList(0, 10)).thenReturn(List.of());

        // act
        List<ProductDTO> result = inventoryFacade.getWineList(0, 10);

        // assert
        assertTrue(result.isEmpty());
    }

    @Test
    void shouldReturnWineListWhenWinesAreAvailable() {
        // arrange
        Product testProduct = new Product();
        ProductDTO testProductDTO = new ProductDTO();
        when(productService.getWineList(0, 10)).thenReturn(List.of(testProduct));
        when(mapper.mapProductListToDTOList(List.of(testProduct))).thenReturn(List.of(testProductDTO));

        // act
        List<ProductDTO> result = inventoryFacade.getWineList(0, 10);

        // assert
        assertFalse(result.isEmpty());
        assertEquals(testProductDTO, result.getFirst());
    }

    @Test
    void shouldReturnNullWhenNoWineWithGivenIdExists() {
        // arrange
        when(productService.getWineById(1L)).thenReturn(null);

        // act
        ProductDTO result = inventoryFacade.getWineById(1L);

        // assert
        assertNull(result);
    }

    @Test
    void shouldReturnWineWhenWineWithGivenIdExists() {
        // arrange
        Product testProduct = new Product();
        ProductDTO testProductDTO = new ProductDTO();
        when(productService.getWineById(1L)).thenReturn(testProduct);
        when(mapper.mapProductToDTO(testProduct)).thenReturn(testProductDTO);

        // act
        ProductDTO result = inventoryFacade.getWineById(1L);

        // assert
        assertEquals(testProductDTO, result);
    }

    @Test
    void shouldThrowExceptionWhenAddingExistingWine() throws ProductAlreadyExistsException {
        // arrange
        ProductDTO testProductDTO = new ProductDTO();
        when(mapper.mapProductDTOToProduct(testProductDTO)).thenReturn(new Product());
        doThrow(ProductAlreadyExistsException.class).when(productService).addWine(new Product());

        // act & assert
        assertThrows(ProductAlreadyExistsException.class, () -> inventoryFacade.addWine(testProductDTO));
    }

    @Test
    void shouldAddWineSuccessfullyWhenWineDoesNotExist() throws ProductAlreadyExistsException {
        // arrange
        ProductDTO testProductDTO = new ProductDTO();
        Product testProduct = new Product();
        when(mapper.mapProductDTOToProduct(testProductDTO)).thenReturn(testProduct);

        // act & assert
        assertDoesNotThrow(() -> inventoryFacade.addWine(testProductDTO));
        verify(productService).addWine(testProduct);
    }

    @Test
    void shouldThrowExceptionWhenRestockingNonExistentWine() throws ProductNotFoundException {
        // arrange
        ProductDTO testProductDTO = new ProductDTO();
        testProductDTO.setCode("non-existent-code");
        doThrow(ProductNotFoundException.class).when(inventoryService).restockWine(testProductDTO.getCode(), testProductDTO.getQuantity());

        // act & assert
        assertThrows(ProductNotFoundException.class, () -> inventoryFacade.restockWine(testProductDTO));
    }

    @Test
    void shouldRestockWineSuccessfullyWhenWineExists() throws ProductNotFoundException {
        // arrange
        ProductDTO testProductDTO = new ProductDTO();
        testProductDTO.setCode("existent-code");
        testProductDTO.setQuantity(10);

        // act & assert
        assertDoesNotThrow(() -> inventoryFacade.restockWine(testProductDTO));
        verify(inventoryService).restockWine(testProductDTO.getCode(), testProductDTO.getQuantity());
    }

    @Test
    void shouldCreateTransactionSuccessfully() {
        // arrange
        TransactionDTO testTransactionDTO = new TransactionDTO();
        Transaction testTransaction = new Transaction();
        when(mapper.mapTransactionDtoToTransaction(testTransactionDTO)).thenReturn(testTransaction);

        // act & assert
        assertDoesNotThrow(() -> inventoryFacade.createTransaction(testTransactionDTO));
        verify(transactionService).createTransaction(testTransaction);
    }

    @Test
    void shouldReturnNullWhenNoWineWithGivenCodeExists() {
        // arrange
        when(productService.getProductByCode(anyString())).thenReturn(null);

        // act
        ProductDTO result = inventoryFacade.getWineByCode("non-existent-code");

        // assert
        assertNull(result);
    }

    @Test
    void shouldReturnWineWhenWineWithGivenCodeExists() {
        // arrange
        Product testProduct = new Product();
        ProductDTO testProductDTO = new ProductDTO();
        when(productService.getProductByCode(anyString())).thenReturn(testProduct);
        when(mapper.mapProductToDTO(testProduct)).thenReturn(testProductDTO);

        // act
        ProductDTO result = inventoryFacade.getWineByCode("existent-code");

        // assert
        assertEquals(testProductDTO, result);
    }

    @Test
    void shouldReturnEmptyListWhenNoGrapesAvailable() {
        // arrange
        when(grapeService.getGrapeList(0, 10)).thenReturn(List.of());

        // act
        List<GrapeDTO> result = inventoryFacade.getGrapeList(0, 10);

        // assert
        assertTrue(result.isEmpty());
    }

    @Test
    void shouldReturnGrapeListWhenGrapesAreAvailable() {
        // arrange
        Grape testGrape = new Grape();
        GrapeDTO testGrapeDTO = new GrapeDTO();
        when(grapeService.getGrapeList(0, 10)).thenReturn(List.of(testGrape));
        when(mapper.mapGrapeToDTO(testGrape)).thenReturn(testGrapeDTO);

        // act
        List<GrapeDTO> result = inventoryFacade.getGrapeList(0, 10);

        // assert
        assertFalse(result.isEmpty());
        assertEquals(testGrapeDTO, result.getFirst());
    }

    @Test
    void shouldReturnNullWhenNoGrapeWithGivenCodeExists() {
        // arrange
        when(grapeService.getGrapeByCode(anyString())).thenReturn(null);

        // act
        GrapeDTO result = inventoryFacade.getGrapeByCode("non-existent-code");

        // assert
        assertNull(result);
    }

    @Test
    void shouldReturnGrapeWhenGrapeWithGivenCodeExists() {
        // arrange
        Grape testGrape = new Grape();
        GrapeDTO testGrapeDTO = new GrapeDTO();
        when(grapeService.getGrapeByCode(anyString())).thenReturn(testGrape);
        when(mapper.mapGrapeToDTO(testGrape)).thenReturn(testGrapeDTO);

        // act
        GrapeDTO result = inventoryFacade.getGrapeByCode("existent-code");

        // assert
        assertEquals(testGrapeDTO, result);
    }

    @Test
    void shouldThrowExceptionWhenRestockingNonExistentGrape() throws GrapeDoesNotExistException {
        // arrange
        Grape testGrape = new Grape();
        GrapeDTO testGrapeDTO = new GrapeDTO();
        testGrapeDTO.setCode("non-existent-code");
        when(mapper.mapGrapeDTOToGrape(testGrapeDTO)).thenReturn(testGrape);
        doThrow(GrapeDoesNotExistException.class).when(grapeService).restockGrape(testGrape);

        // act & assert
        assertThrows(GrapeDoesNotExistException.class, () -> inventoryFacade.restockGrape(testGrapeDTO));
    }

    @Test
    void shouldRestockGrapeSuccessfullyWhenGrapeExists() throws GrapeDoesNotExistException {
        // arrange
        GrapeDTO testGrapeDTO = new GrapeDTO();
        testGrapeDTO.setCode("existent-code");
        testGrapeDTO.setQuantity(10);

        // act & assert
        assertDoesNotThrow(() -> inventoryFacade.restockGrape(testGrapeDTO));
        verify(grapeService).restockGrape(mapper.mapGrapeDTOToGrape(testGrapeDTO));
    }

    @Test
    void shouldReturnNullWhenNoIngredientWithGivenNameExists() {
        // arrange
        when(ingredientService.getIngredientByName(anyString())).thenReturn(null);

        // act
        IngredientDTO result = inventoryFacade.getIngredientByName("non-existent-name");

        // assert
        assertNull(result);
    }

    @Test
    void shouldReturnIngredientWhenIngredientWithGivenNameExists() {
        // arrange
        Ingredient testIngredient = new Ingredient();
        IngredientDTO testIngredientDTO = new IngredientDTO();
        when(ingredientService.getIngredientByName(anyString())).thenReturn(testIngredient);
        when(mapper.mapIngredientToDTO(testIngredient)).thenReturn(testIngredientDTO);

        // act
        IngredientDTO result = inventoryFacade.getIngredientByName("existent-name");

        // assert
        assertEquals(testIngredientDTO, result);
    }

    @Test
    void shouldReturnEmptyListWhenNoIngredientsAvailable() {
        // arrange
        when(ingredientService.getIngredientList(0, 10)).thenReturn(List.of());

        // act
        List<IngredientDTO> result = inventoryFacade.getIngredientList(0, 10);

        // assert
        assertTrue(result.isEmpty());
    }

    @Test
    void shouldReturnIngredientListWhenIngredientsAreAvailable() {
        // arrange
        Ingredient testIngredient = new Ingredient();
        IngredientDTO testIngredientDTO = new IngredientDTO();
        when(ingredientService.getIngredientList(0, 10)).thenReturn(List.of(testIngredient));
        when(mapper.mapIngredientToDTO(testIngredient)).thenReturn(testIngredientDTO);

        // act
        List<IngredientDTO> result = inventoryFacade.getIngredientList(0, 10);

        // assert
        assertFalse(result.isEmpty());
        assertEquals(testIngredientDTO, result.getFirst());
    }

    @Test
    void shouldRestockIngredientSuccessfullyWhenIngredientExists() {
        // arrange
        IngredientDTO testIngredientDTO = new IngredientDTO();
        testIngredientDTO.setName("existent-name");
        testIngredientDTO.setQuantity(10);

        // act & assert
        assertDoesNotThrow(() -> inventoryFacade.restockIngredient(testIngredientDTO));
        verify(ingredientService).restockIngredient(mapper.mapIngredientDTOToIngredient(testIngredientDTO));
    }
}
