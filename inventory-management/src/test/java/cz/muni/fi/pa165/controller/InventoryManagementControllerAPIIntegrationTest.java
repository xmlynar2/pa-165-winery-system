package cz.muni.fi.pa165.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import cz.muni.fi.pa165.model.Grape;
import cz.muni.fi.pa165.model.Ingredient;
import cz.muni.fi.pa165.model.InventoryItem;
import cz.muni.fi.pa165.model.Product;
import cz.muni.fi.pa165.repository.GrapeRepository;
import cz.muni.fi.pa165.repository.IngredientRepository;
import cz.muni.fi.pa165.repository.InventoryItemRepository;
import cz.muni.fi.pa165.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@WithMockUser(authorities = {"SCOPE_test_read", "SCOPE_test_write"})
class InventoryManagementControllerAPIIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private InventoryItemRepository inventoryItemRepository;
    @Autowired
    private GrapeRepository grapeRepository;
    @Autowired
    private IngredientRepository ingredientRepository;


    @BeforeEach
    void setUp() {
        Product wine = productRepository.findByProductCode("test-code");
        if (wine == null) {
            wine = new Product();
            wine.setProductName("Test Wine");
            wine.setProductCode("test-code");
            wine.setDescription("Test Wine Description");
            wine.setQuantityAvailable(100);
            wine.setPrice(10.0);
            productRepository.save(wine);
        }

        InventoryItem inventoryItem = inventoryItemRepository.findByProductProductCode(wine.getProductCode());
        if (inventoryItem == null) {
            inventoryItem = new InventoryItem();
            inventoryItem.setProduct(wine);
            inventoryItem.setQuantity(100);
            inventoryItemRepository.save(inventoryItem);
        }

        Grape testGrape = grapeRepository.findByCode("test-code");
        if (testGrape == null) {
            testGrape = new Grape();
            testGrape.setName("Test Grape");
            testGrape.setCode("test-code");
            testGrape.setDescription("Test Grape Description");
            grapeRepository.save(testGrape);
        }

        Ingredient testIngredient = ingredientRepository.findByName("Test Ingredient");
        if (testIngredient == null) {
            testIngredient = new Ingredient();
            testIngredient.setName("Test Ingredient");
            ingredientRepository.save(testIngredient);
        }
    }

    @Test
    void getWineList() throws Exception {
        mockMvc.perform(get("/wine/list"))
                .andExpect(status().isOk());
    }


    @Test
    void getWineById_Success() throws Exception {

        mockMvc.perform(get("/wine/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Test Wine")))
                .andExpect(jsonPath("$.code", is("test-code")));
    }

    @Test
    void getWineById_Failure() throws Exception {
        mockMvc.perform(get("/wine/999"))
                .andExpect(status().isNotFound());
    }

    @Test
    void getWineByCode_Success() throws Exception {
        mockMvc.perform(get("/wine/code/test-code"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Test Wine")))
                .andExpect(jsonPath("$.code", is("test-code")));
    }

    @Test
    void getWineByCode_Failure() throws Exception {
        mockMvc.perform(get("/wine/code/non-existing-code"))
                .andExpect(status().isNotFound());
    }

    @Test
    void addWine_Success() throws Exception {
        // Create a new wine
        ProductDTO newWine = new ProductDTO();
        newWine.setName("New Wine");
        newWine.setCode("new-code");
        newWine.setDescription("New Wine Description");
        newWine.setPrice(10.0);

        mockMvc.perform(post("/wine/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(newWine)))
                .andExpect(status().isCreated());
    }

    @Test
    void addWine_Failure() throws Exception {
        ProductDTO newWine = new ProductDTO();
        newWine.setName("New Wine");
        newWine.setCode("test-code");
        newWine.setDescription("New Wine Description");
        newWine.setPrice(10.0);

        mockMvc.perform(post("/wine/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(newWine)))
                .andExpect(status().isConflict());
    }

    @Test
    void restockWine_Success() throws Exception {
        // Create a new wine
        ProductDTO restockWine = new ProductDTO();
        restockWine.setName("Test Wine");
        restockWine.setCode("test-code");

        mockMvc.perform(post("/wine/restock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(restockWine)))
                .andExpect(status().isOk());
    }

    @Test
    void restockWine_Failure() throws Exception {
        ProductDTO restockWine = new ProductDTO();
        restockWine.setName("Non-existing Wine");
        restockWine.setCode("non-existing-code");

        mockMvc.perform(post("/wine/restock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(restockWine)))
                .andExpect(status().isNotFound());
    }

    @Test
    void createTransaction_Success() throws Exception {
        TransactionDTO newTransaction = new TransactionDTO();
        newTransaction.setProductCode("test-code");
        newTransaction.setTransactionType("Sale");
        newTransaction.setQuantity(10);

        mockMvc.perform(post("/transaction/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(newTransaction)))
                .andExpect(status().isCreated());

    }

    @Test
    void getGrapeList_Success() throws Exception {
        // Perform the request and assert the response
        mockMvc.perform(get("/grape/list"))
                .andExpect(status().isOk());
    }

    @Test
    void getGrapeByCode_Success() throws Exception {
        // Perform the request and assert the response
        mockMvc.perform(get("/grape/test-code"))
                .andExpect(status().isOk());
    }

    @Test
    void getGrapeByCode_Failure() throws Exception {
        // Perform the request and assert the response
        mockMvc.perform(get("/grape/non-existing-code"))
                .andExpect(status().isNotFound());
    }

    @Test
    void createGrape_Success() throws Exception {
        // Create a new grape
        GrapeDTO newGrape = new GrapeDTO();
        newGrape.setName("New Grape");
        newGrape.setCode("new-code");
        newGrape.setDescription("New Grape Description");

        // Perform the request and assert the response
        mockMvc.perform(post("/grape")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(newGrape)))
                .andExpect(status().isCreated());
    }

    @Test
    void createGrape_Failure() throws Exception {
        // Create a new grape with an existing code
        GrapeDTO newGrape = new GrapeDTO();
        newGrape.setName("New Grape");
        newGrape.setCode("test-code");
        newGrape.setDescription("New Grape Description");

        // Perform the request and assert the response
        mockMvc.perform(post("/grape")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(newGrape)))
                .andExpect(status().isConflict());
    }

    @Test
    void restockGrape_Success() throws Exception {
        // Create a new grape
        GrapeDTO restockGrape = new GrapeDTO();
        restockGrape.setName("Test Grape");
        restockGrape.setCode("test-code");

        // Perform the request and assert the response
        mockMvc.perform(post("/grape/restock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(restockGrape)))
                .andExpect(status().isOk());
    }

    @Test
    void restockGrape_Failure() throws Exception {
        // Create a new grape with a non-existing code
        GrapeDTO restockGrape = new GrapeDTO();
        restockGrape.setName("Non-existing Grape");
        restockGrape.setCode("non-existing-code");

        // Perform the request and assert the response
        mockMvc.perform(post("/grape/restock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(restockGrape)))
                .andExpect(status().isNotFound());
    }

    @Test
    void getIngredientList_Success() throws Exception {
        // Perform the request and assert the response
        mockMvc.perform(get("/ingredient/list"))
                .andExpect(status().isOk());
    }

    @Test
    void getIngredientByName_Success() throws Exception {
        // Perform the request and assert the response
        mockMvc.perform(get("/ingredient/Test Ingredient"))
                .andExpect(status().isOk());
    }

    @Test
    void getIngredientByName_Failure() throws Exception {
        // Perform the request and assert the response
        mockMvc.perform(get("/ingredient/non-existing-name"))
                .andExpect(status().isNotFound());
    }

    @Test
    void restockIngredient_Success() throws Exception {
        // Create a new ingredient
        IngredientDTO restockIngredient = new IngredientDTO();
        restockIngredient.setName("Test Ingredient");

        // Perform the request and assert the response
        mockMvc.perform(post("/ingredient/restock")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(restockIngredient)))
                .andExpect(status().isCreated());
    }
}
