package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.Grape;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class GrapeRepositoryTest {

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private GrapeRepository grapeRepository;

  @Test
  void findByCode_Success() {
    // given
    Grape grape = new Grape();
    grape.setCode("GRAPE001");
    entityManager.persist(grape);
    entityManager.flush();

    // when
    Grape found = grapeRepository.findByCode(grape.getCode());

    // then
    assertNotNull(found);
    assertEquals(grape.getCode(), found.getCode());
  }

  @Test
  void findByCode_NotFound() {
    // given
    String nonExistentCode = "NON_EXISTENT_CODE";

    // when
    Grape found = grapeRepository.findByCode(nonExistentCode);

    // then
    assertNull(found);
  }
}
