package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.InventoryItem;
import cz.muni.fi.pa165.model.Product;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class InventoryItemRepositoryTest {

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private InventoryItemRepository inventoryItemRepository;

  @Test
  void findByProductProductCode_Success() {
    // given
    Product product = new Product();
    product.setProductCode("PRODUCT001");
    entityManager.persist(product);
    entityManager.flush();

    InventoryItem inventoryItem = new InventoryItem();
    inventoryItem.setProduct(product);
    entityManager.persist(inventoryItem);
    entityManager.flush();

    // when
    InventoryItem found = inventoryItemRepository.findByProductProductCode(product.getProductCode());

    // then
    assertNotNull(found);
    assertEquals(inventoryItem.getProduct().getProductCode(), found.getProduct().getProductCode());
  }

  @Test
  void findByProductProductCode_NotFound() {
    // given
    String nonExistentProductCode = "NON_EXISTENT_PRODUCT_CODE";

    // when
    InventoryItem found = inventoryItemRepository.findByProductProductCode(nonExistentProductCode);

    // then
    assertNull(found);
  }
}
