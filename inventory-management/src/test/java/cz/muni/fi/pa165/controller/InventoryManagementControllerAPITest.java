package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.exceptions.GrapeAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.GrapeDoesNotExistException;
import cz.muni.fi.pa165.exceptions.ProductAlreadyExistsException;
import cz.muni.fi.pa165.exceptions.ProductNotFoundException;
import cz.muni.fi.pa165.facade.InventoryFacade;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.IngredientDTO;
import cz.muni.fi.pa165.model.dto.ProductDTO;
import cz.muni.fi.pa165.model.dto.TransactionDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@WithMockUser(authorities = {"SCOPE_test_read", "SCOPE_test_write"})
class InventoryManagementControllerAPITest {

    @Mock
    private InventoryFacade inventoryFacade;

    @InjectMocks
    private InventoryManagementController inventoryManagementController;

    private final ProductDTO testProduct = new ProductDTO("test", "test", 2, "code");
    private final GrapeDTO testGrape = new GrapeDTO();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getWineListReturnsOkWhenWinesExist() {
        // arrange
        List<ProductDTO> wines = List.of(testProduct);
        when(inventoryFacade.getWineList(0, 10)).thenReturn(wines);

        // act
        ResponseEntity<List<ProductDTO>> result = inventoryManagementController.getWineList(0, 10);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(wines, result.getBody());
    }

    @Test
    void getWineListReturnsOkWhenNoWinesExist() {
        // arrange
        when(inventoryFacade.getWineList(0, 10)).thenReturn(List.of());

        // act
        ResponseEntity<List<ProductDTO>> result = inventoryManagementController.getWineList(0, 10);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertTrue(result.getBody().isEmpty());
    }

    @Test
    void getWineByIdReturnsOkWhenWineExists() {
        // arrange
        when(inventoryFacade.getWineById(1L)).thenReturn(testProduct);

        // act
        ResponseEntity<ProductDTO> result = inventoryManagementController.getWineById(1L);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(testProduct, result.getBody());
    }

    @Test
    void getWineByIdReturnsNotFoundWhenWineDoesNotExist() {
        // arrange
        when(inventoryFacade.getWineById(1L)).thenReturn(null);

        // act
        ResponseEntity<ProductDTO> result = inventoryManagementController.getWineById(1L);

        // assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    void getWineByCodeReturnsOkWhenWineExists() {
        when(inventoryFacade.getWineByCode("code")).thenReturn(testProduct);

        ResponseEntity<ProductDTO> result = inventoryManagementController.getWineByCode("code");

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(testProduct, result.getBody());
    }

    @Test
    void getWineByCodeReturnsNotFoundWhenWineDoesNotExist() {
        when(inventoryFacade.getWineByCode("code")).thenReturn(null);

        ResponseEntity<ProductDTO> result = inventoryManagementController.getWineByCode("code");

        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    void addWineReturnsCreatedWhenSuccessful() throws ProductAlreadyExistsException {
        // arrange
        doNothing().when(inventoryFacade).addWine(testProduct);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.addWine(testProduct);

        // assert
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    void addWineReturnsConflictWhenProductAlreadyExists() throws ProductAlreadyExistsException {
        // arrange
        doThrow(ProductAlreadyExistsException.class).when(inventoryFacade).addWine(testProduct);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.addWine(testProduct);

        // assert
        assertEquals(HttpStatus.CONFLICT, result.getStatusCode());
    }

    @Test
    void restockWineReturnsOkWhenSuccessful() throws ProductNotFoundException {
        // arrange
        doNothing().when(inventoryFacade).restockWine(testProduct);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.restockWine(testProduct);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void restockWineReturnsNotFoundWhenProductNotFound() throws ProductNotFoundException {
        // arrange
        doThrow(ProductNotFoundException.class).when(inventoryFacade).restockWine(testProduct);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.restockWine(testProduct);

        // assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    void createTransactionReturnsCreatedWhenSuccessful() {
        // arrange
        TransactionDTO testTransaction = new TransactionDTO();
        doNothing().when(inventoryFacade).createTransaction(testTransaction);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.createTransaction(testTransaction);

        // assert
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    void createTransactionThrowsExceptionWhenInvalidInput() {
        // arrange
        TransactionDTO testTransaction = new TransactionDTO();
        doThrow(IllegalArgumentException.class).when(inventoryFacade).createTransaction(testTransaction);

        // act & assert
        assertThrows(IllegalArgumentException.class, () -> inventoryManagementController.createTransaction(testTransaction));
    }

    @Test
    void getGrapeListReturnsOkWhenGrapesExist() {
        // arrange
        List<GrapeDTO> grapes = List.of(testGrape);
        when(inventoryFacade.getGrapeList(0, 10)).thenReturn(grapes);

        // act
        ResponseEntity<List<GrapeDTO>> result = inventoryManagementController.getGrapeList(0, 10);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(grapes, result.getBody());
    }

    @Test
    void getGrapeListReturnsOkWhenNoGrapesExist() {
        // arrange
        when(inventoryFacade.getGrapeList(0, 10)).thenReturn(List.of());

        // act
        ResponseEntity<List<GrapeDTO>> result = inventoryManagementController.getGrapeList(0, 10);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertTrue(result.getBody().isEmpty());
    }
    @Test
    void createGrapeReturnsCreatedWhenSuccessful() throws GrapeAlreadyExistsException {
        // arrange
        doNothing().when(inventoryFacade).createGrape(testGrape);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.createGrape(testGrape);

        // assert
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    void createGrapeReturnsConflictWhenGrapeAlreadyExists() throws GrapeAlreadyExistsException {
        // arrange
        doThrow(GrapeAlreadyExistsException.class).when(inventoryFacade).createGrape(testGrape);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.createGrape(testGrape);

        // assert
        assertEquals(HttpStatus.CONFLICT, result.getStatusCode());
    }

    @Test
    void restockGrapeReturnsOkWhenSuccessful() throws GrapeDoesNotExistException {
        // arrange
        doNothing().when(inventoryFacade).restockGrape(testGrape);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.restockGrape(testGrape);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void restockGrapeReturnsNotFoundWhenGrapeDoesNotExist() throws GrapeDoesNotExistException {
        // arrange
        doThrow(GrapeDoesNotExistException.class).when(inventoryFacade).restockGrape(testGrape);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.restockGrape(testGrape);

        // assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    void getIngredientListReturnsOkWhenIngredientsExist() {
        // arrange
        List<IngredientDTO> ingredients = List.of(new IngredientDTO());
        when(inventoryFacade.getIngredientList(0, 10)).thenReturn(ingredients);

        // act
        ResponseEntity<List<IngredientDTO>> result = inventoryManagementController.getIngredientList(0, 10);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(ingredients, result.getBody());
    }

    @Test
    void getIngredientListReturnsOkWhenNoIngredientsExist() {
        // arrange
        when(inventoryFacade.getIngredientList(0, 10)).thenReturn(List.of());

        // act
        ResponseEntity<List<IngredientDTO>> result = inventoryManagementController.getIngredientList(0, 10);

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertTrue(result.getBody().isEmpty());
    }

    @Test
    void getIngredientByNameReturnsOkWhenIngredientExists() {
        // arrange
        IngredientDTO testIngredient = new IngredientDTO();
        when(inventoryFacade.getIngredientByName("name")).thenReturn(testIngredient);

        // act
        ResponseEntity<IngredientDTO> result = inventoryManagementController.getIngredientByName("name");

        // assert
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(testIngredient, result.getBody());
    }

    @Test
    void getIngredientByNameReturnsNotFoundWhenIngredientDoesNotExist() {
        // arrange
        when(inventoryFacade.getIngredientByName("name")).thenReturn(null);

        // act
        ResponseEntity<IngredientDTO> result = inventoryManagementController.getIngredientByName("name");

        // assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }

    @Test
    void restockIngredientReturnsCreatedWhenSuccessful() {
        // arrange
        IngredientDTO testIngredient = new IngredientDTO();
        doNothing().when(inventoryFacade).restockIngredient(testIngredient);

        // act
        ResponseEntity<HttpStatus> result = inventoryManagementController.restockIngredient(testIngredient);

        // assert
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    void restockIngredientThrowsExceptionWhenInvalidInput() {
        // arrange
        IngredientDTO testIngredient = new IngredientDTO();
        doThrow(IllegalArgumentException.class).when(inventoryFacade).restockIngredient(testIngredient);

        // act & assert
        assertThrows(IllegalArgumentException.class, () -> inventoryManagementController.restockIngredient(testIngredient));
    }
}
