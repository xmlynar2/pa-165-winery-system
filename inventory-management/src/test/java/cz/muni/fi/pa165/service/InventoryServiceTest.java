package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.exceptions.ProductNotFoundException;
import cz.muni.fi.pa165.model.Enums;
import cz.muni.fi.pa165.model.InventoryItem;
import cz.muni.fi.pa165.model.Product;
import cz.muni.fi.pa165.model.Transaction;
import cz.muni.fi.pa165.repository.InventoryItemRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InventoryServiceTest {

    @Mock
    private InventoryItemRepository inventoryRepository;

    @Mock
    private ProductService productService;

    @InjectMocks
    private InventoryService inventoryService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRestockWine_Success() throws ProductNotFoundException {
        String productCode = "WINE001";
        int quantity = 10;
        Product product = new Product();
        product.setProductCode(productCode);

        InventoryItem inventoryItem = new InventoryItem(product, 5);

        when(productService.getProductByCode(productCode)).thenReturn(product);
        when(inventoryRepository.findByProductProductCode(productCode)).thenReturn(inventoryItem);

        inventoryService.restockWine(productCode, quantity);

        verify(inventoryRepository, times(1)).findByProductProductCode(productCode);
        verify(inventoryRepository, times(1)).save(inventoryItem);
        assertEquals(15, inventoryItem.getQuantity()); // 5 (initial quantity) + 10 (restocked quantity)
    }

    @Test
    public void testCommitTransaction_Sale() {
        String productCode = "WINE001";
        int initialQuantity = 10;
        int saleQuantity = 5;
        Product product = new Product();
        product.setProductCode(productCode);

        InventoryItem inventoryItem = new InventoryItem(product, initialQuantity);

        Transaction saleTransaction = new Transaction();
        saleTransaction.setProduct(product);
        saleTransaction.setTransactionType(Enums.TransactionType.Sale);
        saleTransaction.setQuantity(saleQuantity);

        when(inventoryRepository.findByProductProductCode(productCode)).thenReturn(inventoryItem);

        inventoryService.commitTransaction(saleTransaction);

        verify(inventoryRepository, times(1)).findByProductProductCode(productCode);
        verify(inventoryRepository, times(1)).save(inventoryItem);
        assertEquals(initialQuantity - saleQuantity, inventoryItem.getQuantity()); // 10 (initial quantity) - 5 (sale quantity)
    }
}