package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.Customer;
import cz.muni.fi.pa165.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CustomerServiceTest {
    @Mock
    private CustomerRepository customerRepository;
    @InjectMocks
    private CustomerService customerService;

    private final Customer testCustomer = new Customer();

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getCustomerByExternalId() {
        when(customerRepository.findByExternalId(1L)).thenReturn(testCustomer);

        Customer result = customerService.getCustomerByExternalId(1L);

        assertEquals(testCustomer, result);
    }
}