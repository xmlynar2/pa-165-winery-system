package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.Enums;
import cz.muni.fi.pa165.model.Transaction;
import cz.muni.fi.pa165.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class TransactionServiceTest {

    @Mock
    private InventoryService inventoryService;

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionService transactionService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateTransaction() {
        Transaction transaction = new Transaction();
        transaction.setTransactionType(Enums.TransactionType.Purchase);
        transaction.setQuantity(10);

        transactionService.createTransaction(transaction);

        assertNotNull(transaction.getTimestamp());
        verify(transactionRepository, times(1)).save(transaction);
        verify(inventoryService, times(1)).commitTransaction(transaction);
    }
}