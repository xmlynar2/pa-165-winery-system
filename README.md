# PA165 - Winery System

### Running the project:
Project contains 4 main runnable modules and one module, which contains helper functionality.

Runnable modules are in `harvest-management`, `inventory-management`, `wine-production` and `customer-service` folders.

* **Harvest-management** (port 8081): functionality regarding grape harvest.
* **Inventory-management** (port 8082): contains functionality regarding bottles and the number of things in stock(inventory).
* **Wine-production** (port 8083): contains functionality regarding wine production.
* **Customer-service** (port 8084): contains functionality regarding customer and purchases with feedbacks.


#### Docker:

Make sure you have `Docker` and `Docker Compose` installed.

(Note: do this at your own risk, as it will most likely burn your computer.)
* Run `docker-compose up` (or `docker-compose up --build` if you want to rebuild) in the root directory of the project.
* Change credentials of databases in the `.env` file in the root directory of the project.
* Change credentials in the `external-app.yml` files in each service to the credentials in the `.env` file.


#### Locally:
* Run `mvn clean install` in the root directory of the project.
* Run `mvn spring-boot:run` in the directory of the module (harvest-management, wine-production, inventory-management, customer-service folders)


### Team members:

- Bc. Eduard Štefan Mlynárik
- Bc. Patrik Paľovčík
- Bc. Martin Baron
- Bc. Marek Žákovic

### Project description:
Create an information system that will allow a person to manage its own winery in a better way.  The main user role can manage the yearly wine harvest, annotating the quality, quantity of different types of grapes. Then he will manage the wines that can be produced depending on the type of grapes available and other ingredients. The user can also manage the list of wine bottles sold and stocked and available to buyers. Buyers can consult the system to buy quantities of wine bottles, also leaving feedback about the quality of the wine they have bought.

## Database:
The database is started with docker-compose.
To correctly run the database you need to create `.env` file in the root directory.
The `.env` file should contain properties as described in the `.env.example` file.

### How to run seed
(Note: can be run in linux systems or in wsl, databases need to be started via docker-compose)
Go to main project directory.
Firstly it is advised to clean all tables. 
* Firstly, we advise to clean all data `bash database-management/clean-all.sh`
* After you can run seed by `bash database-management/seed-all.sh`

### Security
To access security you have to access localhost:8080 and login via google or muni.
Afterward you gain a bearer token, which you can use to access other services.

### How to run you scenario
(Note: scenario is will be enhanced and improved in M4)
Go to main project directory and run`python scenario.py`
Scenario will run correctly only in linux systems or in wsl, databases and services need to be started via docker-compose.
Before running the scenario, you need to change the token in the scenario.py file to your token you got from the security server.
In the scenario, you can see how the services work together and how the basic usage of the system might look like.


### Metrics and monitoring
Metrics and monitoring are available on grafana dashboard.
The dashboard is available via `http://localhost:3000/` with credentials `admin:admin`.
Grafana is started with docker-compose and is connected to prometheus and all services.


### Diagrams:
#### Use case:
![Use case diagram](diagrams/png/UseCaseDiagram.png?raw=true)

#### Class diagrams:
![Alt text](diagrams/png/classDiagrams/CustomerServiceDTOs.png?raw=true "Customer Service")
![Alt text](diagrams/png/classDiagrams/HarvestServiceDTOs.png?raw=true "Harvest Service")
![Alt text](diagrams/png/classDiagrams/InventoryManagementDTOs.png?raw=true "Inventory Service")
![Alt text](diagrams/png/classDiagrams/WineProductionDTOs.png?raw=true "Wine Service")



