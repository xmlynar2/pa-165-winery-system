package cz.muni.fi.pa165.client;

import cz.muni.fi.pa165.model.dto.GrapeSimpleDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(name = "InventoryManagementClient", url = "${inventory-management.url}")
public interface InventoryManagementClient {
    @PostMapping("/grape")
    void createGrape(@RequestBody GrapeSimpleDTO grape);

    @PostMapping("/grape/restock")
    void updateGrapeStock(@RequestBody GrapeSimpleDTO grape);
}
