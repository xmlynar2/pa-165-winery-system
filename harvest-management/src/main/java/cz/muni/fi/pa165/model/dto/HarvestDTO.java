package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.Quality;
import cz.muni.fi.pa165.model.dao.GrapeDAO;

import java.time.LocalDate;

/**
 * @author Patrik Palovcik
 */
public class HarvestDTO {
    private LocalDate date;
    private double amountInKg;
    private GrapeDAO grapeType;
    private Quality quality;

    public HarvestDTO(LocalDate date, double amountInKg, GrapeDAO grapeType, Quality quality) {
        this.date = date;
        this.amountInKg = amountInKg;
        this.grapeType = grapeType;
        this.quality = quality;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getAmountInKg() {
        return amountInKg;
    }

    public void setAmountInKg(double amountInKg) {
        this.amountInKg = amountInKg;
    }

    public GrapeDAO getGrapeType() {
        return grapeType;
    }

    public void setGrapeType(GrapeDAO grapeType) {
        this.grapeType = grapeType;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    @Override
    public String toString() {
        return "HarvestDTO(" +
                ", date=" + date +
                ", amount in Kg=" + amountInKg +
                ", grape type=" + grapeType +
                ", quality=" + quality + '\'' +
                ')';
    }
}
