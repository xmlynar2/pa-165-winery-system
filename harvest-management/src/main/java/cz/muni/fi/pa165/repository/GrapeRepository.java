package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.GrapeDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Patrik Palovcik
 */
@Repository
public interface GrapeRepository extends JpaRepository<GrapeDAO, Long> {
    @Query("SELECT g FROM GrapeDAO g WHERE g.code = :code")
    Optional<GrapeDAO> findByCode(String code);
}
