package cz.muni.fi.pa165.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {
    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(x -> x
                        .requestMatchers("/swagger-ui/**", "/swagger-resources/**", "/v3/api-docs/**", "/").permitAll()
                        .requestMatchers("/actuator/**").permitAll()
                        .requestMatchers( "/grape").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/grape/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/grape/**").hasAuthority("SCOPE_test_write")
                        .requestMatchers( "/harvest").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.GET, "/harvest/**").hasAuthority("SCOPE_test_read")
                        .requestMatchers(HttpMethod.POST, "/harvest/**").hasAuthority("SCOPE_test_write")
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(Customizer.withDefaults()))
        ;
        return http.build();
    }
}
