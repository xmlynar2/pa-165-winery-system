package cz.muni.fi.pa165.model;

/**
 * @author Patrik Palovcik
 */
public enum Quality {
    EXCELLENT,
    GOOD,
    AVERAGE,
    BAD,
    HORRIBLE
}
