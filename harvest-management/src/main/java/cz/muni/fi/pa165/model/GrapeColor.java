package cz.muni.fi.pa165.model;

/**
 * @author Patrik Palovcik
 */
public enum GrapeColor {
    CRIMSON,
    BLACK,
    DARK_BLUE,
    YELLOW,
    GREEN,
    ORANGE,
    PINK
}
