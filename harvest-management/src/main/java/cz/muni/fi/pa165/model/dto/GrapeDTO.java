package cz.muni.fi.pa165.model.dto;

import cz.muni.fi.pa165.model.GrapeColor;

/**
 * @author Patrik Palovcik
 */
public class GrapeDTO {
    private String code;
    private String name;
    private GrapeColor color;
    private String description;

    public GrapeDTO(String code, String name, GrapeColor color, String description) {
        this.code = code;
        this.name = name;
        this.color = color;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GrapeColor getColor() {
        return color;
    }

    public void setColor(GrapeColor color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GrapeDTO(" +
                ", code=" + code +
                ", name=" + name +
                ", color=" + color +
                ", description=" + description + '\'' +
                ')';
    }
}
