package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.client.InventoryManagementClient;
import cz.muni.fi.pa165.model.dao.HarvestDAO;
import cz.muni.fi.pa165.model.dto.GrapeSimpleDTO;
import cz.muni.fi.pa165.model.dto.HarvestDTO;
import cz.muni.fi.pa165.service.HarvestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrik Palovcik
 */
@Service
public class HarvestFacade {

    private final HarvestService harvestService;
    private final InventoryManagementClient inventoryManagementClient;

    @Autowired
    public HarvestFacade(HarvestService harvestService, InventoryManagementClient inventoryManagementClient) {
        this.harvestService = harvestService;
        this.inventoryManagementClient = inventoryManagementClient;
    }

    public List<HarvestDTO> getAllHarvests(int page, int pageSize) {
        return harvestService.getAllHarvests(page, pageSize).stream()
                .map(this::mapToHarvestDTO)
                .collect(Collectors.toList());
    }

    public List<HarvestDTO> getHarvestsByDate(LocalDate date) {
        return harvestService.getHarvestsByDate(date).stream()
                .map(this::mapToHarvestDTO)
                .collect(Collectors.toList());
    }

    public HarvestDTO getHarvestById(Long id) {
        return mapToHarvestDTO(harvestService.getHarvestById(id));
    }

    public HarvestDTO addHarvest(HarvestDTO harvestDTO) {
        GrapeSimpleDTO grapeCreated = new GrapeSimpleDTO();
        GrapeSimpleDTO grapeStocked = new GrapeSimpleDTO();
        grapeCreated.setCode(harvestDTO.getGrapeType().getCode());
        grapeStocked.setCode(harvestDTO.getGrapeType().getCode());
        grapeCreated.setDescription(harvestDTO.getGrapeType().getDescription());
        grapeStocked.setDescription(null);
        grapeCreated.setName(harvestDTO.getGrapeType().getName());
        grapeStocked.setName(null);
        grapeCreated.setQuantity(0);
        grapeStocked.setQuantity(harvestDTO.getAmountInKg());
        inventoryManagementClient.createGrape(grapeCreated);
        inventoryManagementClient.updateGrapeStock(grapeStocked);
        return mapToHarvestDTO(harvestService.addHarvest(harvestDTO));
    }

    public HarvestDTO updateHarvest(Long id, HarvestDTO updatedHarvestDTO) {
        return mapToHarvestDTO(harvestService.updateHarvest(id, updatedHarvestDTO));
    }

    public HarvestDTO deleteHarvest(Long id) {
        return mapToHarvestDTO(harvestService.deleteHarvest(id));
    }

    public HarvestDTO mapToHarvestDTO(HarvestDAO harvestDAO) {
        return new HarvestDTO(harvestDAO.getDate(), harvestDAO.getAmountInKg(), harvestDAO.getGrapeType(),
                harvestDAO.getQuality());
    }

}
