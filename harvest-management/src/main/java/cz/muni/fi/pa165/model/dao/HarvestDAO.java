package cz.muni.fi.pa165.model.dao;

import cz.muni.fi.pa165.model.Quality;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Patrik Palovcik
 */
@Entity
@Table(name = "harvest")
public class HarvestDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "amount_in_kg")
    private double amountInKg;

    @Column(name = "quality")
    private Quality quality;

    @JoinColumn(name = "grape_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private GrapeDAO grapeType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getAmountInKg() {
        return amountInKg;
    }

    public void setAmountInKg(double amountInKg) {
        this.amountInKg = amountInKg;
    }

    public GrapeDAO getGrapeType() {
        return grapeType;
    }

    public void setGrapeType(GrapeDAO grapeType) {
        this.grapeType = grapeType;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HarvestDAO that = (HarvestDAO) o;
        return Objects.equals(id, that.id) && Objects.equals(date, that.date) &&
                Objects.equals(amountInKg, that.amountInKg) && Objects.equals(grapeType, that.grapeType)
                && Objects.equals(quality, that.quality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, amountInKg, grapeType, quality);
    }

    @Override
    public String toString() {
        return "HarvestDAO(" +
                "id=" + id +
                ", date=" + date +
                ", amount in Kg=" + amountInKg +
                ", grape type=" + grapeType +
                ", quality=" + quality + '\'' +
                ')';
    }
}

