package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.HarvestDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Patrik Palovcik
 */
@Repository
public interface HarvestRepository extends JpaRepository<HarvestDAO, Long> {
    @Query("SELECT h FROM HarvestDAO h WHERE h.date = :date")
    List<HarvestDAO> getHarvestsByDate(LocalDate date);
}
