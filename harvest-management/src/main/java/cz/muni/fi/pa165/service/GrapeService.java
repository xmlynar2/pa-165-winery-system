package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.repository.GrapeRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class GrapeService {

    private final GrapeRepository grapeRepository;

    @Autowired
    public GrapeService(GrapeRepository grapeRepository) {
        this.grapeRepository = grapeRepository;
    }

    public List<GrapeDAO> getAllGrapes(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return grapeRepository.findAll(pageable).getContent();
    }

    public GrapeDAO getGrapeById(Long id) {
        return grapeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Grape not found with id: " + id));
    }

    public GrapeDAO getGrapeByCode(String code) {
        return grapeRepository.findByCode(code)
                .orElseThrow(() -> new EntityNotFoundException("Grape not found with code: " + code));
    }

    @Transactional
    public GrapeDAO addGrape(GrapeDTO grapeDTO) {
        GrapeDAO grapeDAO = mapToGrapeDAO(grapeDTO);
        grapeRepository.save(grapeDAO);
        return grapeDAO;
    }

    @Transactional
    public GrapeDAO updateGrape(Long id, GrapeDTO updatedGrape) {
        GrapeDAO existingGrape = grapeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Grape not found with id: " + id));
        existingGrape.setCode(updatedGrape.getCode());
        existingGrape.setName(updatedGrape.getName());
        existingGrape.setColor(updatedGrape.getColor());
        existingGrape.setDescription(updatedGrape.getDescription());
        grapeRepository.save(existingGrape);
        return existingGrape;
    }

    @Transactional
    public GrapeDAO deleteGrape(Long id) {
        GrapeDAO deletedGrape = grapeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Grape not found with id: " + id));
        grapeRepository.deleteById(id);
        return deletedGrape;
    }

    private GrapeDAO mapToGrapeDAO(GrapeDTO grapeDTO) {
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setCode(grapeDTO.getCode());
        grapeDAO.setColor(grapeDTO.getColor());
        grapeDAO.setName(grapeDTO.getName());
        grapeDAO.setDescription(grapeDTO.getDescription());
        return grapeDAO;
    }
}