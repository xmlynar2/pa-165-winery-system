package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.service.GrapeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Patrik Palovcik
 */
@Service
public class GrapeFacade {

    private final GrapeService grapeService;

    @Autowired
    public GrapeFacade(GrapeService grapeService) {
        this.grapeService = grapeService;
    }

    public List<GrapeDTO> getAllGrapes(int page, int pageSize) {
        return grapeService.getAllGrapes(page, pageSize).stream()
                .map(this::mapToGrapeDTO)
                .collect(Collectors.toList());
    }

    public GrapeDTO getGrapeById(Long id) {
        return mapToGrapeDTO(grapeService.getGrapeById(id));
    }

    public GrapeDTO getGrapeByCode(String code) {
        return mapToGrapeDTO(grapeService.getGrapeByCode(code));
    }

    public GrapeDTO addGrape(GrapeDTO grape) {
        return mapToGrapeDTO(grapeService.addGrape(grape));
    }

    public GrapeDTO updateGrape(Long id, GrapeDTO updatedGrape) {
        return mapToGrapeDTO(grapeService.updateGrape(id, updatedGrape));
    }

    public GrapeDTO deleteGrape(Long id) {
        return mapToGrapeDTO(grapeService.deleteGrape(id));
    }

    private GrapeDTO mapToGrapeDTO(GrapeDAO grapeDAO) {
        return new GrapeDTO(grapeDAO.getCode(), grapeDAO.getName(), grapeDAO.getColor(),
                grapeDAO.getDescription());
    }

}
