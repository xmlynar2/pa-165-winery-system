package cz.muni.fi.pa165.model.dao;

import cz.muni.fi.pa165.model.GrapeColor;
import jakarta.persistence.*;

import java.util.Objects;

/**
 * @author Patrik Palovcik
 */

@Entity
@Table(name = "grape")
public class GrapeDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "color")
    private GrapeColor color;

    @Column(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GrapeColor getColor() {
        return color;
    }

    public void setColor(GrapeColor color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrapeDAO that = (GrapeDAO) o;
        return Objects.equals(code, that.code) && Objects.equals(id, that.id) && Objects.equals(name, that.name) &&
                Objects.equals(color, that.color)
                && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, id, name, color, description);
    }

    @Override
    public String toString() {
        return "GrapeDAO(" +
                "id=" + id +
                "code=" + code +
                ", name=" + name +
                ", color=" + color +
                ", description=" + description + '\'' +
                ')';
    }
}
