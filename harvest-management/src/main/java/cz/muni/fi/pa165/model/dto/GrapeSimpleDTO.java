package cz.muni.fi.pa165.model.dto;

/**
 * @author Patrik Palovcik
 */
public class GrapeSimpleDTO {
    private String code;
    private double quantity;
    private String name;
    private String description;

    public GrapeSimpleDTO(String code, double quantity, String name, String description) {
        this.code = code;
        this.quantity = quantity;
        this.name = name;
        this.description = description;
    }

    public GrapeSimpleDTO() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GrapeDTO(" +
                ", code=" + code +
                ", quantity=" + quantity +
                ", name=" + name +
                ", description=" + description + '\'' +
                ')';
    }
}
