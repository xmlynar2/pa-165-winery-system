package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.GrapeFacade;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Grape API", description = "Provides API for manipulating grapes.")
@RestController
@RequestMapping("/grape")
public class GrapeController {
    private final GrapeFacade grapeFacade;

    @Autowired
    public GrapeController(GrapeFacade grapeFacade) {
        this.grapeFacade = grapeFacade;
    }

    @Operation(summary = "Get all grapes", description = "Returns a list of all grapes.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found all grapes",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = List.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid input")

    })
    @GetMapping("/list")
    public List<GrapeDTO> getAllGrapes(@RequestParam(name = "page", defaultValue = "0") int page,
                                       @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return grapeFacade.getAllGrapes(page, pageSize);
    }

    @Operation(summary = "Get grape by ID", description = "Returns detail of grape by ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the grape",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GrapeDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Grape not found")
    })
    @GetMapping("/{id}")
    public GrapeDTO getGrapeById(
            @Parameter(description = "Grape ID") @PathVariable Long id) {
        return grapeFacade.getGrapeById(id);
    }

    @Operation(summary = "Get grape by code", description = "Returns detail of grape by code.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the grape",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GrapeDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Grape not found")
    })
    @GetMapping("/code/{code}")
    public GrapeDTO getGrapeByCode(
            @Parameter(description = "Grape Code") @PathVariable String code) {
        return grapeFacade.getGrapeByCode(code);
    }

    @Operation(summary = "Creates grape")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created the grape",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GrapeDTO.class))}),
            @ApiResponse(responseCode = "409", description = "Harvest already exists")
    })
    @PostMapping
    public GrapeDTO addGrape(
            @Parameter(description = "Grape details") @RequestBody GrapeDTO grape) {
        return grapeFacade.addGrape(grape);
    }

    @Operation(summary = "Update grape", description = "Updates an existing grape.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the grape",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GrapeDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Grape not found")
    })
    @PutMapping("/{id}")
    public GrapeDTO updateGrape(
            @Parameter(description = "Grape ID") @PathVariable Long id,
            @Parameter(description = "Updated grape details") @RequestBody GrapeDTO updatedGrape) {
        return grapeFacade.updateGrape(id, updatedGrape);
    }

    @Operation(summary = "Delete grape", description = "Deletes an existing grape.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the grape",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GrapeDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Grape not found")
    })
    @DeleteMapping("/{id}")
    public GrapeDTO deleteGrape(
            @Parameter(description = "Grape ID") @PathVariable Long id) {
        return grapeFacade.deleteGrape(id);
    }
}
