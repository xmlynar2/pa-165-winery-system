package cz.muni.fi.pa165;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class HarvestApplication {
    public static void main(String[] args) {
        SpringApplication.run(HarvestApplication.class, args);
    }
}

