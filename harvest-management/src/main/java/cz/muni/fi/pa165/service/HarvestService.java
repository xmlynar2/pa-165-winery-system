package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dao.HarvestDAO;
import cz.muni.fi.pa165.model.dto.HarvestDTO;
import cz.muni.fi.pa165.repository.GrapeRepository;
import cz.muni.fi.pa165.repository.HarvestRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class HarvestService {

    private final HarvestRepository harvestRepository;
    private final GrapeRepository grapeRepository;

    @Autowired
    public HarvestService(HarvestRepository harvestRepository, GrapeRepository grapeRepository) {
        this.harvestRepository = harvestRepository;
        this.grapeRepository = grapeRepository;
    }

    public List<HarvestDAO> getAllHarvests(int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return harvestRepository.findAll(pageable).getContent();
    }

    public HarvestDAO getHarvestById(Long id) {
        return harvestRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Harvest not found with id: " + id));
    }

    public List<HarvestDAO> getHarvestsByDate(LocalDate date) {
        return harvestRepository.getHarvestsByDate(date);
    }

    @Transactional
    public HarvestDAO addHarvest(HarvestDTO harvestDTO) {
        HarvestDAO harvestDAO = mapToHarvestDAO(harvestDTO);
        GrapeDAO grapeCode = grapeRepository.findByCode(harvestDTO.getGrapeType().getCode()).orElseThrow();
        harvestDAO.setGrapeType(grapeCode);
        harvestRepository.save(harvestDAO);
        return harvestDAO;
    }

    @Transactional
    public HarvestDAO updateHarvest(Long id, HarvestDTO updatedHarvestDTO) {
        HarvestDAO existingHarvest = harvestRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Harvest not found with id: " + id));
        existingHarvest.setDate(updatedHarvestDTO.getDate());
        existingHarvest.setAmountInKg(updatedHarvestDTO.getAmountInKg());
        existingHarvest.setGrapeType(updatedHarvestDTO.getGrapeType());
        existingHarvest.setQuality(updatedHarvestDTO.getQuality());
        harvestRepository.save(existingHarvest);
        return existingHarvest;
    }

    @Transactional
    public HarvestDAO deleteHarvest(Long id) {
        HarvestDAO deletedHarvest = harvestRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Harvest not found with id: " + id));
        harvestRepository.deleteById(id);
        return deletedHarvest;
    }

    private HarvestDAO mapToHarvestDAO(HarvestDTO harvestDTO) {
        HarvestDAO harvestDAO = new HarvestDAO();
        harvestDAO.setQuality(harvestDTO.getQuality());
        harvestDAO.setDate(harvestDTO.getDate());
        harvestDAO.setGrapeType(harvestDTO.getGrapeType());
        harvestDAO.setAmountInKg(harvestDTO.getAmountInKg());
        return harvestDAO;
    }
}