package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.HarvestFacade;
import cz.muni.fi.pa165.model.dto.HarvestDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Tag(name = "Harvest API", description = "Provides API for manipulating harvests.")
@RestController
@RequestMapping("/harvest")
public class HarvestController {

    private final HarvestFacade harvestFacade;

    @Autowired
    public HarvestController(HarvestFacade harvestFacade) {
        this.harvestFacade = harvestFacade;
    }

    @Operation(summary = "Get all harvests", description = "Returns a list of all harvests.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the harvests",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = List.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    @GetMapping("/list")
    public List<HarvestDTO> getAllHarvests(@RequestParam(name = "page", defaultValue = "0") int page,
                                           @RequestParam(name = "pageSize", defaultValue = "10") int pageSize) {
        return harvestFacade.getAllHarvests(page, pageSize);
    }

    @Operation(summary = "Get all harvests by date", description = "Returns a list of all harvests by date.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the list of harvests"),
            @ApiResponse(responseCode = "400", description = "Invalid date format")
    })
    @GetMapping("/date/{date}")
    public List<HarvestDTO> getHarvestsByDate(@PathVariable String date) {
        LocalDate localDate = LocalDate.parse(date);
        return harvestFacade.getHarvestsByDate(localDate);
    }

    @Operation(summary = "Get harvest by ID", description = "Returns detail of harvest by ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the harvest",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HarvestDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Harvest not found")
    })
    @GetMapping("/{id}")
    public HarvestDTO getHarvestById(
            @Parameter(description = "Harvest ID") @PathVariable Long id) {
        return harvestFacade.getHarvestById(id);
    }

    @Operation(summary = "Add harvest", description = "Creates a new harvest.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Harvest created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HarvestDTO.class))}),
            @ApiResponse(responseCode = "409", description = "Harvest already exists")
    })
    @PostMapping
    public HarvestDTO addHarvest(
            @Parameter(description = "Harvest details") @RequestBody HarvestDTO harvest) {
        return harvestFacade.addHarvest(harvest);
    }

    @Operation(summary = "Update harvest", description = "Updates an existing harvest.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the harvest",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HarvestDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Harvest not found")
    })
    @PutMapping("/{id}")
    public HarvestDTO updateHarvest(
            @Parameter(description = "Harvest ID") @PathVariable Long id,
            @Parameter(description = "Updated harvest details") @RequestBody HarvestDTO updatedHarvest) {
        return harvestFacade.updateHarvest(id, updatedHarvest);
    }

    @Operation(summary = "Delete harvest", description = "Deletes an existing harvest.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the harvest",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = HarvestDTO.class))}),
            @ApiResponse(responseCode = "404", description = "Harvest not found")
    })
    @DeleteMapping("/{id}")
    public HarvestDTO deleteHarvest(
            @Parameter(description = "Harvest ID") @PathVariable Long id) {
        return harvestFacade.deleteHarvest(id);
    }
}
