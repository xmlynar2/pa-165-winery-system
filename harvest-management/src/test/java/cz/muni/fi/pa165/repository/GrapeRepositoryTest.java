package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.GrapeDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GrapeRepositoryTest {
    @Mock
    private GrapeRepository grapeRepository;

    @Test
    public void testFindByCode() {
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setCode("code");

        when(grapeRepository.findByCode("code")).thenReturn(Optional.of(grapeDAO));

        Optional<GrapeDAO> result = grapeRepository.findByCode("code");

        assertEquals(grapeDAO, result.orElse(null));
    }
}