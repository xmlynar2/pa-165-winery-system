package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.HarvestFacade;
import cz.muni.fi.pa165.model.Quality;
import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dto.HarvestDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HarvestControllerTest {

    @Mock
    private HarvestFacade harvestFacade;

    @InjectMocks
    private HarvestController harvestController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsHarvestWhenPresent() {
        HarvestDTO harvestDTO = new HarvestDTO(LocalDate.of(2020, 1, 8), 1, new GrapeDAO(), Quality.AVERAGE);
        when(harvestFacade.getHarvestById(1L)).thenReturn(harvestDTO);

        HarvestDTO result = harvestController.getHarvestById(1L);

        assertEquals(harvestDTO, result);
    }

    @Test
    void getByIdReturnsNullWhenAbsent() {
        when(harvestFacade.getHarvestById(1L)).thenReturn(null);

        HarvestDTO result = harvestController.getHarvestById(1L);

        assertEquals(null, result);
    }

    @Test
    public void testGetHarvestsByDate() {
        String date = "2022-01-01";
        LocalDate localDate = LocalDate.parse(date);

        HarvestDTO harvestDTO1 = new HarvestDTO(localDate, 100.0, null, null);
        HarvestDTO harvestDTO2 = new HarvestDTO(localDate, 150.0, null, null);
        List<HarvestDTO> expectedHarvests = new ArrayList<>();
        expectedHarvests.add(harvestDTO1);
        expectedHarvests.add(harvestDTO2);

        when(harvestFacade.getHarvestsByDate(localDate)).thenReturn(expectedHarvests);

        List<HarvestDTO> result = harvestController.getHarvestsByDate(date);

        assertEquals(expectedHarvests.size(), result.size());
        assertEquals(expectedHarvests, result);

        verify(harvestFacade, Mockito.times(1)).getHarvestsByDate(localDate);
    }

    @Test
    void createReturnsCreatedHarvest() {
        HarvestDTO harvestDTO = new HarvestDTO(LocalDate.of(2020, 1, 8), 1, new GrapeDAO(), Quality.AVERAGE);
        when(harvestFacade.addHarvest(any(HarvestDTO.class))).thenReturn(harvestDTO);

        HarvestDTO result = harvestController.addHarvest(harvestDTO);

        assertEquals(harvestDTO, result);
    }

    @Test
    void updateReturnsUpdatedHarvest() {
        HarvestDTO harvestDTO = new HarvestDTO(LocalDate.of(2020, 1, 8), 1, new GrapeDAO(), Quality.AVERAGE);
        when(harvestFacade.updateHarvest(anyLong(), any(HarvestDTO.class))).thenReturn(harvestDTO);

        HarvestDTO result = harvestController.updateHarvest(1L, harvestDTO);

        assertEquals(harvestDTO, result);
    }

    @Test
    void getAllHarvestsReturnsListOfHarvests() {
        List<HarvestDTO> expectedHarvests = new ArrayList<>();
        expectedHarvests.add(new HarvestDTO(LocalDate.of(2021, 1, 1), 100.0, new GrapeDAO(), Quality.AVERAGE));
        expectedHarvests.add(new HarvestDTO(LocalDate.of(2021, 1, 2), 150.0, new GrapeDAO(), Quality.GOOD));
        when(harvestFacade.getAllHarvests(0, 10)).thenReturn(expectedHarvests);

        List<HarvestDTO> result = harvestController.getAllHarvests(0, 10);

        assertEquals(expectedHarvests, result);
    }

    @Test
    void getAllHarvestsReturnsEmptyListIfNoHarvestsExist() {
        when(harvestFacade.getAllHarvests(0, 10)).thenReturn(Collections.emptyList());

        List<HarvestDTO> result = harvestController.getAllHarvests(0, 10);

        assertTrue(result.isEmpty());
    }

    @Test
    void deleteHarvestReturnsDeletedHarvest() {
        HarvestDTO harvestDTO = new HarvestDTO(LocalDate.of(2021, 1, 1), 100.0, new GrapeDAO(), Quality.AVERAGE);
        when(harvestFacade.deleteHarvest(1L)).thenReturn(harvestDTO);

        HarvestDTO result = harvestController.deleteHarvest(1L);

        assertEquals(harvestDTO, result);
        verify(harvestFacade).deleteHarvest(1L);
    }

    @Test
    void deleteHarvestReturnsNullIfHarvestNotFound() {
        when(harvestFacade.deleteHarvest(1L)).thenReturn(null);

        HarvestDTO result = harvestController.deleteHarvest(1L);

        assertNull(result);
        verify(harvestFacade).deleteHarvest(1L);
    }
}
