package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.Quality;
import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dao.HarvestDAO;
import cz.muni.fi.pa165.model.dto.HarvestDTO;
import cz.muni.fi.pa165.repository.GrapeRepository;
import cz.muni.fi.pa165.repository.HarvestRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class HarvestServiceTest {

    @Mock
    private HarvestRepository harvestRepository;

    @Mock
    private GrapeRepository grapeRepository;

    @InjectMocks
    private HarvestService harvestService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsHarvestWhenPresent() {
        HarvestDAO harvestDAO = new HarvestDAO();
        when(harvestRepository.findById(1L)).thenReturn(Optional.of(harvestDAO));

        HarvestDAO result = harvestService.getHarvestById(1L);

        assertEquals(harvestDAO, result);
    }

    @Test
    void getByIdReturnsNullWhenAbsent() {
        when(harvestRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
            harvestService.getHarvestById(1L);
        });
    }

    @Test
    void getByDateReturnsHarvestsWhenPresent() {
        LocalDate date = LocalDate.of(2022, 1, 1);

        HarvestDAO harvestDAO1 = new HarvestDAO();
        harvestDAO1.setId(1L);

        HarvestDAO harvestDAO2 = new HarvestDAO();
        harvestDAO2.setId(2L);

        List<HarvestDAO> expectedHarvests = new ArrayList<>();
        expectedHarvests.add(harvestDAO1);
        expectedHarvests.add(harvestDAO2);

        when(harvestRepository.getHarvestsByDate(date)).thenReturn(expectedHarvests);

        List<HarvestDAO> result = harvestService.getHarvestsByDate(date);

        assertEquals(expectedHarvests.size(), result.size());
        for (int i = 0; i < expectedHarvests.size(); i++) {
            assertEquals(expectedHarvests.get(i).getId(), result.get(i).getId());
        }
    }

    @Test
    void createReturnsSavedHarvest() {
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setCode("code");
        HarvestDTO harvestDTO = new HarvestDTO(LocalDate.of(2020, 1, 8), 1, grapeDAO, Quality.AVERAGE);
        when(grapeRepository.findByCode(anyString())).thenReturn(Optional.of(grapeDAO));
        when(harvestRepository.save(any(HarvestDAO.class))).thenAnswer(invocation -> {
            HarvestDAO savedHarvestDAO = invocation.getArgument(0);

            return savedHarvestDAO;
        });

        HarvestDAO result = harvestService.addHarvest(harvestDTO);

        assertEquals(harvestDTO.getDate(), result.getDate());
        assertEquals(harvestDTO.getAmountInKg(), result.getAmountInKg());
        assertEquals(harvestDTO.getQuality(), result.getQuality());
    }

    @Test
    void updateReturnsNull() {
        when(harvestRepository.findById(1L)).thenReturn(Optional.empty());

        HarvestDTO harvestDTO = new HarvestDTO(LocalDate.of(2020, 1, 8), 1, new GrapeDAO(), Quality.AVERAGE);
        assertThrows(EntityNotFoundException.class, () -> {
            harvestService.updateHarvest(1L, harvestDTO);
        });
    }

    @Test
    void deleteReturnsDeletedHarvest() {
        HarvestDAO harvestDAO = new HarvestDAO();
        when(harvestRepository.findById(1L)).thenReturn(Optional.of(harvestDAO));

        HarvestDAO result = harvestService.deleteHarvest(1L);

        assertEquals(harvestDAO, result);
        verify(harvestRepository, times(1)).deleteById(1L);
    }
}
