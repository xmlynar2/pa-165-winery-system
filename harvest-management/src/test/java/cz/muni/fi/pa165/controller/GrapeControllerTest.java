package cz.muni.fi.pa165.controller;

import cz.muni.fi.pa165.facade.GrapeFacade;
import cz.muni.fi.pa165.model.GrapeColor;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GrapeControllerTest {

    @Mock
    private GrapeFacade grapeFacade;

    @InjectMocks
    private GrapeController grapeController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsGrapeWhenPresent() {
        GrapeDTO grapeDTO = new GrapeDTO("code", "name", GrapeColor.BLACK, "description");
        when(grapeFacade.getGrapeById(1L)).thenReturn(grapeDTO);

        GrapeDTO result = grapeController.getGrapeById(1L);

        assertEquals(grapeDTO, result);
    }

    @Test
    void getByIdReturnsNullWhenAbsent() {
        when(grapeFacade.getGrapeById(1L)).thenReturn(null);

        GrapeDTO result = grapeController.getGrapeById(1L);

        assertEquals(null, result);
    }

    @Test
    public void testGetGrapeByCode() {
        String code = "sampleCode";

        GrapeDTO sampleGrapeDTO = new GrapeDTO(code, "Sample Name", GrapeColor.BLACK, "Sample Description");
        sampleGrapeDTO.setCode(code);
        sampleGrapeDTO.setName("Sample Name");

        when(grapeFacade.getGrapeByCode(code)).thenReturn(sampleGrapeDTO);

        GrapeDTO result = grapeController.getGrapeByCode(code);

        assertEquals(sampleGrapeDTO, result);
    }

    @Test
    void createReturnsCreatedGrape() {
        GrapeDTO grapeDTO = new GrapeDTO("code", "name", GrapeColor.BLACK, "description");
        when(grapeFacade.addGrape(any(GrapeDTO.class))).thenReturn(grapeDTO);

        GrapeDTO result = grapeController.addGrape(grapeDTO);

        assertEquals(grapeDTO, result);
    }

    @Test
    void updateReturnsUpdatedIngredient() {
        GrapeDTO grapeDTO = new GrapeDTO("code", "name", GrapeColor.BLACK, "description");
        when(grapeFacade.updateGrape(anyLong(), any(GrapeDTO.class))).thenReturn(grapeDTO);

        GrapeDTO result = grapeController.updateGrape(1L, grapeDTO);

        assertEquals(grapeDTO, result);
    }

    @Test
    void getAllGrapesReturnsListOfGrapes() {
        List<GrapeDTO> expectedGrapes = new ArrayList<>();
        expectedGrapes.add(new GrapeDTO("code1", "name1", GrapeColor.BLACK, "description1"));
        expectedGrapes.add(new GrapeDTO("code2", "name2", GrapeColor.DARK_BLUE, "description2"));
        when(grapeFacade.getAllGrapes(0, 10)).thenReturn(expectedGrapes);

        List<GrapeDTO> result = grapeController.getAllGrapes(0, 10);

        assertEquals(expectedGrapes, result);
    }

    @Test
    void getAllGrapesReturnsEmptyListIfNoGrapesExist() {
        when(grapeFacade.getAllGrapes(0, 10)).thenReturn(Collections.emptyList());

        List<GrapeDTO> result = grapeController.getAllGrapes(0, 10);

        assertTrue(result.isEmpty());
    }

    @Test
    void deleteGrapeReturnsDeletedGrape() {
        GrapeDTO grapeDTO = new GrapeDTO("code", "name", GrapeColor.BLACK, "description");
        when(grapeFacade.deleteGrape(1L)).thenReturn(grapeDTO);

        GrapeDTO result = grapeController.deleteGrape(1L);

        assertEquals(grapeDTO, result);
        verify(grapeFacade).deleteGrape(1L);
    }

    @Test
    void deleteGrapeReturnsNullIfGrapeNotFound() {
        when(grapeFacade.deleteGrape(1L)).thenReturn(null);

        GrapeDTO result = grapeController.deleteGrape(1L);

        assertNull(result);
        verify(grapeFacade).deleteGrape(1L);
    }
}
