package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.model.GrapeColor;
import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.service.GrapeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;


public class GrapeFacadeTest {

    @Mock
    private GrapeService grapeService;

    @InjectMocks
    private GrapeFacade grapeFacade;

    private GrapeDAO grapeDAO;
    private GrapeDTO grapeDTO;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        grapeDAO = new GrapeDAO();
        grapeDAO.setName("Grape");
        grapeDAO.setColor(GrapeColor.BLACK);
        grapeDAO.setCode("code");
        grapeDAO.setDescription("description");

        grapeDTO = new GrapeDTO("code", "Grape", GrapeColor.BLACK, "description");
    }

    @Test
    public void getByIdReturnsCorrectGrape() {
        when(grapeService.getGrapeById(1L)).thenReturn(grapeDAO);

        GrapeDTO result = grapeFacade.getGrapeById(1L);

        assertEquals(grapeDTO.getCode(), result.getCode());
        assertEquals(grapeDTO.getName(), result.getName());
        assertEquals(grapeDTO.getColor(), result.getColor());
        assertEquals(grapeDTO.getDescription(), result.getDescription());
        verify(grapeService, times(1)).getGrapeById(1L);
    }

    @Test
    public void getByCodeReturnsCorrectGrape() {
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setCode("code");
        grapeDAO.setName("name");
        grapeDAO.setColor(GrapeColor.BLACK);
        grapeDAO.setDescription("description");

        when(grapeService.getGrapeByCode("code")).thenReturn(grapeDAO);

        GrapeDTO result = grapeFacade.getGrapeByCode("code");

        assertEquals(grapeDAO.getCode(), result.getCode());
        assertEquals(grapeDAO.getName(), result.getName());
        assertEquals(grapeDAO.getColor(), result.getColor());
        assertEquals(grapeDAO.getDescription(), result.getDescription());

        verify(grapeService, times(1)).getGrapeByCode("code");
    }

    @Test
    public void createReturnsCorrectGrape() {
        when(grapeService.addGrape(grapeDTO)).thenReturn(grapeDAO);

        GrapeDTO result = grapeFacade.addGrape(grapeDTO);

        assertEquals(grapeDTO.getCode(), result.getCode());
        assertEquals(grapeDTO.getName(), result.getName());
        assertEquals(grapeDTO.getColor(), result.getColor());
        assertEquals(grapeDTO.getDescription(), result.getDescription());
        verify(grapeService, times(1)).addGrape(grapeDTO);
    }

    @Test
    public void getAllReturnsCorrectGrapes() {
        GrapeDAO grapeDAO1 = createMockGrapeDAO("code1", "Grape 1", GrapeColor.GREEN, "Description 1");
        GrapeDAO grapeDAO2 = createMockGrapeDAO("code2", "Grape 2", GrapeColor.BLACK, "Description 2");

        when(grapeService.getAllGrapes(0, 10)).thenReturn(Arrays.asList(grapeDAO1, grapeDAO2));

        List<GrapeDTO> result = grapeFacade.getAllGrapes(0, 10);

        assertEquals(2, result.size());

        GrapeDTO expectedDTO1 = result.get(0);
        assertEquals(grapeDAO1.getCode(), expectedDTO1.getCode());
        assertEquals(grapeDAO1.getName(), expectedDTO1.getName());
        assertEquals(grapeDAO1.getColor(), expectedDTO1.getColor());
        assertEquals(grapeDAO1.getDescription(), expectedDTO1.getDescription());

        GrapeDTO expectedDTO2 = result.get(1);
        assertEquals(grapeDAO2.getCode(), expectedDTO2.getCode());
        assertEquals(grapeDAO2.getName(), expectedDTO2.getName());
        assertEquals(grapeDAO2.getColor(), expectedDTO2.getColor());
        assertEquals(grapeDAO2.getDescription(), expectedDTO2.getDescription());

        verify(grapeService, times(1)).getAllGrapes(0, 10);
    }

    @Test
    public void updateReturnsCorrectIngredient() {
        when(grapeService.updateGrape(1L, grapeDTO)).thenReturn(grapeDAO);

        GrapeDTO result = grapeFacade.updateGrape(1L, grapeDTO);

        assertEquals(grapeDTO.getCode(), result.getCode());
        assertEquals(grapeDTO.getName(), result.getName());
        assertEquals(grapeDTO.getColor(), result.getColor());
        assertEquals(grapeDTO.getDescription(), result.getDescription());
        verify(grapeService, times(1)).updateGrape(1L, grapeDTO);
    }

    @Test
    void deleteReturnsDeletedGrape() {
        // Arrange
        when(grapeService.deleteGrape(1L)).thenReturn(grapeDAO);

        // Act
        GrapeDTO result = grapeFacade.deleteGrape(1L);

        // Assert
        assertEquals(grapeDTO.getCode(), result.getCode());
        assertEquals(grapeDTO.getName(), result.getName());
        assertEquals(grapeDTO.getColor(), result.getColor());
        assertEquals(grapeDTO.getDescription(), result.getDescription());
        verify(grapeService).deleteGrape(1L);
    }

    private GrapeDAO createMockGrapeDAO(String code, String name, GrapeColor color, String description) {
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setCode(code);
        grapeDAO.setName(name);
        grapeDAO.setColor(color);
        grapeDAO.setDescription(description);
        return grapeDAO;
    }
}
