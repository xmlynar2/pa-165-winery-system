package cz.muni.fi.pa165.repository;

import cz.muni.fi.pa165.model.dao.HarvestDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HarvestRepositoryTest {

    @Mock
    private HarvestRepository harvestRepository;

    @Test
    public void testGetHarvestsByDate() {
        LocalDate date = LocalDate.of(2022, 1, 1);

        HarvestDAO harvestDAO1 = new HarvestDAO();
        harvestDAO1.setId(1L);

        HarvestDAO harvestDAO2 = new HarvestDAO();
        harvestDAO2.setId(2L);

        List<HarvestDAO> expectedHarvests = new ArrayList<>();
        expectedHarvests.add(harvestDAO1);
        expectedHarvests.add(harvestDAO2);

        when(harvestRepository.getHarvestsByDate(date)).thenReturn(expectedHarvests);

        List<HarvestDAO> result = harvestRepository.getHarvestsByDate(date);

        assertEquals(expectedHarvests.size(), result.size());
        for (int i = 0; i < expectedHarvests.size(); i++) {
            assertEquals(expectedHarvests.get(i).getId(), result.get(i).getId());
        }
    }
}
