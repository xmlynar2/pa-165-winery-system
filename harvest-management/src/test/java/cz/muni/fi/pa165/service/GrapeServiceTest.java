package cz.muni.fi.pa165.service;

import cz.muni.fi.pa165.model.GrapeColor;
import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.repository.GrapeRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class GrapeServiceTest {

    @Mock
    private GrapeRepository grapeRepository;

    @InjectMocks
    private GrapeService grapeService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getByIdReturnsIngredientWhenPresent() {
        GrapeDAO grapeDAO = new GrapeDAO();
        when(grapeRepository.findById(1L)).thenReturn(Optional.of(grapeDAO));

        GrapeDAO result = grapeService.getGrapeById(1L);

        assertEquals(grapeDAO, result);
    }

    @Test
    void getByIdReturnsNullWhenAbsent() {

        when(grapeRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> {
            GrapeDAO result = grapeService.getGrapeById(1L);
        });
    }

    @Test
    void getByCodeReturnsGrapeWhenPresent() {
        GrapeDAO grapeDAO = new GrapeDAO();
        when(grapeRepository.findByCode("code")).thenReturn(Optional.of(grapeDAO));

        GrapeDAO result = grapeService.getGrapeByCode("code");

        assertEquals(grapeDAO, result);
    }

    @Test
    void getByCodeThrowsExceptionWhenAbsent() {
        when(grapeRepository.findByCode("code")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> grapeService.getGrapeByCode("code"));
    }

    @Test
    void createReturnsSavedIngredient() {
        GrapeDTO grapeDTO = new GrapeDTO("code", "name", GrapeColor.BLACK, "description");
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setCode(grapeDTO.getCode());
        grapeDAO.setName(grapeDTO.getName());
        grapeDAO.setColor(grapeDTO.getColor());
        grapeDAO.setDescription(grapeDTO.getDescription());

        when(grapeRepository.save(any(GrapeDAO.class))).thenReturn(grapeDAO);

        GrapeDAO result = grapeService.addGrape(grapeDTO);

        assertEquals(grapeDAO.getCode(), result.getCode());
        assertEquals(grapeDAO.getName(), result.getName());
        assertEquals(grapeDAO.getColor(), result.getColor());
        assertEquals(grapeDAO.getDescription(), result.getDescription());
    }

    @Test
    void updateReturnsNull() {
        when(grapeRepository.findById(1L)).thenReturn(Optional.empty());

        GrapeDTO grapeDTO = new GrapeDTO("code", "name", GrapeColor.BLACK, "description");
        assertThrows(EntityNotFoundException.class, () -> {
            grapeService.updateGrape(1L, grapeDTO);
        });
    }

    @Test
    void deleteReturnsDeletedGrape() {
        GrapeDAO grapeDAO = new GrapeDAO();
        when(grapeRepository.findById(1L)).thenReturn(Optional.of(grapeDAO));

        GrapeDAO result = grapeService.deleteGrape(1L);

        assertEquals(grapeDAO, result);
        verify(grapeRepository, times(1)).deleteById(1L);
    }
}
