package cz.muni.fi.pa165.integration;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;

import cz.muni.fi.pa165.controller.GrapeController;
import cz.muni.fi.pa165.facade.GrapeFacade;
import cz.muni.fi.pa165.model.GrapeColor;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GrapeControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GrapeFacade grapeFacade;

    @Test
    @WithMockUser(authorities = {"SCOPE_test_read", "SCOPE_test_write"})
    public void testGetGrapeById() throws Exception {
        GrapeDTO grapeDTO = new GrapeDTO("code", "Sample Name", GrapeColor.BLACK, "Sample Description");

        Mockito.when(grapeFacade.getGrapeById(anyLong())).thenReturn(grapeDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/grape/{id}", 1L)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value("code"))
                .andExpect(jsonPath("$.name").value("Sample Name"))
                .andExpect(jsonPath("$.color").value("BLACK"))
                .andExpect(jsonPath("$.description").value("Sample Description"));

        Mockito.verify(grapeFacade, Mockito.times(1)).getGrapeById(1L);
    }
}

