package cz.muni.fi.pa165.facade;

import cz.muni.fi.pa165.client.InventoryManagementClient;
import cz.muni.fi.pa165.model.GrapeColor;
import cz.muni.fi.pa165.model.Quality;
import cz.muni.fi.pa165.model.dao.GrapeDAO;
import cz.muni.fi.pa165.model.dao.HarvestDAO;
import cz.muni.fi.pa165.model.dto.GrapeDTO;
import cz.muni.fi.pa165.model.dto.GrapeSimpleDTO;
import cz.muni.fi.pa165.model.dto.HarvestDTO;
import cz.muni.fi.pa165.service.HarvestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class HarvestFacadeTest {

    @Mock
    private HarvestService harvestService;

    @InjectMocks
    private HarvestFacade harvestFacade;

    @Mock
    private InventoryManagementClient inventoryManagementClient;

    private HarvestDAO harvestDAO;
    private HarvestDTO harvestDTO;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setId(1L);
        grapeDAO.setName("Grape");
        grapeDAO.setColor(GrapeColor.BLACK);
        grapeDAO.setCode("Code");
        grapeDAO.setDescription("Description");

        harvestDAO = new HarvestDAO();
        harvestDAO.setDate(LocalDate.of(2020, 1, 8));
        harvestDAO.setAmountInKg(1);
        harvestDAO.setQuality(Quality.AVERAGE);
        harvestDAO.setGrapeType(grapeDAO);

        harvestDTO = new HarvestDTO(LocalDate.of(2020, 1, 8), 1, grapeDAO, Quality.AVERAGE);
    }

    @Test
    public void getByIdReturnsCorrectHarvest() {
        doNothing().when(inventoryManagementClient).createGrape(any(GrapeSimpleDTO.class));
        doNothing().when(inventoryManagementClient).updateGrapeStock(any(GrapeSimpleDTO.class));
        when(harvestService.getHarvestById(1L)).thenReturn(harvestDAO);

        HarvestDTO result = harvestFacade.getHarvestById(1L);

        assertEquals(harvestDTO.getDate(), result.getDate());
        assertEquals(harvestDTO.getAmountInKg(), result.getAmountInKg());
        assertEquals(harvestDTO.getQuality(), result.getQuality());
        assertEquals(harvestDTO.getGrapeType(), result.getGrapeType());
        verify(harvestService, times(1)).getHarvestById(1L);
    }

    @Test
    public void createReturnsCorrectHarvest() {
        when(harvestService.addHarvest(harvestDTO)).thenReturn(harvestDAO);

        HarvestDTO result = harvestFacade.addHarvest(harvestDTO);

        assertEquals(harvestDTO.getDate(), result.getDate());
        assertEquals(harvestDTO.getAmountInKg(), result.getAmountInKg());
        assertEquals(harvestDTO.getQuality(), result.getQuality());
        assertEquals(harvestDTO.getGrapeType(), result.getGrapeType());
        verify(harvestService, times(1)).addHarvest(harvestDTO);
    }

    @Test
    public void getAllReturnsCorrectHarvests() {
        HarvestDAO harvestDAO1 = createMockHarvestDAO(1L, LocalDate.of(2024, 4, 25), 100.0, Quality.AVERAGE);
        HarvestDAO harvestDAO2 = createMockHarvestDAO(2L, LocalDate.of(2024, 4, 26), 150.0, Quality.BAD);

        List<HarvestDAO> mockHarvestsPage0 = Arrays.asList(harvestDAO1, harvestDAO2);
        when(harvestService.getAllHarvests(0, 10)).thenReturn(mockHarvestsPage0);

        List<HarvestDTO> mockHarvestDTOsPage0 = mockHarvestsPage0.stream()
                .map(harvestFacade::mapToHarvestDTO)
                .collect(Collectors.toList());

        List<HarvestDTO> result = harvestFacade.getAllHarvests(0, 10);

        assertEquals(mockHarvestDTOsPage0.size(), result.size());

        HarvestDTO expectedDTO = result.get(0);
        assertEquals(mockHarvestDTOsPage0.get(0).getDate(), expectedDTO.getDate());
        assertEquals(mockHarvestDTOsPage0.get(0).getAmountInKg(), expectedDTO.getAmountInKg());
        assertEquals(mockHarvestDTOsPage0.get(0).getQuality(), expectedDTO.getQuality());
        assertEquals(mockHarvestDTOsPage0.get(0).getGrapeType(), expectedDTO.getGrapeType());

        verify(harvestService, times(1)).getAllHarvests(0, 10);
    }

    @Test
    void getByDateReturnsHarvestDTOsWhenPresent() {
        LocalDate date = LocalDate.of(2022, 1, 1);

        HarvestDAO harvestDAO1 = new HarvestDAO();
        harvestDAO1.setId(1L);

        HarvestDAO harvestDAO2 = new HarvestDAO();
        harvestDAO2.setId(2L);

        List<HarvestDAO> expectedHarvests = new ArrayList<>();
        expectedHarvests.add(harvestDAO1);
        expectedHarvests.add(harvestDAO2);

        when(harvestService.getHarvestsByDate(date)).thenReturn(expectedHarvests);

        List<HarvestDTO> result = harvestFacade.getHarvestsByDate(date);

        assertEquals(expectedHarvests.size(), result.size());

        for (int i = 0; i < expectedHarvests.size(); i++) {
            assertEquals(expectedHarvests.get(i).getDate(), result.get(i).getDate());
        }
    }

    @Test
    public void updateReturnsCorrectHarvest() {
        when(harvestService.updateHarvest(1L, harvestDTO)).thenReturn(harvestDAO);

        HarvestDTO result = harvestFacade.updateHarvest(1L, harvestDTO);

        assertEquals(harvestDTO.getDate(), result.getDate());
        assertEquals(harvestDTO.getAmountInKg(), result.getAmountInKg());
        assertEquals(harvestDTO.getQuality(), result.getQuality());
        assertEquals(harvestDTO.getGrapeType(), result.getGrapeType());
        verify(harvestService, times(1)).updateHarvest(1L, harvestDTO);
    }

    @Test
    void deleteReturnsDeletedGrape() {
        // Arrange
        when(harvestService.deleteHarvest(1L)).thenReturn(harvestDAO);

        // Act
        HarvestDTO result = harvestFacade.deleteHarvest(1L);

        // Assert
        assertEquals(harvestDTO.getDate(), result.getDate());
        assertEquals(harvestDTO.getAmountInKg(), result.getAmountInKg());
        assertEquals(harvestDTO.getQuality(), result.getQuality());
        assertEquals(harvestDTO.getGrapeType(), result.getGrapeType());
        verify(harvestService).deleteHarvest(1L);
    }

    private HarvestDAO createMockHarvestDAO(Long id, LocalDate date, double amountInKg, Quality quality) {
        HarvestDAO harvestDAO = new HarvestDAO();
        harvestDAO.setId(id);
        harvestDAO.setDate(date);
        harvestDAO.setAmountInKg(amountInKg);
        harvestDAO.setQuality(quality);
        // Assuming grapeType is a GrapeDAO object, create and set it accordingly
        GrapeDAO grapeDAO = new GrapeDAO();
        grapeDAO.setId(1L); // Set appropriate ID
        harvestDAO.setGrapeType(grapeDAO);
        return harvestDAO;
    }
}
