## Harvest management
This is a simple inventory management system that allows to add, update, delete, and view harvest for wine production.
Each harvest has their date, quality, type of grape, and amount in kilograms.
It also allows to add, update and delete types of grape, with their code, name, description and color.

## How to run
1. `docker-compose up`
2. `mvn clean install`
3. `mvn spring-boot:run`
