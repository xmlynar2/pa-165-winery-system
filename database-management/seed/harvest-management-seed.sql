insert into grape (id, code, name, color, description)
values (1, '111', 'Chardonnay', 0, 'GRAPE DESCRIPTION');

insert into grape (id, code, name, color, description)
values (2, '222', 'Merlot', 1, 'GRAPE DESCRIPTION');

insert into grape (id, code, name, color, description)
values (3, '333', 'Cabernet Sauvignon', 2, 'GRAPE DESCRIPTION');

insert into grape (id, code, name, color, description)
values (4, '444', 'Riesling', 3, 'GRAPE DESCRIPTION');

insert into grape (id, code, name, color, description)
values (5, '555', 'Sauvignon Blanc', 4, 'GRAPE DESCRIPTION');

insert into grape (id, code, name, color, description)
values (6, '666', 'Pinot Noir', 5, 'GRAPE DESCRIPTION');

insert into grape (id, code, name, color, description)
values (7, '777', 'Syrah', 6, 'GRAPE DESCRIPTION');

insert into harvest (id, date, amount_in_kg, quality, grape_id)
values (1, '2021-10-10', 100, 1, 1);

insert into harvest (id, date, amount_in_kg, quality, grape_id)
values (2, '2021-10-10', 200, 2, 2);

insert into harvest (id, date, amount_in_kg, quality, grape_id)
values (3, '2021-10-10', 300, 3, 3);

insert into harvest (id, date, amount_in_kg, quality, grape_id)
values (4, '2021-10-10', 400, 4, 4);

insert into harvest (id, date, amount_in_kg, quality, grape_id)
values (5, '2021-10-10', 500, 1, 5);

insert into harvest (id, date, amount_in_kg, quality, grape_id)
values (6, '2021-10-10', 600, 2, 6);

insert into harvest (id, date, amount_in_kg, quality, grape_id)
values (7, '2021-10-10', 700, 3, 7);
