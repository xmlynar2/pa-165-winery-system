insert into ingredient (id, name, quantity, unit)
values (1, 'sugar', 20, 'kg');

insert into ingredient (id, name, quantity, unit)
values (2, 'water', 20, 'l');

insert into ingredient (id, name, quantity, unit)
values (3, 'yeast', 250, 'kg');

insert into grape (id, code, quantity_available, name, description)
values (1, '111', 100, 'grape1', 'grape1description');

insert into grape (id, code, quantity_available, name, description)
values (2, '222', 100, 'grape2', 'grape2description');

insert into grape (id, code, quantity_available, name, description)
values (3, '333', 100, 'grape3', 'grape3description');

insert into grape (id, code, quantity_available, name, description)
values (4, '444', 100, 'grape4', 'grape4description');

insert into grape (id, code, quantity_available, name, description)
values (5, '555', 100, 'grape5', 'grape5description');

insert into grape (id, code, quantity_available, name, description)
values (6, '666', 100, 'grape6', 'grape6 description');

insert into grape (id, code, quantity_available, name, description)
values (7, '777', 100, 'grape7', 'grape7description');

insert into product (product_id, product_code, product_name, description, price, quantity_available, vintage_year)
values (1, '111', 'product1', 'product1description', 100.0, 200, 2019);
insert into product (product_id, product_code, product_name, description, price, quantity_available, vintage_year)
values (2, '222', 'product2', 'product2description', 10.0, 500.0, 2019);
insert into product (product_id, product_code, product_name, description, price, quantity_available, vintage_year)
values (3, '333', 'product3', 'product3description', 10.0, 500.0, 2019);
insert into product (product_id, product_code, product_name, description, price, quantity_available, vintage_year)
values (4, '444', 'product4', 'product4description', 10.0, 500.0, 2019);

insert into supplier (id, supplier_name, contact_info, region)
values (1, 'supplier1', 'supplier1contact', 'region1');

insert into supplier (id, supplier_name, contact_info, region)
values (2, 'supplier2', 'supplier2contact', 'region2');

insert into supplier (id, supplier_name, contact_info, region)
values (3, 'supplier3', 'supplier3contact', 'region3');

insert into supplier (id, supplier_name, contact_info, region)
values (4, 'supplier4', 'supplier4contact', 'region4');

insert into customer(customer_id, external_id, first_name, last_name, email, address)
values (1, 11, 'Tom', 'John', 'tom@email.com', 'address1');

insert into customer(customer_id, external_id, first_name, last_name, email, address)
values (2, 22, 'Jerry', 'John', 'jerry@email.cpm', 'address2');

insert into customer(customer_id, external_id, first_name, last_name, email, address)
values (3, 33, 'Mickey', 'Mouse', 'mickey@email.com', 'address3');

insert into transaction (id, transaction_type, product_id, quantity, customer_id, timestamp)
values (1, 1, 1, 100, 1, '2019-01-01 00:00:00');

insert into transaction (id, transaction_type, product_id, quantity, customer_id, timestamp)
values (2, 2, 2, 100, 3, '2019-01-01 00:00:00');

insert into transaction (id, transaction_type, product_id, quantity, customer_id, timestamp)
values (3, 3, 3, 100, 3, '2019-01-01 00:00:00');

insert into inventory_item (id, product_id, quantity, location)
values (1, 1, 100, 'wine cellar');
