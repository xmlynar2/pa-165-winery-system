insert into production_process (id, name, difficulty)
values ('652c1ca4-af70-4439-9e98-e9cfeb54eaec', 'Production 1', 0);
insert into production_process (id, name, difficulty)
values ('b6b7cb86-dfe8-48e2-9068-19f4a8d6fdc7', 'Production 2', 0);
insert into production_process (id, name, difficulty)
values ('d951ffd3-608b-40a9-8c56-e5d5d6f1f58f', 'Production 3', 1);
insert into production_process (id, name, difficulty)
values ('05fb3ba4-d1ce-40d9-b83c-20420dde9969', 'Production 4', 2);



insert into production_step (id, name, description, production_process_id, duration_unit)
values ('9fc0f899-efbe-405a-93be-29394390d13f', 'process_1_step_1', 1, '652c1ca4-af70-4439-9e98-e9cfeb54eaec', 1);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('9fdaf575-11e6-4782-8f8d-67a34304329f', 'process_2_step_1', 2, 'b6b7cb86-dfe8-48e2-9068-19f4a8d6fdc7', 2);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('25f5cea2-e7f0-4ff4-8921-9ca0b795ce61', 'process_2_step_2', 2, 'b6b7cb86-dfe8-48e2-9068-19f4a8d6fdc7', 2);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('0983c590-a16e-4c15-8139-d7167478503f', 'process_3_step_1', 3, 'd951ffd3-608b-40a9-8c56-e5d5d6f1f58f', 3);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('0407e5b7-d370-4c63-b12b-d748d50bd85e', 'process_3_step_2', 3, 'd951ffd3-608b-40a9-8c56-e5d5d6f1f58f', 3);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('f9c2c37c-2a75-4819-85ca-e76c94e6518e', 'process_3_step_3', 3, 'd951ffd3-608b-40a9-8c56-e5d5d6f1f58f', 3);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('24eb27d2-1e7d-4c8d-a31b-5adfa867c98c', 'process_4_step_1', 4, '05fb3ba4-d1ce-40d9-b83c-20420dde9969', 4);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('64f831c7-7e6d-4d1b-aa9e-a8669ae211c3', 'process_4_step_2', 4, '05fb3ba4-d1ce-40d9-b83c-20420dde9969', 4);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('a9b2dba0-c113-4e74-afe5-7c1b904eae6b', 'process_4_step_3', 4, '05fb3ba4-d1ce-40d9-b83c-20420dde9969', 4);

insert into production_step (id, name, description, production_process_id, duration_unit)
values ('43b3daaf-b056-473c-997d-babe45fd7d6c', 'process_4_step_4', 4, '05fb3ba4-d1ce-40d9-b83c-20420dde9969', 5);



insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('bc59eaef-3588-44cb-b630-f653861d86cb', 'sugar', 10, 1, '9fc0f899-efbe-405a-93be-29394390d13f');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('ccce4d0a-5860-4709-b7d0-19a81b988a38', 'water', 10, 2, '9fc0f899-efbe-405a-93be-29394390d13f');



insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('876be910-be41-4bdc-9dee-75bcc5907ca7', 'sugar', 20, 1, '9fdaf575-11e6-4782-8f8d-67a34304329f');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('56b5efc0-b9ce-4d59-b404-c2af9022abd9', 'water', 20, 2, '9fdaf575-11e6-4782-8f8d-67a34304329f');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('9f241c79-421a-424d-9f68-2baa5bafd889', 'sugar', 20, 1, '25f5cea2-e7f0-4ff4-8921-9ca0b795ce61');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('568e9915-b80c-4bdf-aa46-e95b11e9060b', 'water', 20, 2, '25f5cea2-e7f0-4ff4-8921-9ca0b795ce61');


insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('605db225-6a71-4e61-84e2-a004467c7eb2', 'sugar', 30, 1, '0983c590-a16e-4c15-8139-d7167478503f');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('1d12f9fa-a7e4-43c2-a303-c4f36f9ed306', 'water', 30, 2, '0407e5b7-d370-4c63-b12b-d748d50bd85e');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('e843cc47-f668-4804-8707-e2fc50563316', 'yeast', 30, 1, 'f9c2c37c-2a75-4819-85ca-e76c94e6518e');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('71e2b9d4-8c03-44b7-a574-61e534e0461e', 'sugar', 40, 1, '24eb27d2-1e7d-4c8d-a31b-5adfa867c98c');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('44f8ca04-0ddb-4ab7-ad3b-4a34a6e9dedd', 'water', 40, 1, '24eb27d2-1e7d-4c8d-a31b-5adfa867c98c');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('acd25062-6c3f-428e-b448-1701158a887e', 'yeast', 40, 1, '24eb27d2-1e7d-4c8d-a31b-5adfa867c98c');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('2355444c-9fea-48f0-9c72-37e1c0ab75b9', 'sugar', 40, 1, '64f831c7-7e6d-4d1b-aa9e-a8669ae211c3');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('df83481d-90f1-4a82-94f5-a3ed47f307ab', 'water', 40, 1, 'a9b2dba0-c113-4e74-afe5-7c1b904eae6b');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('6346437d-6be3-4536-86de-a610d2fe55e9', 'yeast', 40, 1, 'a9b2dba0-c113-4e74-afe5-7c1b904eae6b');

insert into ingredient (id, name, quantity, quantity_unit, production_step_id)
values ('09615327-7430-4c09-b9b6-1e2b3bff362d', 'sugar', 40, 1, '43b3daaf-b056-473c-997d-babe45fd7d6c');


insert into grape (id, name, code, quantity, quantity_unit, description, production_step_id)
values ('03965357-c888-4d3c-b973-53c73ee978d8', 'grape1', '111', 10, 1, 'This is grape of code 111',
        '9fc0f899-efbe-405a-93be-29394390d13f');

insert into grape (id, name, code, quantity, quantity_unit, description, production_step_id)
values ('77531881-2f2a-4cb7-9c50-79209b8ca5cf', 'grape2', '222', 10, 1, 'This is grape of code 222',
        '9fdaf575-11e6-4782-8f8d-67a34304329f');

insert into grape (id, name, code, quantity, quantity_unit, description, production_step_id)
values ('9a4a4a4a-b572-4db1-8011-0a4818f5caee', 'grape3', '333', 10, 1, 'This is grape of code 333',
        '0983c590-a16e-4c15-8139-d7167478503f');

insert into grape (id, name, code, quantity, quantity_unit, description, production_step_id)
values ('94f88a3f-62ec-43cc-887c-87c3967bd2c0', 'grape4', '444', 10, 1, 'This is grape of code 444',
        '24eb27d2-1e7d-4c8d-a31b-5adfa867c98c');