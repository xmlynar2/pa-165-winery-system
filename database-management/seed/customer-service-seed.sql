INSERT INTO customer (id, first_name, last_name, email, address)
VALUES (11, 'Tom', 'John', 'tom@email.com', 'address1');

INSERT INTO customer (id, first_name, last_name, email, address)
VALUES (22, 'Jerry', 'John', 'jerry@email.com', 'address2');

INSERT INTO customer (id, first_name, last_name, email, address)
VALUES (33, 'Mickey', 'Mouse', 'mickey@email.com', 'address3');

INSERT INTO invoice_item (id, item_name, quantity, price, wine_ext_id)
VALUES (1, 'invoice_item1', 100, 10, 111);

INSERT INTO invoice_item (id, item_name, quantity, price, wine_ext_id)
VALUES (2, 'invoice_item2', 100, 10, 222);

INSERT INTO invoice_item (id, item_name, quantity, price, wine_ext_id)
VALUES (3, 'invoice_item3', 100, 10, 333);

INSERT INTO invoice_item (id, item_name, quantity, price, wine_ext_id)
VALUES (4, 'invoice_item4', 100, 10, 444);

insert into invoice (invoice_id, price, description, date, due_date, email, phone, zip_code, address, city, customer_id)
values (1, 10, 'invoice1', '2023-01-01', '2023-02-02', 'email@email.com', '999999999', 'zip_code', 'address1', 'city1',
        33);

insert into invoice (invoice_id, price, description, date, due_date, email, phone, zip_code, address, city, customer_id)
values (2, 10, 'invoice2', '2023-03-03', '2023-04-04', 'email2@email.com', '888888888', 'zip_code2', 'address2',
        'city2', 22);

insert into purchase_history (purchase_id, customer_id, purchased_at, wine_ext_id, quantity, price)
values (1, 11, '2023-01-01', 111, 100, 10);

insert into purchase_history (purchase_id, customer_id, purchased_at, wine_ext_id, quantity, price)
values (2, 22, '2023-02-02', 222, 100, 10);

insert into purchase_history (purchase_id, customer_id, purchased_at, wine_ext_id, quantity, price)
values (3, 33, '2023-03-03', 333, 100, 10);

insert into purchase_history (purchase_id, customer_id, purchased_at, wine_ext_id, quantity, price)
values (4, 11, '2023-04-04', 444, 100, 10);

insert into feedback(id, rating, created_at, wine_ext_id, customer_id)
values (50, 4, '2023-02-02', 222, 22);

insert into feedback(id, rating, created_at, wine_ext_id, customer_id)
values (51, 3, '2023-03-03', 333, 33);

insert into feedback(id, rating, created_at, wine_ext_id, customer_id)
values (52, 2, '2023-04-04', 444, 33);
