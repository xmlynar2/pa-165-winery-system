#!/bin/bash

docker exec -i pa-165-winery-system-harvest-management-db-1 mysql -u root -p'password' harvest-management < ./database-management/clean/harvest-management-clean.sql
docker exec -i pa-165-winery-system-inventory-management-db-1 mysql -u root -p'password' winery-inventory < ./database-management/clean/inventory-management-clean.sql
docker exec -i pa-165-winery-system-customer-service-db-1 psql -U postgres -d CustomerService < ./database-management/clean/customer-service-clean.sql
docker exec -i pa-165-winery-system-wine-production-db-1 mysql -u root -p'password' wine-production < ./database-management/clean/wine-production-clean.sql

