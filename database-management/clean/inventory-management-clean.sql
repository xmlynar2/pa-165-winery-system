DELETE FROM transaction;
DELETE FROM customer;
DELETE FROM ingredient;
DELETE FROM supplier;
DELETE FROM inventory_item;
DELETE FROM grape;
DELETE FROM product;
